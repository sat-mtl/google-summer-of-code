# Google Summer of Code at SAT

This repository contains project ideas and proposals for the Google Summer of Code at the [Society for Arts and Technology](https://sat.qc.ca/).

The static website is generated with [nikola](https://getnikola.com/) and deployed to GitLab pages using [a GitLab CI configuration](.gitlab-ci.yml)

## Contributing

We have 2 major steps before start adding your ideas and work together with us to create a [GSoC proposal](https://developers.google.com/open-source/gsoc/resources/glossary#proposal): 

1. Configuring the environment: 
   * To start contributing with GSoC at SAT, please clone this repository locally and follow the instructions [environment configuration](env_configuration.md) instructions.
1. Choose the appropriate category and follow the contribution instructions:
   * At SAT, we have 2 contribution categories for preparing our GSoC submissions: [proposals](#proposals) and [project ideas](#project-ideas). Please check if you match the target audience and start contributing!

## Contribution categories

### Proposals

#### Target audience

Proposals are submitted by Contributors and reviewed by Mentors and Org Admins. 

We encourage participation from Contributors from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

#### Instructions

1. Check if you are eligible:
    * confirm that you are eligible to participate as a student contributor by reading section `7. GSoC Contributors` of GSoC rules: [https://summerofcode.withgoogle.com/rules](https://summerofcode.withgoogle.com/rules)
    * confirm that you are available during the GSoC timeline: [https://developers.google.com/open-source/gsoc/timeline](https://developers.google.com/open-source/gsoc/timeline)
    * read the GSOC contributor guide: [https://google.github.io/gsocguides/student/](https://google.github.io/gsocguides/student/)
    * browse our project ideas at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)
    * read our proposals guide at SAT: [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2025-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2025-proposals/proposal-welcome/)
1. Open a GitLab issue to reach out to us:
    1. sign [up](https://gitlab.com/users/sign_up)/[in](https://gitlab.com/users/sign_in) on [gitlab.com](https://gitlab.com)
    1. create an issue on: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues)
1. We will then work together with you Contributors so that you can submit your proposal, first as a [merge request to our repository](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests) and then by applying on [https://summerofcode.withgoogle.com](https://summerofcode.withgoogle.com) as required by the GSoC program. We will invite you to join this repository with a Developer role so that you can push to branches and use our continuous integration (CI) nodes to validate your merge request.
1. Open a merge request: In your issue, click on the blue button `Create merge request` to open a merge request linked to your issue.
1. Two options for remaining instructions: 
    1. you can clone this repository if you are familiar with the process of cloning and branching and requesting merge requests, 
    1. or (recommended) you can navigate to the branch linked on top of the page of your merge request and then click on [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) button.
1. Copy [proposal-template.md](proposal-template.md) into the [2025-proposals](website/posts/2025-proposals) folder in your merge request branch: `website/posts/2025-proposals/proposal-username-title.md`. Then rename `proposal-username-title.md` by replacing `username` with your GitLab username and `title` with your proposal title, the whole filename should be a dash-separated list of lowercase words. 
1. Edit your proposal file and follow the instructions formatted as inline HTML comments. While you write your proposal, regularly commit and push your contributions.
1. Once your proposal is ready for review, mark your merge request as ready, so that we can discuss your proposal idea. 

Google requires proposals to be submitted through their online forms, including a PDF file. Please check our [proposal forms walkthrough](website/posts/2025-proposals/proposal-forms.md). We automate the production of a PDF file for each proposal in markdown format through our [GitLab CI config](.gitlab-ci.yml). These PDF files are available as artifacts from our [GitLab CI pipelines](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/pipelines). To test the production of PDF files locally, with Ubuntu 22.04 LTS, please first run `./install.sh` then `./build-proposal.sh website/posts/2025-proposals/proposal-welcome.md` with the filename adapted to your proposal. 

### Project ideas

#### Target audience

Project ideas are mainly written by Mentors and Org Admins. 
Contributors and new/external Mentors are welcome to suggest project ideas in collaboration with existing Mentors and Org Admins.

We encourage participation from Contributors and Mentors from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. 
We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

#### Instructions

1. [Open an issue](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues) to discuss with us. Existing Mentors and Org Admins will invite Contributors and new/external Mentors to join this repository with Developer role, so that they can push to branches and use our continuous integration (CI) nodes to validate your merge request.
1. Open a merge request. In your issue, click on the blue button `Create merge request` to open a merge request linked to your issue. Two options for the remaining instructions: 
   1. you can clone this repository if you are familiar with the process of cloning and branching and requesting merge requests, 
   1. or (recommended) you can navigate to the branch linked on top of the page of your merge request and then click on [Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) button.
1. Copy [idea-template.md](idea-template.md) into the [2025-ideas](website/posts/2025-ideas) folder in your merge request branch: `website/posts/2025-ideas/idea-title.md`. Then rename `idea-title.md` by replacing `title` by a dash-separated list of lowercase words.
1. Write your project idea, commit and push your contributions.
1. Once your project idea is ready for review, mark your merge request as ready, so that we can discuss your project idea.

<!-- 

### Communication

From: https://developers.google.com/open-source/gsoc/resources/marketing#template_email_for_gsoc_applicants

> Do you know anyone who might be a good candidate to participate as a contributor in GSoC? You can use the sample email below to help advertise the program. Consider sending it to appropriate schools, teachers, friends, clubs, developer communities, etc.

Please use this communication template adapted for SAT: [website/posts/2025-share.md](website/posts/2025-share.md)

### Contributions

Every contributor has their own folder under `website/posts/2025-contributions/` where they can publish:
* blog posts: please copy [blog-post-template.md](blog-post-template.md) into your folder under `website/posts/2025-contributions/` with `YYYY-MM-dd-username-title` for filename and `slug` (replacing `YYYY-MM-dd` by the date, `username` by your gitlab username and `title` by your blog post title, the whole filename should be a dash-separated list of lowercase words) and customize metadata and contents.
* [work products](https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission): please copy [work-product-template.md](work-product-template.md) into your folder under `website/posts/2025-contributions/` with as filename `username-work-product.md` (replacing `username` by your gitlab username) and customize metadata and contents. 

-->

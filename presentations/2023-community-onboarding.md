---
title: Google Summer of Code (GSoC) with the Society for Arts and Technology (SAT)
subtitle: 2023 Community Onboarding
author: Christian Frisson, Edu Meneses
institute: Society for Arts and Technology [SAT]
separator: <!--s-->
verticalSeparator: <!--v-->
theme: white 
revealOptions:
  transition: 'none' 
  loop: false 
  slideNumber: "c/t"
  autoPlayMedia: true
--- 

<!-- .slide: id="start" -->
  <h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
  <h3 data-i18n="conference">Community Onboarding</h3>
  <h4 data-i18n="subtitle">2023-05-26</h4>

  <div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>

<hr/>

#### Organization Administrators (and Mentors)


  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/EduMeneses.jpg" alt="Edu Meneses"/>
      <div class="name"><a href="https://gitlab.com/edumeneses">Edu Meneses</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/EmmanuelDurand.jpg" alt="Emmanuel Durand"/>
      <div class="name"><a href="https://gitlab.com/paperManu">Emmanuel Durand</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/FrancisLecavalier.jpg" alt="Francis Lecavalier"/>
      <div class="name"><a href="https://gitlab.com/flecavalier">Francis Lecavalier</a></div>
    </div>
  </div>

#### Mentors

  <div class="authors">
    <div class="author">
      <img src="images/portraits/AnkeVanOosterhout.jpg" alt="Anke van Oosterhout"/>
      <div class="name"><a href="https://gitlab.com/AnkevOosterhout">Anke van Oosterhout</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/GuillaumeRiou.jpg" alt="Guillaume Riou"/>
      <div class="name"><a href="https://gitlab.com/guillaumeriousat">Guillaume Riou</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/JeanMichaelCelerier.jpg" alt="Jean-Michaël Celerier"/>
      <div class="name"><a href="https://gitlab.com/jcelerier">Jean-Michaël Celerier</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/ManuelBolduc.jpg" alt="Manuel Bolduc "/>
      <div class="name"><a href="https://gitlab.com/bolducmanuel">Manuel Bolduc</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
      <div class="name"><a href="https://gitlab.com/djiamnot">Michał Seta</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/OlivierGauthier.jpg" alt="Olivier Gauthier"/>
      <div class="name"><a href="https://gitlab.com/ogauthier_sat">Olivier Gauthier</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/RochanaFardon.jpg" alt="Rochana Fardon"/>
      <div class="name"><a href="https://gitlab.com/rfardon">Rochana Fardon</a></div>
    </div>
  </div>


#### Contributors

  <div class="authors">
    <div class="author">
      <img src="images/portraits/FannyCacilie.jpg" alt="Fanny Cacilie"/>
      <div class="name"><a href="https://gitlab.com/caciliefanny">Fanny Cacilie</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/HaokunSong.jpg" alt="Haokun Song"/>
      <div class="name"><a href="https://gitlab.com/shk741612898">Haokun Song</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/MaxwellGentiliMorin.jpg" alt="Maxwell Gentili-Morin"/>
      <div class="name"><a href="https://gitlab.com/Maxw3llGM">Maxwell Gentili-Morin</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/PedroAndradeFerreiraSobrinho.jpg" alt="Pedro Andrade Ferreira Sobrinho"/>
      <div class="name"><a href="https://gitlab.com/pedroansa1">Pedro Andrade Ferreira Sobrinho</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/TarekYasser.jpg" alt="Tarek Yasser"/>
      <div class="name"><a href="https://gitlab.com/KnockerPulsar">Tarek Yasser</a></div>
    </div>
  </div>

<!--s-->
  <!-- .slide: id="sat" -->
  <h2 data-i18n="title"><a href="https://sat.qc.ca">Society for Arts and Technology (SAT)</a></h2>

  <div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>

<div class="row">
<div class="col-20 fragment">
  <img src="images/sat.svg" style="height:700px;margin-top:-50px" />  
</div>
<div class="col-80 fragment">

<iframe src="https://player.vimeo.com/video/390637480?loop=false&amp;autoplay=false&amp;muted=false&amp;gesture=media&amp;playsinline=true&amp;byline=false&amp;portrait=false&amp;title=false&amp;speed=true&amp;transparent=false&amp;customControls=true" allowfullscreen="" allow="autoplay; fullscreen; picture-in-picture; encrypted-media; accelerometer; gyroscope" width="640" height="360" title="Player for Société des arts technologiques (SAT)" data-ready="true" tabindex="-1"></iframe>

</div>
</div>

Notes:

> In the heart of Montreal, the SAT is a unique place that offers the public immersive experiences in its famous dome, but also concerts, workshops, conferences, exhibitions… Hundreds of events are presented there every year.

> Founded in 1996, the Society for Arts and Technology [SAT] is a non-profit organization dedicated to digital culture. With its triple mission as a center for the arts, training and research, the SAT is a gathering space for diverse talent, curiosity, and knowledge. It is recognized internationally for its active, leading role in developing technologies for immersive creation, mixed realities and telepresence. The Society for Arts and Technology is a place of collective learning that holds the promise of exploring technology to infuse it with more meaning, magic and humanity. 

<!--v-->
  <!-- .slide: id="sat-tools" -->
  <h1>Ecosystem of tools at the SAT</h1>
  <h2>Inter-operability and modalities</h2>
  <div class="cite"><a href="https://sat-mtl.gitlab.io/tools/">https://sat-mtl.gitlab.io/tools/</a></div>
  <div class="fig-container" data-file="https://sat-mtl.gitlab.io/tools/" style="overflow: visible;"></div>

<!--v-->
  <!-- .slide: id="gsoc-stats" -->
  <h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
  <h3 data-i18n="conference">Statistics</h3>

<div class="row">
<div class="col-50 fragment">

[GSoC](https://opensource.googleblog.com/2023/05/google-summer-of-code-2023-accepted-contributors-announced.html)

19th year of GSoC 

171 mentoring organizations

43,765 applicants from 160 countries

7,723 proposals submitted

967 GSoC contributors accepted from 65 countries

Over 2,400 mentors and organization administrators

</div>
<div class="col-50 fragment">

at the SAT

2nd year as mentoring organization <br/>(since 2022)

21 proposals submitted

13 proposals ranked

5 GSoC contributors accepted

11 mentors and 4 organization administrators

</div>
</div>


<!--v-->

# For each proposal

## Related open-source tool(s)

## Scope of the contribution

## Benefits to the community at the SAT


<!--s-->

<h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
<h3 data-i18n="conference">Community Onboarding</h3>
<h4 data-i18n="subtitle">2023-05-26</h4>
<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>
<hr/>
<div class="row">
<div class="col-40">
<h4>Proposal</h4> 

[Motion Capture in WebXR](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-caciliefanny-motion-capture-in-webxr/)

</div>
<div class="col-60">
<h4>Contributor</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/FannyCacilie.jpg" alt="Fanny Cacilie"/>
      <div class="name"><a href="https://gitlab.com/caciliefanny">Fanny Cacilie</a></div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-40">
<h4>Project idea</h4> 

[Motion Capture in WebXR](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-ideas/idea-mocap-in-webvr/)

</div>
<div class="col-60">
<h4>Mentors</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/ManuelBolduc.jpg" alt="Manuel Bolduc "/>
      <div class="name"><a href="https://gitlab.com/bolducmanuel">Manuel Bolduc</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/RochanaFardon.jpg" alt="Rochana Fardon"/>
      <div class="name"><a href="https://gitlab.com/rfardon">Rochana Fardon</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
  </div>
</div>
</div>

<!--v-->

## Related open-source tool(s)

<!-- insert video(s) -->

<div class="row">
<div class="col-50">

[LivePose](https://gitlab.com/sat-mtl/tools/livepose)

(embedding [Google MediaPipe](https://developers.google.com/mediapipe) among other pose estimation backends)

</div>
<div class="col-50">

[Satellite](https://hub.satellite.sat.qc.ca)

(SAT's fork of [Mozilla Hubs](https://hubs.mozilla.com))

</div>
</div>

<iframe src="https://player.vimeo.com/video/604196712?h=e3903dffe0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/604196712">Experimental control of Mozilla Hubs client with LivePose (Sept. 2020)</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->


<!--v-->

## Scope of the contribution

<!-- import content from proposal -->

Develop a platform prototype to generate a fluid XR experience for users by reliving live performances through motion capture. 
The key technologies for this goal are: MediaPipe Pose, open-source software for pose detection in a live streaming context, and WebXR for the development and hosting of virtual reality and augmented reality experiences on the web.

<!--v-->

## Benefits to the community at the SAT

<!-- import content from proposal -->

- Possibilitites for exploring new forms of entertainment, preserving cultural heritage, and creating economic opportunities
- Make XR experiences available to people who are unable to attend in person
- Enhance the experience of revisiting past performances by providing a more immersive and engaging environment than traditional video recordings

<!--s-->

<h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
<h3 data-i18n="conference">Community Onboarding</h3>
<h4 data-i18n="subtitle">2023-05-26</h4>
<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>
<hr/>
<div class="row">
<div class="col-40">
<h4>Proposal</h4> 

[Development of Generalizable SATIE Mappers for Hybrid Sound Configurations that Optimized Audio Spatialization Flexibility](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-kuki-song-satie-multispatializer-mapper/)

</div>
<div class="col-60">
<h4>Contributor</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/HaokunSong.jpg" alt="Haokun Song"/>
      <div class="name"><a href="https://gitlab.com/shk741612898">Haokun Song</a></div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-40">
<h4>Project idea</h4> 

[SATIE Multispatializer Mapper](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-ideas/idea-SATIE-multispatializer-mapper/)

</div>
<div class="col-60">
<h4>Mentors</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/EduMeneses.jpg" alt="Edu Meneses"/>
      <div class="name"><a href="https://gitlab.com/edumeneses">Edu Meneses</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
      <div class="name"><a href="https://gitlab.com/djiamnot">Michał Seta</a></div>
    </div>
  </div>
</div>
</div>

<!--v-->

## Related open-source tool(s)

<!-- insert video(s) -->

[SATIE](https://gitlab.com/sat-mtl/tools/satie/satie)

<iframe src="https://player.vimeo.com/video/690698818?h=450e74bee7" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/690698818">SATIE livecoding new synths</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

<!--v-->

## Scope of the contribution

<!-- import content from proposal -->

One interesting (and often unexplored) user case for spatializing sound in SATIE is coupling the venue's sound system with added speakers. 
This practice is employed to increase spatial resolution. 

The common method to implement this hybrid configuration (venue and added speakers) does not account for scenarios where it makes semantic sense to have separate speaker configurations while having sound objects shared between spatializers.

<!--v-->

## Benefits to the community at the SAT

<!-- import content from proposal -->

- Allow the user to define relative positioning for multiple spatializers in SATIE in different scenarios without having to reprogram the mapper.
- Improve understanding of complex spatialization settings by allowing the organization of spatializers semantically.

<!--s-->

<h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
<h3 data-i18n="conference">Community Onboarding</h3>
<h4 data-i18n="subtitle">2023-05-26</h4>
<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>
<hr/>
<div class="row">
<div class="col-40">
<h4>Proposal</h4> 

[Audio to Haptic interaction design with ForceHost and Feelix supporting DeformableHapticSurfaces](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-maxw3llgm-force-host-support-deformablehapticsurfaces-feelix/)

</div>
<div class="col-60">
<h4>Contributor</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/MaxwellGentiliMorin.jpg" alt="Maxwell Gentili-Morin"/>
      <div class="name"><a href="https://gitlab.com/Maxw3llGM">Maxwell Gentili-Morin</a></div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-40">
<h4>Project idea</h4> 

[Haptic and audio interaction design with Feelix supporting TorqueTuner and/or DeformableHapticSurfaces](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-ideas/idea-feelix-torquetuner-forcehost-deformablehapticsurfaces/)

</div>
<div class="col-60">
<h4>Mentors</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/AnkeVanOosterhout.jpg" alt="Anke van Oosterhout"/>
      <div class="name"><a href="https://gitlab.com/AnkevOosterhout">Anke van Oosterhout</a></div>
    </div>
  </div>
</div>
</div>

<!--v-->

## Related open-source tool(s)

<!-- insert video(s) -->

<div class="row">
<div class="col-50 fragment">

[SAT's Haptic Floor](https://sat.qc.ca/en/haptic-floor)

<iframe src="https://player.vimeo.com/video/676270108?h=4b5a162502" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/676270108">Scalable Haptic Floor (SAT - Metalab, 2018)</a> from <a href="https://vimeo.com/satmontreal">Society for Arts and Technology</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

</div>
<div class="col-50 fragment">

[Feelix](https://feelix.xyz/)

<iframe src="https://player.vimeo.com/video/726537054?h=a8307ee9b0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/726537054">Introduction to Feelix.mp4</a> from <a href="https://vimeo.com/user45764564">Anke van Oosterhout</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

</div>
</div>

<!--v-->

## Scope of the contribution

<!-- import content from proposal -->


The authoring tools [Feelix](https://feelix.xyz/) and [ForceHost](https://gitlab.com/ForceHost) will be extended for use with the [DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces), a work-in-progress and open-source toolkit for interactive multi-linear DoF deformable surfaces, to create and demo the proposed immersive haptic and audio interaction toolkit.

<div class="row">
<div class="col-50 fragment">

[DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces)

![](images/projects/DeformableHapticSurfaces.jpg)

</div>
<div class="col-50 fragment">

[ForceHost](https://gitlab.com/ForceHost)

<iframe width="560" height="315" src="https://www.youtube.com/embed/smFpkdw-J2w" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

</div>
</div>

<!--v-->

## Benefits to the community at the SAT

<!-- import content from proposal -->

- Facilitate the development and authoring of haptic devices by having the designer focus more on the creation process than the technical aspects
- Artists in residency can use the toolkit to quickly begin prototyping and designing for the haptic floor in the Dome
- Artists would have more time to create their immersive art performance/installation and spend less time debugging, allowing for more creative results

<!--s-->

<h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
<h3 data-i18n="conference">Community Onboarding</h3>
<h4 data-i18n="subtitle">2023-05-26</h4>
<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>
<hr/>
<div class="row">
<div class="col-40">
<h4>Proposal</h4> 

[Building Immersive Learning Experiences](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-pedroansa1-hybrid-telepresence-campus/)

</div>
<div class="col-60">
<h4>Contributor</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/PedroAndradeFerreiraSobrinho.jpg" alt="Pedro Andrade Ferreira Sobrinho"/>
      <div class="name"><a href="https://gitlab.com/pedroansa1">Pedro Andrade Ferreira Sobrinho</a></div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-40">
<h4>Project idea</h4> 

[Explorable explanations for teaching digital arts concepts in hybrid telepresence campus](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-ideas/idea-explorable-explanations-campus-satellite/)

</div>
<div class="col-60">
<h4>Mentors</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/RochanaFardon.jpg" alt="Rochana Fardon"/>
      <div class="name"><a href="https://gitlab.com/rfardon">Rochana Fardon</a></div>
    </div>
  </div>
</div>
</div>

<!--v-->

## Related open-source tool(s)

<!-- insert video(s) -->

<div class="row">
<div class="col-50 fragment">

[Satellite](https://hub.satellite.sat.qc.ca)

(SAT's fork of [Mozilla Hubs](https://hubs.mozilla.com))


<iframe src="https://player.vimeo.com/video/589518329?h=63072a6e1e&color=69ce8e" width="640" height="360" frameborder="0" allow="fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/589518329">Satellite - Espace Campus</a> from <a href="https://vimeo.com/satmontreal">Society for Arts and Technology</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

</div>
<div class="col-50 fragment">

Explorable Explanations

Media for Thinking the Unthinkable

<iframe src="https://player.vimeo.com/video/67076984?h=f57f26cc02&portrait=0" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/67076984">Media for Thinking the Unthinkable</a> from <a href="https://vimeo.com/worrydream">Bret Victor</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

</div>
</div>

<!--v-->

## Scope of the contribution

<!-- import content from proposal -->

The project seeks to create synergy by interconnecting various cultural and artistic contents in the Satellite hub and embedding explorable explanations to enhance the learning experience. The explorable explanations will be designed to cater to a broad audience and facilitate access to learning.

<!--v-->

## Benefits to the community at the SAT

<!-- import content from proposal -->

- Populate Satellite Hub with examples of playful and educational explanations
- Expand and support pedagogical techniques in areas where traditional education cannot reach or still has many difficulties in developing like digital arts
- Aid popularizing the space of the Satellite Hub as a tool capable of expanding and democratizing access to knowledge in an immersive way

<!--s-->

<h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
<h3 data-i18n="conference">Community Onboarding</h3>
<h4 data-i18n="subtitle">2023-05-26</h4>
<div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>
<hr/>
<div class="row">
<div class="col-40">
<h4>Proposal</h4> 

[Replace OpenGL by a multi-API rendering library in Splash](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-proposals/proposal-knockerpulsar-replace-opengl-by-a-multi-api-rendering-library-in-splash/)

</div>
<div class="col-60">
<h4>Contributor</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/TarekYasser.jpg" alt="Tarek Yasser"/>
      <div class="name"><a href="https://gitlab.com/KnockerPulsar">Tarek Yasser</a></div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div class="col-40">
<h4>Project idea</h4> 

[Replace OpenGL by a multi-API rendering library in Splash](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-ideas/idea-splash-rendering-api/)

</div>
<div class="col-60">
<h4>Mentors</h4>
  <div class="authors">
    <div class="author">
      <img src="images/portraits/EmmanuelDurand.jpg" alt="Emmanuel Durand"/>
      <div class="name"><a href="https://gitlab.com/paperManu">Emmanuel Durand</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/JeanMichaelCelerier.jpg" alt="Jean-Michaël Celerier"/>
      <div class="name"><a href="https://gitlab.com/jcelerier">Jean-Michaël Celerier</a></div>
    </div>
  </div>
</div>
</div>

<!--v-->

## Related open-source tool(s)

<!-- insert video(s) -->

Splash

<iframe src="https://player.vimeo.com/video/268028595?h=ffc8974d11" width="640" height="360" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<!-- <p><a href="https://vimeo.com/268028595">Projection sur objets mobiles - Projection mapping on moving objects</a> from <a href="https://vimeo.com/satmetalab">Metalab | SAT</a> on <a href="https://vimeo.com">Vimeo</a>.</p> -->

<!--v-->

## Scope of the contribution

<!-- import content from proposal -->

To be able to optimize it further and support more platforms (for example Raspberry Pi), Splash needs refactoring to replace direct use of OpenGL with an intermediate, multi-API rendering library.

<!--v-->

## Benefits to the community at the SAT

<!-- import content from proposal -->

- Helping the community by allowing splash to run on more devices/OSes.
- Helping the Splash team by making working with rendering code easier.
- Would be a precedent on how to convert a project from having mixed rendering code to a clearer separation.

<!--s-->

<!-- .slide: id="end" -->
  <h2 data-i18n="title">Google Summer of Code (GSoC) <br/> with the Society for Arts and Technology (SAT)</h2>
  <h3 data-i18n="conference">Community Onboarding</h3>
  <h4 data-i18n="subtitle">2023-05-26</h4>

  <div class="affiliations">
<div class="affiliation">
<a class="logo" href="https://sat.qc.ca" title="Société des Arts Technologiques"><img src="images/logos/LOGOSAT_ENGBLCK.png" alt="Société des Arts Technologiques"/></a>
</div>
</div>

<hr/>

#### Organization Administrators (and Mentors)


  <div class="authors">
    <div class="author">
      <img src="images/portraits/ChristianFrisson.jpg" alt="Christian Frisson"/>
      <div class="name"><a href="https://gitlab.com/ChristianFrisson">Christian Frisson</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/EduMeneses.jpg" alt="Edu Meneses"/>
      <div class="name"><a href="https://gitlab.com/edumeneses">Edu Meneses</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/EmmanuelDurand.jpg" alt="Emmanuel Durand"/>
      <div class="name"><a href="https://gitlab.com/paperManu">Emmanuel Durand</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/FrancisLecavalier.jpg" alt="Francis Lecavalier"/>
      <div class="name"><a href="https://gitlab.com/flecavalier">Francis Lecavalier</a></div>
    </div>
  </div>

#### Mentors

  <div class="authors">
    <div class="author">
      <img src="images/portraits/AnkeVanOosterhout.jpg" alt="Anke van Oosterhout"/>
      <div class="name"><a href="https://gitlab.com/AnkevOosterhout">Anke van Oosterhout</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/GuillaumeRiou.jpg" alt="Guillaume Riou"/>
      <div class="name"><a href="https://gitlab.com/guillaumeriousat">Guillaume Riou</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/JeanMichaelCelerier.jpg" alt="Jean-Michaël Celerier"/>
      <div class="name"><a href="https://gitlab.com/jcelerier">Jean-Michaël Celerier</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/ManuelBolduc.jpg" alt="Manuel Bolduc "/>
      <div class="name"><a href="https://gitlab.com/bolducmanuel">Manuel Bolduc</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/MichalSeta.jpg" alt="Michał Seta"/>
      <div class="name"><a href="https://gitlab.com/djiamnot">Michał Seta</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/OlivierGauthier.jpg" alt="Olivier Gauthier"/>
      <div class="name"><a href="https://gitlab.com/ogauthier_sat">Olivier Gauthier</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/RochanaFardon.jpg" alt="Rochana Fardon"/>
      <div class="name"><a href="https://gitlab.com/rfardon">Rochana Fardon</a></div>
    </div>
  </div>


#### Contributors

  <div class="authors">
    <div class="author">
      <img src="images/portraits/FannyCacilie.jpg" alt="Fanny Cacilie"/>
      <div class="name"><a href="https://gitlab.com/caciliefanny">Fanny Cacilie</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/HaokunSong.jpg" alt="Haokun Song"/>
      <div class="name"><a href="https://gitlab.com/shk741612898">Haokun Song</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/MaxwellGentiliMorin.jpg" alt="Maxwell Gentili-Morin"/>
      <div class="name"><a href="https://gitlab.com/Maxw3llGM">Maxwell Gentili-Morin</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/PedroAndradeFerreiraSobrinho.jpg" alt="Pedro Andrade Ferreira Sobrinho"/>
      <div class="name"><a href="https://gitlab.com/pedroansa1">Pedro Andrade Ferreira Sobrinho</a></div>
    </div>
    <div class="author">
      <img src="images/portraits/TarekYasser.jpg" alt="Tarek Yasser"/>
      <div class="name"><a href="https://gitlab.com/KnockerPulsar">Tarek Yasser</a></div>
    </div>
  </div>

<!--v-->

# Activity

## Contributors

### Start your onboarding process!

(can also be done later asynchronously)

* open an issue at the [SAT's GSoC repository](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues), 
* choose `onboarding` in the drop-down menu under `Description`, 
* start updating the description and addressing related tasks

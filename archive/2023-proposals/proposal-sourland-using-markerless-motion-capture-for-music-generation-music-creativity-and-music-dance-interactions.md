---
title: Using markerless motion capture for music generation, music creativity and music-dance interactions
slug: proposal-sourland-using-markerless-motion-capture-for-music-generation-music-creativity-and-music-dance-interactions
author: Dimitrios Sourlantzis
date: 2023-3-24 17:42 UTC+03:00
tags: proposals, hard, 350 hours, LivePose, deepcutlab-live
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--

This template has been created using the following resource:

https://google.github.io/gsocguides/student/writing-a-proposal

-->

<!--

Please update post metadata:

* title

* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)

* author (your name as contributor)

* date (not in future otherwise not visible now when website is built)

* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT)

-->

<!--

If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal.

We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers.

-->

<!--

We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).

-->
## Name and Contact Information
<!-- Please your public contact information below this comment and add your name as value in post author metadata -->
* name: Dimitrios Sourlantzis
* website: https://github.com/Sourland
* gitlab username: Sourland
* timezone: UTC+3
<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title
<!--

Your title should be short, clear and interesting.

The job of the title is to convince the reviewer to read your synopsis.

Do not put your name in the title.

-->

Using markerless motion capture for music generation, music creativity and music-dance interactions

## Synopsis
<!--

Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal.

Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md

(150-200 words)

-->

Music and dancing have always been one of the most popular recreational activities for a very long time. Producing music or composing a choreography is a task done by hobbyists and professionals, and both activities have deep ties with each other. We want to explore the possibility of generating music from dancing, use it as feedback, and explore music-dancing interactions on a much bigger scale.

By using visual data (dance moves) as input, we can estimate the body pose in the current frame and use these body marks to map a musical sound like a beat or a note, in real-time. It would be preferable to explore deep learning approaches to complete this task. Computer Vision has been proven to perform exceptionally well in image data interpretation. Moreover, tools like Livepose, which use computer vision, help us extract the positional features of the pose estimation from image data of a person. We can enhance the functionality of these tools to support real-time usage by implementing modifications and improvements. For instance, we can update their core components with better tools to achieve this goal.

To generate music in real-time based on human body poses, we require an audiovisual generator that can produce a sound for every pose. To achieve this, we need to map the body poses to musical elements, synthesize them, and incorporate musical context into the generated music. An audiovisual generation system can use various techniques, such as a rule-based approach, generative models, or neural networks, to accomplish this task. There are multiple paths that such a project can follow depending on the chosen technique and the desired output.

## Benefits to Community
<!--

Make your case for a benefit to the organization, not just to yourself.

Why would Google and SAT be proud to sponsor this work?

How would open source or society as a whole benefit?

What cool things would be demonstrated?

(150-200 words)

-->

This project aims to help dancing or music professionals and hobbyists express themselves freely without restrictions. This can facilitate more creative and innovative music and dance performances while having an impact on people with limited ability and on therapeutic/rehabilitative applications. We also aim to make people bond better. By showing each other music results from free dancing, they can share their experiences and establish emotional connections. We can further explore the relationship between music and dancing, providing valuable insights for research and psychology.

By making this tool open-source, we enable contributions, which can help to improve the overall quality of the project. Open-source projects are often used to create interesting variations or serve as a foundation for entirely new projects, and we invite people from around the world to participate. This includes developing improvements, exploring new approaches, or supporting other platforms.

## Deliverables
<!--

Include a brief, clear work breakdown structure with milestones and deadlines.

Make sure to label deliverables as optional or required.

You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style.

It’s OK to include thinking time (“investigation”) in your work schedule.

Deliverables should include investigation, coding and documentation.

(max 1000 words)

-->

### *Project in detail*

The project consists of 3 major parts:

* Investigation and research on the topic:
	* Investigate the backend to be integrated into LivePose
	* Research on audio generation and synthesis using marks generated by LivePose
	* Research on real-time audio generation with low latency.
* Design in a software engineering manner the new code that is to be implemented, tracked by git issues and roadmap and dependencies:
	* Study code of LivePose	
	* Study code of chosen backend	
	* Design how chosen backend will be integrated into LivePose	
	* Open relevant issues, create a roadmap, and track dependencies
* Code developing resolving created issues:	
	* Develop code implementing the desired behavior to resolve issues.
	* Create unit tests for code validation	
	* Write good documentation	
		* Documentation explaining code	
		* Updates on doc pages if necessary	
	* Write possible improvements for future work

### *May 4 - 28: Community Bonding Period | GSoC contributors get to know mentors, read documentation, get up to speed to begin working on their projects*

In this period it is important to do some legwork before starting developing. This means, getting to know my mentoring organization I will be working with, getting to know mentors, discuss my ideas and approaches, take feedback on decisions. Moreover, have a decision on what backend pose detection I should use, and have ready some papers to study for audio generation in real time.

#### *May 4 - May 16*

* Meet mentors,	
* study livepose documentation,	
* study documentation of potential backends to be integrated into livepose.

#### *May 17 - May 28*

* Study documentation of extra libraries required for the project.	
* Create a collection of papers for the audio generation.	
* Having an initial software-engineered structure on how I will proceed with developing. Since everything will be hosted on GitLab, a platform I have worked on extensively, I will track progress with GitLab Milestones and Issues.  

### *May 29 - Coding officially begins!*

This is the first period of coding in GSoC, lasting 6 weeks in total. I will break down this time into 2 big segments:

#### *May 29 - June 25*

* Have completely integrated a new backend into livepose (required),	
* Implement or improve existing functions in order to support and use the new backend (required)	
* Write documentation	
	* Write of update code documentation (required)	
	* Update Livepose docs pages (required if necessary)	
* Create unit-tests (required).	
* Further develop or update CI/CD pipelines on Gitlab (required if necessary)

#### *June 25 - July 10*

* Investigate on low-latency music generation and synthesis. (Required)	
* Design the audio generating system. (Required)

#### *July 10 - July 14*

Submit midterm evaluation

#### *July 14 - August 21*

* Develop an audio generation engine. This includes implementing the findings from the (June 25 - July 10) segment.
* By using the output of the newly updated LivePose as input, create a "mapping" system that produces a sound.
* Write documentation
	* Write of update code documentation (required)
	* Update Livepose docs pages and create new doc pages for this tool (required)
* Develop unit-tests (required)

#### *Optional Deriverables*

* Investigation and implementation of various music styles.
* Implement a choice system where the user can choose the style of music (hip-hop, rock, classical)
* Create a list of possible improvements.

#### *August 21 - 28*


Submit final work product


## Related Work
<!--

You should understand and communicate other people’s work that may be related to your own.

Do your research, and make sure you understand how the project you are proposing fits into the target organization.

Be sure to explain how the proposed work is different from similar related work.

(max 1000 words)

-->

To begin with, there is related academic work done by a lot of people.

"Deep Audio-visual Learning: A Survey" is a paper investigating different areas of audio and visual component relationships. The areas are:

* audiovisual separation and localization,
* audio-visual correspondence learning,
* audio-visual generation, and
* audio-visual representation learning.

For this project we need to explore audio and visual generation, and more specific, motion to audio and audio to motions. One mention paper is "Learning to dance: A graph convolutional adversarial network to generate realistic dance motions from audio". In this paper, music and a video of a person dancing to said music is used as input and a body pose is estimated. It is possible to explore the opposite relationship, meaning using the dancing and pose estimation to generate the music.

Another approach is a paper called "ChoreoNet: Towards Music to Dance Synthesis with Choreographic Action Unit" that explores learning relations between dancing and music and could be very helpful for the audio generation.

## Biographical Information
<!--

Keep your personal info brief.

Be sure to communicate personal experiences and skills that might be relevant to the project.

Summarize your education, work, and open source experience.

List your skills and give evidence of your qualifications.

Convince your organization that you can do the work.

Any published work, successful open source projects and the like should definitely be mentioned.

(max 200 words)

-->

My name is Dimitrios Sourlantzis, and I am currently studying Electrical and Computer Engineering at the Aristotle University of Thessaloniki. I am interested in AI for more than a year now, and I have picked up relevant courses and completed coursework during my studies. After taking an Image Processing course and a Computer Graphics course, I decided to shift to computer vision. Some of the course projects I have completed are:

* [Deep Learning from scratch and Autoencoders](https://github.com/Sourland/deep-learning)
This project is about creating a neural network from scratch using Julia. Furthermore, there is an implemented Autoencoder for representation learning and image denoising.

* [Image stitching](https://github.com/Sourland/image-stitching)
Combine 2 images with overlapping fields to create a panoramic image

* [Image Clustering](https://github.com/Sourland/image-clustering)
Perform image clustering in normal pictures and using super-pixels

All my projects can be found at my [GitHub](https://github.com/Sourland)

I was introduced to open-source software by joining the AcubeSAT project by SpaceDot. AcubeSAT is an open-source CubeSat satellite containing a lab-on-a-chip to study eukaryotic gene expression on a large scale, under the ESA Education "Fly Your Satellite!" 3 programme
For the past one and a half years, I have been developing software for the On-board Computer (OBC) and the Attitude Determination and Control Subsystem (ADCS) of the nanosatellite. To find out more, click the links below:

* [https://spacedot.gr/](https://spacedot.gr/)
* [https://acubesat.spacedot.gr/](https://acubesat.spacedot.gr/)

The whole AcubeSAT project is hosted at GitLab:

* [https://gitlab.com/acubesat](https://gitlab.com/acubesat)

I do not have any real-life experience with deep learning or computer vision projects. I hope by working on GSoC I will obtain very valuable information.

## Skills
<!--

Copy the list of required/preferred skills from the project idea and paste it below.

For each skill, illustrate with your past experience how you are the best fit for your proposal.

The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.

-->

#### Required:
* Python: Moderate-good experience from school projects, using NumPy, TensorFlow, and PyTorch.
* C++: Moderate-good experience from contributions to AcubeSAT.
* Git: I have used Git extensively for the past year. I am very familiar with issues, merge requests, and code reviews.
#### Preferred:
* Computer Vision: Beginner, knowledge, and some experience from relevant deeplearning.ai course.
* Generative Models: Beginner, knowledge, and some experience from relevant deeplearning.ai course.
#### May need:
* GPU programming: No experience.
## Motivation
I want to work on this project because I see educational value in it. Since I try to gain expertise in AI and become a better developer and researcher, this is a very good opportunity for improvement. Furthermore, I like working on open-source code, I have seen some amazing open-source projects and I like sharing knowledge.

On the other hand, I aim to become financially independent from my parents, so this project is a solid first step in achieving this. Last but not least, the project itself is very creative and innovative. After searching through all 172 organizations registered in GSoC 2023, this project is the best fit for me.

## Expected size of project
<!--

Please write below this comment either: 175 hours or 350 hours

Please also adjust accordingly tags in post metadata.

-->
350 Hours

## Working time and time management

I aim to work 30-40 hours per week (that is 6-8 hours per day, depending on workload), from 8:30 to 15:00 (GMT+3) (on average). During this time, I aim to primarily work on the project, while also planning work for the next day.

I am working on my diploma thesis for my engineering degree on nail disease detection and evaluation. Since it is common for seniors to both apply for internships in the 10th semester of studies and choose a diploma thesis to work on, this will not be a problem.

## Communication with mentor

* By messages or calls, every 1-2 days, to discuss progress, daily plans, and problems.
* By messages, every 7 days, documenting progress done during the week.
* By git issues, about new problems, solutions, detailed approaches.

## Rating of difficulty
<!--

Please write below this comment either: easy, medium or hard

Please also adjust accordingly tags in post metadata.

-->

* New Livepose backend: Medium
* Real-time audio generation and synthesis: hard.
---
title: Replace OpenGL by a multi-API rendering library in Splash
slug: proposal-pedroansa1-replace-opengl-by-rendering-api-in-splash
author: Pedro Andrade Ferreira Sobrinho
date: 2023-03-10 12:08:11 UTC+01:00
tags: proposals, hard, 350 hours, Splash
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

Please your public contact information below this comment and add your name as value in post author metadata:

* name: Pedro Andrade Ferreira Sobrinho
* website: [Github](https://github.com/pedroansa) [Linkedin](https://linkedin.com/in/pedro-andrade-sobrinho/)
* gitlab username: pedroansa1
* timezone: UTC+01 

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title
A multi-API rendering library for Splash
<!-- 
Your title should be short, clear and interesting. 
The job of the title is to Please write below this comment eitherconvince the reviewer to read your synopsis. 
-->

## Synopsis


<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

[Splash](https://gitlab.com/sat-mtl/tools/splash/splash) is an open source mapping software that was developed by the SAT with the aim of mapping and calibrating videos generated by projectors. The software is versatile and capable of projecting 3D models while taking into account the geometry of the space where the projection is located. Furthermore, Splash can calibrate multiple video projectors to display images and videos on non-flat surfaces, making it an excellent tool for creating more complex 3D models.

Currently, Splash uses OpenGL to perform rendering operations. While OpenGL is a high-level API that abstracts much of the hardware-specific details, its implementation can vary depending on the platform being used. As such, it would be advantageous to support more systems and standardize independently from OpenGL. This would optimize Splash further and increase its flexibility.

To achieve this goal, a multi-API rendering library could be created, working as an intermediate for the methods and functionalities from OpenGL or other rendering APIs, enabling Splash to work seamlessly across multiple platforms. By creating a multi-API rendering library, Splash would become more versatile and accessible to a wider range of users, making it an even more valuable mapping and calibrating videos generated by projectors.


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Splash is a tool for projecting images useful in the development of new technologies, artistic expressions, and educational initiatives through computers. With Splash already open-source and supporting low-cost projectors, this proposed contribution to update Splash with a multi-API rendering library will bring the possibility to run Splash on low-cost embedded devices such as the Raspberry Pi 4. Our contribution thus brings the potential to democratize the use of this technology for the public and encourage more developers, artists and creators in general to contribute and produce more in this area, generating a virtuous circle of technical improvement, popularization of the tool, and discovery of new ways to use it.

For this to happen, it's essential that Splash is able to handle more graphic APIs and systems. After all, the more open it is to other systems, the more developers feel encouraged to work with it and more widespread and accessible it will be. It is precisely in this context that I present this proposal. Splash is explicitly dependent on OpenGL in much of its functionality, so it's necessary to isolate and abstract the direct OpenGL calls that Splash executes and replace them with an intermediate rendering API, ensuring a more flexible application and contribute to making it easier for people in the community to join the project and advance its development even further.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

1. Investigation

    * Evaluate existing codebase and identify all OpenGL calls
    * Research multi-platform rendering APIs (such as Vulkan, DirectX, or Metal) that can serve as an intermediate layer
    * Determine the best approach to integrate the new API into the existing codebase

2. Technical Paper

    * Produce a technical paper detailing the approach, benefits, and potential challenges of the project

3. Implementation

    * Isolate OpenGL calls from the rest of the of the project
    * Test and refine the implementation
    * Integrate the implementation into the existing codebase

4. Documentation

    * Produce documentation outlining the changes made to the codebase.
    * Provide instructions for how to build and run the application with the new rendering API as so how to use functionalities if somethings has changed.
    * Document any known issues or limitations

5. Optional Deliverables

    * Develop a prototype implementation of the new rendering API integration
    * Use the flexibility of the new implementation to add new features to the application

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

In the context of our project to replace OpenGL calls in our application with a multi-platform rendering API, we can draw inspiration and learn from the work done in related projects.

Blender, for example, went through a similar transition when they updated their rendering pipeline from OpenGL 2.1 to more modern OpenGL versions. This allowed them to take advantage of new features and improvements in the OpenGL API and open the possibility to integrate other technologies in their applications, such as Vulkan, but also required significant changes to their codebase. Blender is the perfect example to be based on it due to the strong dependency with OpenGl. Basically the developers had to redesign core functionalities and features to isolate OpenGL, which is the same challenge the Splash will face. Investigating and analyzing the approach taken by Blender in their transition could help us identify potential challenges and solutions for our own project.

Godot also provides a relevant example, as they recently implemented a Vulkan backend alongside their existing OpenGL backend, allowing Godot to take advantage of Vulkan's improved performance and better support for modern hardware, while still supporting older hardware that can only use OpenGL. Understanding how Godot implemented this dual backend approach could be beneficial as we investigate the best approach our contribution.

Overall, researching related work and projects such as Blender and Godot can provide valuable insights and inspiration as we embark on our own project.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I'm a Computer Science student at the State University of Campinas in Brazil, and I am currently involved in a double degree Engineering program at Telecom Paris. During my undergraduate studies in Brazil, I specialized in Computer Vision and Machine Learning, specifically in image processing. I have also published an article on the development of an Open Source natural image segmentation tool called Iboju for [Embrapa](https://www.embrapa.br/busca-de-publicacoes/-/publicacao/1135134/iboju-uma-ferramenta-de-anotacao-de-imagens-para-treinamento-de-detectores). 

At Telecom Paris, I'm specializing in image processing, 3D, and iterative systems, but focusing more in the last two and planning to apply for a Master's program in this field in Paris. I have experience with OpenGL, C++, and good knowledge of object-oriented programming. I believe that my participation in this project would be ideal and perfectly aligned with my skills and interests. Although I haven't yet explored the world of Open Source in this area, I believe that it is the initial gateway to a world of learning. I try to keep my Github updated with some experiments I do in college, as well as some games I develop on my own using either Unity or Java.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* C++: I'm in contact with C++ since the beginning of my university and I've had Object Oriented Programmation courses on C++ and Java and studied Data Structures in C and I was also able to develop several personal projects using C++ including OpenGL projects of my university.
* OpenGL: I've taken since september of 2022 computer graphics courses in the IGR master program of Telecom Paris using OpenGL to implement several algorithms in computer graphics. In my github page it can be found from the implementations of a Rigidbody to the implementation of the DFSH algorithm.
* Experience with game/rendering engine programming: I have also experience with game development, and have already used Unity and Godot to develop personal projects in addition to having developed a game from scratch using java API.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours. 
Please also adjust accordingly tags in post metadata.
-->


350 hours.

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard. 
Please also adjust accordingly tags in post metadata.
 -->

Hard.

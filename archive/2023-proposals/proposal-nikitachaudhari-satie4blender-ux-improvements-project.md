---
title: SATIE4Blender UX improvements
slug: proposal-nikitachaudhari-satie4blender-ux-improvements-project
author: Nikita Chaudhari 
date: 2022-03-20 11:24:11 UTC +5:30 
tags: proposals, medium, 350 hours, SATIE, satie4blender
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Nikita Chaudhari
* website: https://in.linkedin.com/in/nikitachaudhari100
* gitlab username: @NikitaChaudhari
* timezone: IST (GMT +5:30)

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->
### **SATIE4Blender UX improvements**


## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

[SAT](https://sat.qc.ca/en/discover-the-sat) is a world renowned organization that builds digital technology. At [Metalab](https://sat.qc.ca/en/metalab), their research laboratory, multiple tools have been created to help explore creative domains in digital art. SATIE is one such tool.

The proposed project aims to bring about changes in the UI of [SATIE4Blender](https://gitlab.com/sat-mtl/tools/satie/satie4blender/-/tree/master?ref_type=heads).
Satie4Blender is a [SATIE](https://gitlab.com/sat-mtl/tools/satie/satie) addon for [Blender](https://www.blender.org/) to allow interactions with SATIE, an audio spatialization engine. It aims to provide one possible way to prototype 3D scenes with spatial (i.e. multi-speaker), immersive audio. 
Audio spatialization is the process of creating a sense of space and location in sound. It can be used to enhance the listening experience by immersing the listener in a three-dimensional soundscape. This technology can be used in various applications such as music production, film sound design, and virtual reality experiences. SATIE is an audio spatialization engine that uses DSP graphs that are controlled by OSC messages and SATIE4Blender is a SATIE addon for Blender. Blender is an open-source 3D software that helps one make stellar animations. SATIE4Blender as a software provides powerful support and accessibility for audio-tech engineers and artists. 
In this project, the main focus will be on improving the User Interface of the addon to integrate it well with Blender's UI. The goal here is to work on the existing codebase and improve the existing functionalities. Another proposed tangent to this project can be an extra add-on that provides additional audio-visual functionalities integrations to Blender; this can either be integrated with SATIE or can be handled as a separate add-on.


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->



The satie4Blender project, which builds on existing code, has the potential to benefit artists from a range of creative fields. Improving the User Interface of satie4Blender would enable artists to interact more easily with SATIE and customize their desired audio and spatial settings, resulting in more precise and effective creative output.
This holds true for the amount of functionality Satie4blender has to offer too as better features will lead to better creative outcomes and smoother audio outputs.  Audio spatializers can prove extremely useful to game and app developers that decide to use Blender. Being open source, they won't have to fret about the cost and once the documentation is updated, using the add-on will be much easier. 
SATIE4Blender will allow animators to create an immersive and dynamic experience for the viewers/users through their animations.
Additionally, once the download process for every major platform is perfected, the users can easily use the addon from whichever operating system they chose. This widens the user base and helps us reach users from all sections of society and backgrounds. 
An easy-to-understand 'User Interface 'for the addon in Blender will allow people from novices to amateurs to professionals to make use of SATIE to enhance their animation.

This project will also help the music production community by letting producers add an element of spatial audio to their music by manipulating sound placement and movement in a 3D space. This holds true for film sound design too as using audio spatializing helps create a more immersive listenting experience for your film viewers. Satie4Blender can aid in creating more convincing and captivating stories with stellar auditory experiences.
Another area that greatly benefits from better audio spatialization technology is the world of Virtual Reality. Audio spatialization can be used to create a sense of presence and immersion in a virtual environment by creating a 3D soundscape that reacts to the user's movements and actions.




## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

#### Deliverables:

* Required: UX Research for SATIE.
* Required: Make UI Changes for satie4blender accoring to user requirements and mentor suggestions.
* Required: Make sure the addon installs easily on all major platforms.
* Required: Expand Documentation for satie4blender.
* Required: Document contributions in a paper and a blog(GSOC Requirement)
* Required: Present contributions at an International Conference.
* Optional: Enhance functionality of satie4blender. 


#### Timeline (DEADLINES):

***----Pre GSOC Period----------***
* February 22  - May 4
 ->Research on satie4blender.
 ->Go through code of pySATIE.
 -> Understand the OSC

***-----Community Bonding GSOC Period-----***
* May 4 - 28 2023
    -> Community Bonding Period | Get to know my mentors: Michal Seta, and Edu Meneses. 
    -> Read SATIE4Blender documentation and its code in detail and discuss the UI suggestions with the mentors while appearing for my university end-term presentations. 

***-----Getting accustomed to SATIE----***
* May 29 2023- July 10
    -> Getting acquainted, installations, research. 
    -> Blog my entire progress.

***----Coding Period--------***
* July 10 - 18:00 UTC
    * -> Mentors and GSoC contributors can begin submitting midterm evaluations
    * -> Submit Mid Term Evaluations.
    * -> Pitch ideas for addon feature enhancements or maybe new addon possibilities in similar 
        audio-visual domains.


* July 14 - 18:00 UTC
   *  -> Midterm evaluation deadline.
   *  -> Coding period.

* July 14 - August 21
   *  -> Work Period | GSoC contributors work on their project with guidance from Mentors
   *  -> Make sure the addon is installable on all major platforms: macOS, Windows and Linux.

* August 21 - 28 - 18:00 UTC
   *  -> Final week: Finish coding and conduct a final evaluation of UI changes. 

***----Final Evaluation--------***

* August 28 - September 4 - 18:00 UTC
   *  -> Final evaluation period. 
   *  -> Finish the documentation and blog on approval from mentors.

* September 5
   *  -> Initial results of Google Summer of Code 2023 Announced

* ***------Post GSOC------***
    * -> Keep enhancing the code base based on mentor suggestions and help document the changes and features for the paper (to present in the conference of SAT's choice).
    * -> Stay connected in the community and if possible help mentor the future batch of GSOC.
...

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->
* The target of this project is to enhance the existing SATIE4Blender addon and get it to install effortlessly on every major platform. 
Enhancements will be focused on the User Interface and the second priority will be on expanding the functionalities of the addon according to what time allows. 

#### **Related Work in this domain is listed below.**

Audio Spatializer: 
1. Microsoft's Spatial Audio Unity: 
CODE : https://github.com/microsoft/spatialaudio-unity
Details: Spatializer for Unity projects made using DSP Algorithms. It's written in Python. Microsoft's Spatial Audio Unity is a set of tools and plugins for the Unity game engine that enables developers to create immersive spatial audio experiences. The Spatial Audio Unity toolkit includes plugins for binaural rendering, ambisonics encoding and decoding, and acoustic simulation. It also includes an integration with Microsoft's HoloLens headset, which allows developers to create spatial audio experiences for mixed reality applications.

2. "Virtual Barber Shop" by QSound Labs - [This](https://www.youtube.com/watch?v=IUDTlvagjJA) is a popular audio spatialization demonstration that simulates the experience of being in a barber shop. The audio is spatialized using binaural audio techniques, which simulate the way that sound waves interact with the head and ears to create a sense of directionality and spatialization. The code for this project is not publicly available, but there are many open-source binaural audio libraries that can be used to create similar effects in code.

3. "Spatial Audio Toolkit" by BBC R&D - This is an open-source toolkit for spatial audio production developed by the BBC Research and Development team. The toolkit includes a range of tools for audio spatialization, including ambisonics encoding and decoding, binaural rendering, and room simulation. The code for this toolkit is available on GitHub: https://github.com/bbcrd/Spatial-Audio-Toolkit.

4. "Web Audio Spatialization" by Mozilla - This is a web-based audio spatialization demo that uses the Web Audio API to create spatialized audio effects in the browser. The demo includes examples of different spatialization techniques, including panning, ambisonics, and binaural audio. The code for this project is available on GitHub: https://github.com/mdn/webaudio-examples/tree/master/spatialization.


**Audio Related Add-Ons in Blender: (UI Enhancements Reference in Blender)**
1. Bizualizer: 
https://www.youtube.com/watch?v=fK9Umxy1AAs
https://github.com/doakey3/Bizualizer

2. AudViz:
https://blendermarket.com/products/audvis
CODE : https://github.com/example-sk/audvis

3. Complete List Of Addons in Blender: 
https://github.com/topics/blender-addon

* Other youtube links: 
https://www.youtube.com/watch?v=AOP0yARiW8U
https://www.youtube.com/watch?v=znX110kh170

* Currently there are no other audio spatialization addons in Blender apart from satie4blender hence the proposed work is completely different from any work done before. Inspiration can be taken from the other Blender addons that already exist. 

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->
Hello SAT, I'm Nikita Chaudhari, a fourth-year Computer Science & Engineering student from Mumbai. I'm keen on contributing to SAT through GSOC this year. The project "SATIE4Blender UX Improvements" caught my eye and I'd like to work on that. I'm a senior studying Computer Science & Engineering in India. I'm currently interning at The Principal Financial Group as an SWE Intern and I have already interned at Bitwise Global as a Machine Learning Intern for 5 months last year(June '22 - November '22). I've been hired by Deloitte for this upcoming season(November) after I graduate from college. This is my first time contributing to an open-source organization but I have a basic understanding of the CI/CD pipeline. I've been learning Blender and SuperCollider and admittedly am enjoying both the software and the language. I am fascinated by audio-visual technology and would be thrilled to have the opportunity to work and learn with SAT this summer, and explore Blender, SC, and SATIE. As part of my college studies, I am currently learning various programming languages including Python, Java, and C, and have a minor in AI&ML. Although I don't have previous experience with open source, I am eager to start my journey with SAT and would be grateful for the opportunity.I have a strong aptitude for technology and enjoy learning new skills quickly, which allows me to adapt to new tools and technologies efficiently. Currently, I'm also working with SCAAI(Symbiosis Centre of Applied Artificial Intelligence) and writing a paper with them on Natural Language Processing. I've already got two papers under process with two professors at my university: 
1. 1st paper: A Comparative Analysis of Indian Sign Language recognition using Deep learning models. 
2. 2nd paper: (Submitted to IEEE Access) "Autonomous Driving through Deep Reinforcement  
Communication: I'm a [Microsoft Student Learn Ambassador](https://studentambassadors.microsoft.com/) and have headed the Journalism Society of my university as the Editor in Chief. Communicating clearly with other contributors and mentors on an open-source project will not be a challenge for me when collaborating.


## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->


* **required: experience with Blender**
    - I've spent the past few weeks learning how to use Blender. I've gone through the various functionalities that Blender has to offer. I've added the link to the github repository where I've experimented with Blender. Understanding Blender helps me grasp this project much better and helps me understand the goals. 
    - Github Link : https://github.com/nikitachaudharicodes/BlenderAddOns
* **required: intermediate knowledge of Python**
    - I have worked with Python since my second year of college. I had to use this language extensively in my AI&ML minor specialization course in college and used it to build my projects. I've also done a few courses in Python and have a basic understanding of Data Structures. 
    * Github Repo Link: https://github.com/nikitachaudharicodes/Research-Paper-Summarizer
                        https://github.com/nikitachaudharicodes/Hotel-Reviews-Sentiment-Analysis-
                        https://github.com/nikitachaudharicodes/ISL-Recognition-Deep-Learning-ComparativeAnalysis-
* **preferred: experience with Blender's API for Python**
    - I've tinkered around with Blender this month and I learnt how to build addons using Python in Blender.
    * Github Repo Link: https://github.com/nikitachaudharicodes/BlenderAddOns
* **preferred: understanding of the fundamentals of digital arts technologies**
* **preferred: interest in audio-visual technologies** 
    * Audio-visual tech interests me as I have roots in music. I studied music theory as a kid and this idea of combining art and technology interests me.
* **preferred: willingness to learn some fundamentals of SuperCollider, upon which SATIE is built**
    * Github Repo Link: https://github.com/nikitachaudharicodes/SuperColliderLearn 
    
* Currently pursuing the **Google UX Design Professional Certificate**[https://www.coursera.org/professional-certificates/google-ux-design]. Completed 2 of 7 courses in the specialization. 
* Familiar with writing technical/research papers. 


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

#### 350 hours


 
## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->
#### Medium


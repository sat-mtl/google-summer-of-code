---
title: Windy City
slug: proposal-wanyileun-windy-city
author: Wan Yi Leung
date: 2023-03-28 11:24:11 UTC-05:00
tags: proposals, medium, 350 hours
type: rejected 
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Wan Yi Leung
* website: https://lilystillill.github.io/
* gitlab username: @wanyileun
* timezone: "Eastern Standard Time" (EST)

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->
Windy City


## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Windy City is a virtual dance WebXR application that allows users to dance in a virtual sandstorm environment using their webcam or phone camera. The user's dance moves are captured and processed in real-time using Media Pipe, and the sand particles in the environment, built with P5.js and A-Frame, interact with the user's movements, creating poetic ripples along their curves.

In Windy City, the sand storm environment is hostile and unpredictable, with sand particles swirling around the user as they dance. The user's motion will be transformed into an invisible body that is outlined by sand ripples; the sand ripples will flow over and trace the curves of their body. This interaction between the user and the sand storm environment highlights the independence between humans and our nature.

Windy City being a WebXR application allows users to easily access it from their browser on their computer or phone with a built-in camera. The user-friendly interface allows anyone to choreograph dance with the virtual sand storm without the need for any additional hardware.

Once the accuracy of the motion data representation and interactivity between the dancer and sand storm have been tested and refined, the A-Frame system can be extended to include other natural disasters such as heavy precipitation, flood, or hurricane.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Windy City is an interactive WebXR application that allows both dancers and non-dancers to submerge and experience the virtual sand storm. Our virtual interaction in the sand storm depicts the relationship between humans and nature. It raises awareness about climate change and its impact on us. Furthermore, Windy City's use of motion capture and particle systems offers a new way to explore and experience the unpredictable dynamics of natural phenomena like sandstorms.

As a Montreal-based artist, I see the potential for Windy City to be developed as a prototype for future performances at the SAT. The system could then be extended to include other natural disasters such as heavy precipitation, flood, or hurricane, and broaden the scope of research and potential applications. Collaborating with other artists and researchers at SAT would help to develop more robust immersive experiences that allow more people to experience the humbling power of natural disasters in virtual reality, making it a valuable tool for interactive data visualization for geoinformatics.

As a recreational dancer and performance artist, I can showcase the program's capabilities by performing a live dance piece using the platform at the end of the program. Windy City can be embedded into SAT's archive website where the online visiters can experience dancing in a virtual sand storm virtual.


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

May 4 to 28: Community Bonding Period

Milestones: 

- [ ] Test MediaPipe API in Python to accurately map and track joint landmarks on the human body during motion capture.
- [ ] Capture a stream of motion data from a live dancer through a webcam and clean and process the motion data using Media Pipe modules or Python libraries.
- [ ] Verify the accuracy and smoothness of the motion capture data and refine the motion capture pipeline parameters for more natural motion data.

Deliverables, required:
- [ ] A simple Python program using Media Pipe to capture motion capture data in real-time and with preliminary optimazation for a smooth and accurate motion replay. 
- [ ] Provide a refined timeline with milestones and deadlines based on the experience gained from creating the system.

- [ ] Optional: explore other input method of motion data: The project will focus on capturing motion capture data from video streams from webcam or phonecam. However, other motion capture hardware devices will also be explored to determine their compatibility with MediaPipe.


May 29: Coding officially begins!

May 29 - June 19, 3 weeks

Milestones:
- [ ] Research and implement methods for establishing secure connections between our WebXR application and users' web browsers, like HTTPS and/or WebRTC.
- [ ] Create the vitual environment using A-Frame, p5.js, and HTML/CSS. p5.js will be used to create the particle system for the sandstorm effect with A-Frame. 
- [ ] Then synchronize the motion data with the sand storm environment, so that the dancer's movements are accurately represented in the virtual world. 
- [ ] Continue to verify the accuracy of the motion capture data in the virtual world.

Deliverables, required:
Document the following codes and test cases on Github:
- [ ] Host a publicly accessible demo version of the WebXR application with a properly configured and functioning secure connection method.
- [ ] A basic p5.js sketch with a particle system rendered into the A-Frame environment as a vitual sand storm.
- [ ] A virtual scene of a dancer in a sand storm in a functional WebXR application that accurately transform and position the dancer's motion within the sand storm environment
- [ ] Improved code for the refinement of the motion capture pipeline with better accuracy and smoothness of the motion capture data.


June 20 - July 13, 3 weeks

Milestones:
- [ ] Add more complexity to the virtual sand storm by incorporating random motion and directiona, and adjusting the force and shape of the sand particles that define the curves of the dancer.
- [ ] Add post-processing effects like motion blur and depth of field to simulate the distortion caused by the sand storm.
- [ ] Adjust the position and orientation of the dancer using A-Frame's animation component.
- [ ] Continue testing and refining the synchronization of the motion data in the sand storm.

Deliverables, required:
Document the following codes and test cases on Github:
- [ ] An updated A-Frame applicaion with randomised sand motion and force using p5.js Perlin Noise and A-Frame's animation and physics components for an organic and natural-looking particle system.
- [ ] learn and use physics engine libraries, Matter.js or Cannon.js, for more advanced physics simulations, such as fluid and collision dynamics.


July 14 to August 4, 3 weeks: Work with mentors

Milestones for the first week:
- [ ] Customize the particle system to emit particles based on the motion data, with the particle velocity and gravity adjusted to create a natural sand storm effect. Therefore, the motion data can interact and control the sand storm by determining the position and movement of the dancer, which in turn affects the particles emitted by the particle system.
- [ ] Continue to test the motion data accurately synchronized within the virtual environment.

Deliverables, required:
Document the following codes and test cases on Github:
- [ ] A functional code repository that integrates all the necessary code and dependencies to integrate the motion capture pipeline, particle system, post-processing storm effects, and synchronized motion data representation into a fully synchronized WebXR application. The WebXR application will be able to accurately represent the dancer's motion within the sand storm environment in real-time.

Milestones for the last two weeks:
- [ ] Adapt the system to involve multiple users and present their interactions in the virtual sand storm. This involves modifying the motion capture pipeline to capture and process data from multiple sources, adjusting the particle system and post-processing effects for the individual body movements. Also, the synchronization of the motion data may be modified to accurately represent the interactions between multiple dancers.
- [ ] Test the platform with multiple users to ensure its performance and scalability.

Deliverables, required:
Document the following codes and test cases on Github:
- [ ] An updated code repository of that system that can accommodate multiple users and accurately display their interactions with the virtual sand storm.
- [ ] Multiple user test scenarios and results that reflect the platform performance.

August 5 to 20, 2 weeks

Milestones:
- [ ] Continue testing the platforms with multiple users. 
- [ ] Write documentation on how to use the system, including instructions on how to set up and run the program, and any technologies or libraries integrated into it.
- [ ] Continue to work on the version that can accommodate multiple users.
- [ ] Continue user testing to identify and address any issues or bugs in the system.
- [ ] Rehearse a live performance to demonstrate the program's functionality and showcase its features.


Deliverables, required: 
- [ ] A video recording of the live performance, showcasing the program's features and demonstrating its functionality.
- [ ] A final code repository with all the code, including documentation and test cases on Github.
- [ ] An fully functional code repository that can accommodate multiple users.


August 21 to 28: Submit final work product.

August 28 to September 4: Final evaluations




## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->
Comparing my proposed project to 'Living Archive' by Wayne McGregor, and 'Unwanted Water' by Unwired Dance Theatre, highlighting their similarities and differences.

'Living Archive' and 'Unwanted Water' are two innovative dance projects that use motion capture technology to enhance dance performances. 'Living Archive' is an interactive choreography tool that allows audiences to engage with McGregor's dance movement archive. It uses motion capture technology to record and analyze McGregor and his dancers' movements, inventing extended movement in real-time that enables users to create their own dance moves. 'Unwanted Water' is a live mocap collective virtual dance performance, where remote dancers can animate fully rigged virtual characters in real-time, interacting with the audience through a browser-based social VR platform.

In contrast, the proposed project has a very distinct feature that set it apart from the others. Windy City uses a particle system to emit particles based on the motion data of the dancer and adjusts particle velocity and gravity to create a realistic sandstorm effect, while Unwanted Water renders the motion data as 3D mannequins and Living Archive focuses on how human dancers incorporate AI-powered choreography.

The proposed project and 'Living Archive' share a similarity in that they both capture motion data from video streams of live webcam video streams. However, the differences lie in how the motion data is used. 'Living Archive' uses the motion data to feed their machine learning models to create infinite choreography possibilities and focuses on how human dancers incorporate AI-powered choreography. In contrast, the proposed project uses the motion data to simulate the movement of sand particles around the dancer. The particle system is customized to emit particles based on the motion data of the dancer, and the particle velocity and gravity are adjusted to create a sand storm effect that feels real and immersive. The proposed project focuses on the real-time dynamics of the live performers in the virtual sand storm.

The proposed project and 'Unwanted Water' share the use of a browser-based social VR platform that enables multiple users to interact in a virtual environment regardless of their physical locations. However, they differ in how they render the motion data. The proposed project utilizes WebXR to render the motion data into an invisible body outlined by sand ripples and shaped by gliding sand particles over the body, while 'Unwanted Water' renders the motion data as 3D mannequins. Another difference is that 'Unwanted Water' uses a mocap suit to acquire motion data, whereas the proposed project acquires it from video streams.

The proposed project is well-aligned with the mission of SAT, which aims to advance digital arts and foster interdisciplinary collaboration. By integrating cutting-edge technology and merging dance and game-like stories, the project offers an interactive story-telling virtual platform for both dancers and non-dancers.

Moreover, the proposed project can be further developed using Metalab originated technologies, such as LivePose and Switcher, to enahnce the immersive experiences. In the future, the project could be integrated into SAT's Satosphere immersive theater with a more advanced version for an in-person immersive experience. As a Montreal-based artist, I am eager to be part of the SAT community and benefit from its knowledge and guidance in the long run.



## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->
I am thrilled to apply to work with SAT for the GSOC program! As a data analytics student, a Fine Art Photography MFA graduate from Concordia University, and a recreational dancer, I have a unique set of skills that I believe make me an excellent candidate for this program. My MFA thesis focused on video performances that explored the dynamics of my encounters with men from dating apps. You can check out one of my performances here: https://youtu.be/OL6KQ-wg2nI.

I also worked as an assistant to media artist Don Ritter. I worked as the primary dancer and assisted him in his immersive interactive architectural projection, "Given." This experience enabled me to gain hands-on knowledge of capturing motion data from motion sensors and rendering it to control 3D characters in the virtual world. You can find more about "Given" at http://aesthetic-machinery.com/given-mud.html.

Last year, I completed a Front-End Web Development Certification and became proficient in using HTML and CSS to create layouts and content structure. I am excited to dive into A-frame, a tool that can develop a WebXr virtual world using HTML and CSS. You can check out my web portfolio here: https://lilystillill.github.io/.

Currently, I am in the process of gaining a data analyst certificate and becoming proficient in Python for data analysis, including data visualization with seaborn and Matplotlib, and making predictions using linear regression and multiple factor regression with scikit-learn. Additionally, I have experience using AI tools and tweaking existing codes to create AI-powered images. You can see some of my work here: https://objkt.com/collection/KT1Edyi5KYek1iWrCET7rNkrRVqYk118vHbg and here: https://opensea.io/collection/leghurts-solong.

With my current data learning and experience in photography and using AI tools, I am confident in my ability to troubleshoot and read documentation to be an independent learner in the first half of the program. I am also eager to work with my mentor to push my limits and develop my skills further.

I believe that this opportunity will be a significant milestone for me to begin working at the intersection of art and technology and become an active member of the vibrant art and tech community in Montreal. I am eager to continue pursuing innovation in art and technology with the SAT and through other opportunities in Montreal.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* required: some experience with HTML and CSS. My Front-End web porfolfio https://lilystillill.github.io/
* required: p5.js I made a quick sketch to review Perlin Field Flow: https://lilystillill.github.io/windy-city/
* preferred: Free and Open Source Software (FOSS): Pandas, Numpy, Seaborn, and Matplotlib in Python, Jupyter notebook, and VScode. 
DawsCon_Data_Journalism_Challenge
https://colab.research.google.com/drive/103Hrs4mGqPKHQ30TXWT_5y-E6B70BtMO
https://colab.research.google.com/drive/1n7hHQujjcdv803WAmDoNNezJIcr84vq7
* preferred: Intermediate level of Python to create data visulisation and deploy simple machine learning for CSV files
* preferred: interest in computer graphics: My photogrpahic work and design https://opensea.io/collection/leghurts-solong and https://objkt.com/collection/KT1Edyi5KYek1iWrCET7rNkrRVqYk118vHbg 


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->
350 hours



## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

medium

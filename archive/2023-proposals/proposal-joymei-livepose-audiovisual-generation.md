---
title: Using markerless motion capture for music generation, music creativity and music-dance interactions
slug: proposal-joymei-livepose-audiovisual-generation
author: Han Mei
date: 2023-04-04 22:24:11 UTC-08:00
tags: proposals, hard, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

Please your public contact information below this comment and add your name as value in post author metadata:

* name: Han Mei
* website: [joymei.fun](https://joymei.fun/)
* gitlab username: [JoyMei](https://gitlab.com/JoyMei)
* timezone:  GMT+8

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted.

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to Please write below this comment eitherconvince the reviewer to read your synopsis. 
-->

Using markerless motion capture for music generation, music creativity and music-dance interactions

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Music and dance are a way for human beings to express themselves. The two are closely related. Ballet, hip-hop, Kpop,  and different forms of artistic expression carry different emotional expressions. Through this project, we can explore more possibilities of music and dance, make styles more diverse, and enrich people's cultural life.

This is a new exploratory project aimed at utilizing real-time, markerless motion/pose capture software SAT's [Livepose](https://gitlab.com/sat-mtl/tools/livepose) , in combination with technologies such as  MediaPipe, OpenMMLab MMPose, as well as Jarvis/DeepLabCut, to create a framework that translates movements into real-time responsive sound, enabling dancers to control their music while generating improvised background music and even can driving novel gesture-controlled instruments in sync with the dance rhythm.

The development of such tools will facilitate both artistic creation, as well as scientific exploration of multiple areas, including for example - how people engage interactively with vision, sound, and movement and combine their respective latent creative spaces. Such a tool will also have therapeutic/rehabilitative applications in populations of people with limited ability to generate music and in whom agency and creativity in producing music have been shown to produce beneficial effects.



## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Music and dance are integral parts of human culture, capable of expressing emotions, conveying information, establishing connections, and creating beauty. They are not just entertainment activities but also cultural heritage and forms of bodily expression. 

The open-source nature of this project can promote the sharing and collaboration of technology and creativity, thereby driving technological development and innovation. This aligns with the mission of SAT, which is to democratize technologies to enable people to experience and author multisensory immersions. 

The project will foster artistic creation as well as scientific exploration in multiple fields. For instance, music and dance can be used for therapy and rehabilitation, helping people relieve stress, reduce pain, improve mental health, and promote physical recovery. 

We can also offer an innovative treatment approach based on music and dance to help children with autism express themselves better and enhance their social skills. Music and dance can help children establish emotional connections and boost self-confidence. This can also help promote the spread of this music and dance therapy approach to a wider audience, bringing more possibilities and opportunities to the field of treatment and rehabilitation.



## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Milestones

- Evaluate which pose backend implemented or to be implemented in [Livepose](https://gitlab.com/sat-mtl/tools/livepose) (among [Google MediaPipe](https://github.com/google/mediapipe) and [OpenMMLab MMPose](https://github.com/open-mmlab/mmpose) and [NVIDIA trt_pose with TensorRT](https://gitlab.com/sat-mtl/tools/forks/trt_pose), and towards potentially [Jarvis](https://jarvis-mocap.github.io/jarvis-docs/) / [DeepLabCut](https://deeplabcut.github.io/DeepLabCut/)) is best suited for low-latency audiovisual generation

- Implement or optimize the chosen pose backend (Python)

- Implement an audiovisual generation engine (Python)

- Implement a [LivePose output filter](https://gitlab.com/sat-mtl/tools/livepose/-/tree/develop/livepose/outputs): either embedding directly audiovisual generation (Python) or communicating with an external engine via inter-operatibility protocols (libmapper, OSC, websocket)

  

### Timeline

- **Pre GSOC Period**

  - Remain in contact with Suresh Krishna, Christian Frisson and Michal Seta to inquire further and obtain additional details about this topic.
  - Read music generation, Dance2Music field papers.

- **Community Bonding Period ( May 4, 2022 - May 28, 2023 )**

  - Learn more about SAT's Livepose and other projects.
  - Discuss approaches with the mentors and other Contributors

- **May 29th - June 12th (Coding Period)**

  - Extracting human body pose skeleton points with MMPose.
  - Computing the relative positions of the hand and leg skeleton points with respect to the neck, and generating a time-series of skeleton points.

- **June 12th - June 26th 2023**

  - Convert the skeleton point time series and MIDI music data into digital sequences, and use Melody RNN for training.
  - Apply LSTM and transformer series models, and try to improve the models

- **June 26th - July 10th 2023**

  - Explore multimodal deep learning methods, taking music sequences and bone point time series as different input modalities. Using a joint training or co-training approach, the two modalities are fused together for model training.

- **July 10th  - July 24th 2023 (Phase 1 Evaluation)**

  - Midterm evaluation.
  - Implement continuous integration using Gitlab CI/CD.

- **July 24th - August 7th 2023**

  - Reading the Music2Dance paper.
  - Training music sequence and human skeletal point sequence with DanceFormer and other models  to animate a 3D human model.

- **August 7th - August 21st 2023**

  - Collaborate closely with the team to identify and execute an application project that can serve as a means to evaluate the completed implementation.
  - Write a hands-on document.

- **August 21st 2022 - August 28th 2023 (Final Week)**

  - Complete the report and presentation
  - Share the results of the topic on social media.

- **Post GSOC Period**

  - Keep making required adjustments/incorporating fresh functionalities to the pipeline.

  - Stay closely connected with the team to participate in new initiatives.

    

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

#### Music Generation

**C-RNN-GAN: Continuous recurrent neural networks with adversarial training**

- Combining RNNs, which are used for processing sequence data, with GANs, which are used for data generation, to generate music by using two separate RNNs as D (discriminator) and G (generator).
- Incorporating feature matching loss to enhance diversity in the generated music.
- The key feature is to use a tuple similar to MIDI, consisting of four real numbers (frequency, length, intensity, timing) to represent each note event at every time step, which allows for end-to-end training using backpropagation.

**Music Transformer: Generating Music with Long-Term Structure**

- This work is mainly to improve the calculation and representation algorithm of relative positional information in Transformer, reducing the time complexity from $O(L^2D)$ to $O(LD)$, thus realizing a longer sequence of music generated, and greatly reduces memory consumption.

**MusicLM: Generating Music From Text**

- The paper introduces a model called MusicLM that generates high-quality music from text descriptions. 

- It accomplishes this through a hierarchical sequence-to-sequence modeling task, producing music at 24 kHz that remains consistent over several minutes.

#### Dance2Music

**Dance2Music: Automatic Dance-driven Music Generation**

- Two approaches are presented: a search-based offline approach that generates music after processing the entire dance video and an online approach that uses a deep neural network to generate music on-the-fly as the video proceeds.

- Dance is represented as a sequence of poses, with each pose consisting of 18 2D keypoints represented by a 36-dimensional continuous vector.

- The music is composed of piano notes in the C major pentatonic scale – C4, D4, E4, G4, and A4 – which are consonant with each other and found in most of the world's music.

#### Music2Dance

**DanceFormer: Music Conditioned 3D Dance Generation with Parametric Motion Transformer**

- Proposes a two-stage process for generating 3D dances from music: key pose generation and parametric motion curve prediction

- Proposes DanceFormer, a method that uses two cascading kinematics-enhanced transformer-guided networks to tackle each stage, respectively

- Introduces a new transformer decoder, called Kinematic Propagation Module (KPM), that enhances the capability to predict coherent and smooth movements

- Proposes a structured multi-head attention module, called DanTrans, which can be applied to other related tasks

  

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a Master student in [Biomedical Informatics](https://medicine.nus.edu.sg/dbmi/) at the National University of Singapore. Programming languages that I am well-versed in include Python, C, C++, TypeScript/JavaScript, and MATLAB. My background includes undergraduate studies in Electronic Information Engineering and two years of frontend software development experience, and I am well-versed in Computer Vision, Biomedical Informatics, and WebXR.

Human-computer interaction is a topic that greatly fascinates me. I have a great curiosity and interest in the field of Mixed Reality. At present, I am an intern at the NUHS holomedicine project. I am researching the optimization of interactions between hands and objects and exploring the applications of HoloLens 2 in clinical care.

Although I am new to open-source development, I am enthusiastic about contributing to projects and learning from experienced developers. I have contributed to the open-source community [OpenMMLab](https://openmmlab.com/) and active in [MMPose](https://github.com/open-mmlab/mmpose) and [MMDet3D](https://github.com/open-mmlab/mmdetection3d).

In addition to my passion for technology, I am also an avid music and dance enthusiast. I believe that music and dance provide great outlets for expressing emotions and connecting with nature.

Moreover, while this project holds immense prospects in entertainment scenes like somatosensory interactive games, I am especially thrilled about its potential in the healthcare industry. 

Music and dance can provide therapeutic benefits, such as reducing anxiety, stress, and depression. These benefits can be particularly relevant for patients dealing with chronic illnesses, mental health issues, or those undergoing medical procedures. In addition, dance therapy can help patients with neurological conditions, such as Parkinson's disease or stroke, to improve their movement and cognitive function. Studies have also shown that music and dance can help improve memory and cognitive function in patients with conditions such as dementia or Alzheimer's disease.

The use of dance and music generation in healthcare can provide numerous benefits for patients and healthcare providers, improving patient outcomes and quality of life.



## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

- ### Required: 

  - Python: 

    NUS'S IT5001(Software Development Fundamentals), IT5003(Data Structures and Algorithms), IT5005(Artificial Intelligence), IT5006(Fundamentals of Data Analytics) all taught in Python.

  - Computer Vision, Image/video processing audiovisual displays:

    Computer vision is my primary research focus. My interests lie in human pose recognition, medical imaging, and video object tracking.

    [Hand and Object Recognition](https://www.bilibili.com/video/BV1r34y1f7xT/?spm_id_from=333.999.0.0&vd_source=85676c1838f3b22c2f54cbf142081488)

    [Medical Image](https://github.com/JoyMei/Medical-Image)

  - Deep Learning: 

    Familiar with PyTorch, Tensorflow, PaddlePaddle.

    Participated in Cambridge AI summer school in 2018, got fully funded scholarship.

- ### Preferred:

  - MediaPipe:

    Google's [MediaPipe](https://github.com/google/mediapipe) enables live ML processing in the browser. Here is an example in my personal website:

    [Mediapipe FaceMesh Landmarks](https://joymei.fun/projects/web/2)

  - [MMPose](https://github.com/open-mmlab/mmpose): 

    I made code contributions to the visualization of RTMPose and style replacement for COCO OpenPose. And learning MMHuman3D at the same time.


  - C/C++ :

    Unity and simple game making experience 

  - Low-latency sound generation:

    No experience in low-latency sound generation but I have experience in LSTM and Transformer. I have a Bachelor's degree in Electronic Information Engineering and studied Signal Processing course. I can quickly be familiar with this field. You can find my interest in music and art from this project:

    [Make Art Sing - PaddleGAN Wav2lip Lip Sync La La Land - "City of Stars"](https://www.bilibili.com/video/BV1CY4y1q7Fu/?spm_id_from=333.999.0.0)

  - Mixed Reality:

    Currently, I am an intern at National University Health System (NUHS) [Holomedicine](https://www.youtube.com/watch?v=ddJI1yiMzlc) project. 

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours. 
Please also adjust accordingly tags in post metadata.
-->

350 hours. 

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard. 
Please also adjust accordingly tags in post metadata.
 -->

Hard
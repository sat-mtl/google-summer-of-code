---
title: Haptic to audio interaction design with Feelix supporting TorqueTuner 
slug: proposal-piaoziyue-haptic2audio-feelix-torquetuner
author: Ziyue Piao
date: 2023-03-22 11:24:11 UTC-05:00
tags: proposals, hard, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Ziyue Piao
* website: https://piaoziyue.github.io/
* gitlab username: [piaoziyue](https://github.com/piaoziyue)
* timezone: UTC-05:00

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->
Haptic to audio interaction design with [Feelix](https://feelix.xyz/) supporting [TorqueTuner](https://github.com/idmil/torquetuner) 


## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

In the field of immersive art, the interaction between visual, auditory, and tactile are crucial to the immersive experience. However, the mapping between haptic and audio is yet to be fully explored, which affects the user's immersion, artists' expression through their work, and their communication with participants. Two main reasons that restrict the development of the area are 1) the lack of tools for designing haptic and audio interactions on existing devices and 2) the lack of understanding of the relationship between haptic and audio in designing new interactions. 

To stimulate the emergence of immersive experiences, SAT is developing the [Haptic Floor](https://sat.qc.ca/en/haptic-floor) for groups of participants with individual force feedback. This GSoC project aims to explore haptic and audio interaction using a low-cost affordable device with 1 degree of freedom, the [TorqueTuner](https://github.com/idmil/torquetuner). Building on the haptic authoring editing tool [Feelix](https://feelix.xyz/), we propose to add support for TorqueTuner and an auditory editing toolbox to enable haptic and audio interaction in Feelix. The audio editing toolbox has the capability to synthesize sound by combining [ForceHost](https://gitlab.com/ForceHost), an open-source toolchain for authoring and rendering audio and haptics, with force feedback from TorqueTuner. In the near future, Haptic Floors and more haptic devices could be utilized in immersive art spaces. 


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The project aiming at exploring the interaction between haptic to audio would allow artists to familiarize themselves with multimodal interaction design and display their creativity with our new design tool. Then we can understand more about the science of cross-modality correlation to facilitate more immersive artistic creation. Our project could lead to new immersive art that has the potential to make a significant impact on various industries, such as gaming, entertainment, education, and more fields. Ultimately, this contribution can lead to research that has the potential to change the way we experience the world around us, and that is something worth exploring.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Implementation Goals
This is an approximate implementation plan encompassing all major technical priorities, to be completed before the end of August.

* Add support of TorqueTuner in [Feelix](https://feelix.xyz/)
* Add an audio synthesis control toolbar in [Feelix](https://feelix.xyz/) through ForceHost
* Support the export of audio and haptic data in 
* Update the user's guide on [Feelix](https://feelix.xyz/)’s documentation with new functions
* Create a video of a demo of the project

### Timeline (approx. monthly)

May 4 - June 12

* Community bonding period: Decide on mode and frequency of meeting, documents for meeting and contribution, expectation and plan
* Familiar with the source code of [Feelix](https://feelix.xyz/), [TorqueTuner](https://github.com/idmil/torquetuner), and [ForceHost](https://gitlab.com/ForceHost)
* Specify the functions and implementation plan 

May 29

* Coding officially begins!

June 13 - July 13

* Investigative more research on haptic-audio interaction
* Add audio toolbox from existing visual components of [Feelix](https://feelix.xyz/) 
* Add support of [TorqueTuner](https://github.com/idmil/torquetuner) to [Feelix](https://feelix.xyz/) through [ForceHost](https://gitlab.com/ForceHost) and play with its haptic feedback design 

July 10- July 14

* Midterm evaluation

July 14- August 21

* Add audio effects and design the mapping
* Set some preset mapping between haptic contour to audio effects
* (Optional) Support the haptic feedback of vibrotactile in [Feelix](https://feelix.xyz/) 
* Begin researching potential academic outlets (journals, conferences, etc.) which may be receptive to our topic

August 22 - August 28

* Finish all implementation in the plan
* Produce a demo video depicting our newly designed interface

September - October

* Continue refining and finalizing the codebase and documentation, merge all relevant work
* Write a paper based on work during the project

October - November

* Finalize paper/presentation and submit to a variety of outlets suitable to our topic


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->
The aim of the GSoC project is to contribute to augmenting [Feelix](https://feelix.xyz/)'s user interface to support artists in designing haptic to audio interaction based on [TorqueTuner](https://github.com/idmil/torquetuner), a rotary force-feedback device that can be dynamically modified its rotary force feedback in real-time. [Feelix](https://feelix.xyz/) is a haptic authoring tool developed to support the design of force feedback and motion in user interfaces [(Van Oosterhout et al., 2020)](https://dl.acm.org/doi/abs/10.1145/3382507.3418819?casa_token=E7cQIDoxSr4AAAAA:4j-GOXxCE_9QEJH-W-vO3NxaL7D9faBzHnldayzOXlGzMNJXeaJz0KvpVlRbuKhHLf_QE57Dhs3CaGw). 

In recent years, many researchers are exploring different methods and techniques for haptic and audio interaction including in the [HAID](https://haid2022.qmul.ac.uk/haid-history/) community. Many related works explore creative solutions for authoring haptic feedback and creating authoring tools. Two categories related to force-feedback and sound interaction are physical modeling from solving differential equations and signal modeling by editing transfer functions [(Frisson et al., 2022)](https://nime.pubpub.org/pub/jtdpakvp/release/1). Physical modeling is used in designing applications that relate to real physical interactions, such as plucking, bowing, and striking, for example, Dimple [(Sinclair & Wanderley, 2009)](https://doi.org/10.1016/j.intcom.2008.10.012), and MIPhysics [(Leonard & Villeneuve, 2019)](https://hal.science/hal-02015740/). For signal modeling, many frameworks do not support audio synthesis, but their features are inspired by audio tools. We aim at exploring the potential of signal modeling that couples force feedback and sound synthesis using our interactive interface.

In audio and haptic interaction, most efforts tend to either treat haptics as secondary to audio, or audio as secondary to haptics, and the design of the audio component typically precedes the tactile component in music applications [(Regimbal & Wanderley, 2021)](https://nime.pubpub.org/pub/zd2z1evu/release/1). In this GSoC project, we focus on the audio as secondary to haptics which means the force feedback has been designed in Feelix and the designer wants to match an appropriate audio variation. We propose to add an audio editing toolbar to Feelix, which requires combining the [ForceHost](https://gitlab.com/ForceHost) to complete real-time processing of audio effects. [ForceHost](https://gitlab.com/ForceHost) is an open source firmware that hosts authoring and rendering of force-feedback and audio signals and that communicates through I2C with guest motor and sensor boards. 


## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a first-year Ph.D. in music technology at [IDMIL](https://www.idmil.org/), McGill University, supervised by Marcelo Wanderley (music technology) and Isabelle Cossete (music education). I have an engineering background in digital media technology, and more than 7 years of experience in C/C++, Python, and interface design. Furthermore, I have worked as a research assistant with Roger Dannenberg on music information retrieval in 2021-2022, and with Gus Xia at the Music X Lab, NYU Shanghai on singing tutoring interfaces in 2020-2022. I have published two [NIME](https://www.nime.org/) papers as the first and second author [NIME2021](https://nime.pubpub.org/pub/cgi7t0ta/release/9), [NIME2022](https://nime.pubpub.org/pub/orgf5hrv/release/1?readingCollection=8d5ef7ab). I have a strong interest in exploring cross modality mapping between haptic and audio which will also become part of my Ph.D. thesis. Besides, I participated in performances as a soprano classical singer and completed degrees in communication study and economics, which underscores my interdisciplinary ability in human-computer interaction, humanity, and art.


## Skills (some projects with multiple skills are replicated)

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* Required: experience with C++ and ESP32 

    * [A music editing interface with TorqueTuner](https://github.com/piaoziyue/TorqueTuner_MUMT609) (in processing): This is a personal project in McGill's haptic project course using ESP32, C++, and TorqueTuner
    * [Singing force-feedback tutoring interface](https://nime.pubpub.org/pub/cgi7t0ta/release/9): This was the first NIME21 paper as a co-author using ESP32 and C++.

* Required: experience with TypeScript/JavaScript 
  
    * [A music editing interface with TorqueTuner](https://github.com/piaoziyue/TorqueTuner_MUMT609) : This course project also uses JavaScript, P5.js, Tone.js, and Node.js for the web server and the midi sequencer.

* Preferred: experience with Electron and Angular 

    No experience in Electron and Angular, but I have experience in JavaScript, and I am reading the documentation. I can quickly be familiar with it.

* Preferred: experience with haptics and audio interaction design
  
    * [MappEMG](https://github.com/IDMIL/MappEMG): This is a live-stream MappEMG pipeline which can let audiences feel the performer’s muscle effort through smartphone vibration. I developed the new system using bitalino EMG with python and designed the mapping between EMG to vibration.
    * [Multimodal singing tutoring interface](https://nime.pubpub.org/pub/orgf5hrv/release/1?readingCollection=8d5ef7ab): This is a personal project: an interface that can detect user's singing voice and body expansion, and give real-time visual guidance on pitch, rhythm, and breathing.
    * Finished a Stanford CCRMA summer workshop aiming at learning FAUST in 2021.



## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

350 hours


## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->
hard

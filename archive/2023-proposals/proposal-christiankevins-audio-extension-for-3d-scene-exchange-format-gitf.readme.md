---
title: Audio extension for the 3D scene exchange format glTF
slug: proposal-christiankevins-audio-extension-for-3d-scene-exchange-format-gitf
author: Christian Kevin Sidharta
date: 2023-03-24 20:35:11 UTC-04:00
tags: proposals, medium, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Christian Kevin Sidharta
* website: https://github.com/christiankevins
* gitlab username: christiankevins
* timezone: GMT -4

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Audio Extension For 3D Scene Exchange Format GITF

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

In this project, I propose to develop an audio extension for the 3D Scene Exchange Format (gITF). The extension will allow for the inclusion of audio data within the gITF file format, thus enabling the creation of 3D animated scenes with synchronized audio. The gITF file format is widely used in web-based applications, and its integration with audio will provide a richer and more immersive experience for users.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The extension will allow for the seamless inclusion of audio data within the GIF file format, creating a synchronized and immersive experience for users. With Google and SAT's sponsorship, this project will demonstrate the capability of open-source software to push the boundaries of web-based applications and contribute to the advancement of 3D animation and design tools.

The integration of audio into the glTF file format is a significant development that will bring a range of benefits to the community of game engines and web-based applications. Firstly, it will enhance the user experience by creating more engaging and immersive 3D scenes. This means that users will be able to enjoy richer and more interactive content, which will ultimately improve their overall experience. Moreover, it will provide developers with a new tool to create more dynamic and interesting applications regarding the sound experience, such as games, virtual reality experiences, and educational resources. Lastly it will allow for more integrated workflows when it comes to designing audio scenes, with the possibility to create a scene in a given software (Blender for example, through a dedicated add-on like [satie4blender](https://gitlab.com/sat-mtl/tools/satie/satie4blender)), export it in glTF and import it in another software (like [Godot](https://godotengine.org/))
Another benefit of this project is that it will contribute to the development of open-source software. This will enable more people to contribute to the development of new technologies and create more innovative and impactful applications.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->
The proposed project involves developing an audio extension for the 3D Scene Exchange Format (glTF) to enable the creation of 3D animated scenes with synchronized audio. The project plan is divided into 13 weeks and covers various stages, including project planning and setup, investigation and research, defining the audio extension structure and metadata requirements, developing the audio extension prototype, integrating the prototype with vaRays and Blender tools, reviewing the specification, optimizing the implementation, final testing and documentation, user manuals and tutorials, and final evaluation and submission.

### Milestones and Deadlines

#### Week 1 (May 29 - June 4): Project Planning and Setup

- Meet with the mentor to discuss the project goals, timeline, and expectations 
- Set up the development environment and tools

#### Week 2 (June 5 - June 11): Investigation and Research

- Research existing audio scene description formats
- Evaluate their strengths and weaknesses
- Evaluate other existing glTF extensions specifications, for reference
- Identify potential design options for the audio extension in glTF format

#### Week 3 (June 12 - June 18): Define the Audio Extension Structure and Metadata Requirements

- Define the structure and components of the audio extension in glTF format
- Create a draft of the extension schema
- Identify metadata requirements for the audio extension
- Ensure compatibility with existing audio formats and tools
- Update the extension schema

#### Week 4 - 6 (June 19 - July 9): Develop the audio extension prototype for glTF format

- Review the glTF specification and documentation to understand the requirements for the audio extension.
- Create a prototype implementation of the audio extension using your preferred programming language and tools.
- Test the prototype to ensure that it meets the requirements and produces valid glTF files.
- Test the implementation using different combinations of audio file types, sizes, and bitrates to ensure that it can handle a range of input data.
- Use a tool like the Khronos glTF Validator to ensure that the resulting glTF files are valid and conform to the specification.

#### Week 7 - 8 (July 10 - July 23): Integrate the audio extension prototype with the vaRays and Blender tools by implementing the necessary changes and updates.

- Review the documentation for vaRays and Blender to understand how to extend these tools with new functionality.
- Modify the existing [vaRays](https://gitlab.com/sat-mtl/tools/vaRays), [SATIE](http://sat-mtl.gitlab.io/documentation/satie) and [Blender](blender.org) codebase to support the audio extension. Consider adding new UI elements to enable users to add and configure audio data within the 3D scene.
- Test the integration to ensure that it is functional and does not introduce any new issues.
- Test the implementation for compliance with the defined specification:

#### Week 9 (July 24 - July 30): Review the specification for the audio extension to understand the requirements and constraints.

- Create a suite of tests to validate that the implementation meets the specification.
- Run the tests and review the results to identify any issues or areas for improvement.
- Iterate on the implementation and tests until all requirements are met.
- Optimize the implementation for performance and memory usage:

#### Week 10 (July 31 - August 6) :Profile the implementation to identify any performance or memory usage issues.

- Use optimization techniques such as caching, precomputation, or algorithmic improvements to improve performance and reduce memory usage.
- Test the optimized implementation to ensure that it still meets the specification and is functional.

#### Week 11 (August 14 - August 20): Final Testing and Documentation

- Test the implementation for compliance with the defined specification
- Complete the documentation of the specification, implementation, and usage of the audio extension in glTF format

#### Week 12 (August 21 - August 27): User Manuals and Tutorials

- Provide user manuals and tutorials for vaRays, SATIE and Blender tools

#### Week 13 (August 28): Final Evaluation and Submission

- Finalize documentation and ensure its maintainability
- Submit the final product of the project to the mentor and GSoC administrators

It's important to note that the timeline is subject to change depending on the progress of the project and feedback from the mentor and GSoC administrators. Therefore, it's crucial to remain flexible and adjust the timeline accordingly.


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The proposed project aims to create an audio extension for the 3D scene exchange format glTF. While glTF is a widely used format for exchanging 3D scenes, it does not support the audio component, which makes it difficult to exchange complex 3D scenes that also include sound. There are also currently no widely accepted standards for integrating audio data into the gITF file format. The proposed project aims to address this limitation by defining a dedicated extension that describes the sound aspects of a 3D scene and developing an implementation example.

The proposed project is unique and different from similar related work because it aims to integrate audio data into the glTF format, which is widely used for 3D scene description. Although there are other audio scene description formats, such as Audio Definition Model (ADM) and MPEG-H, they are not specifically designed to integrate with glTF. Another related works including existing audio scene description formats such as OpenAL and AudioSceneDescriptionFormat (ASDF). OpenAL is a cross-platform 3D audio API that provides a standardized way of describing 3D sound environments, including positional audio, attenuation, and sound reflection. On the other hand, ASDF is a file format that is designed to exchange and archive sound scenes that can be used for 3D sound design, rendering, and mixing.


While these existing formats provide ways to describe 3D sound environments, they are not specifically designed to be integrated with glTF. Therefore, the proposed work is different from existing related work in that it aims to define an extension that is specifically designed to integrate with glTF, making it easier to exchange 3D scenes that include both sound and graphics.
Moreover, the proposed work also includes adding support for the extension in the tools developed/used by the Metalab, including vaRays and Blender. This integration will allow users of these tools to take advantage of the new audio extension in glTF, making it easier to work with complex 3D scenes that include both sound and graphics. It also includes optimizing the implementation for performance and memory usage, which is important for real-time applications and AR/VR experiences. 
In summary, while there are existing audio scene description formats such as OpenAL and ASDF, they are not specifically designed to integrate with glTF. The proposed work aims to define an extension that is specifically designed to integrate with glTF, making it easier to exchange complex 3D scenes that include both sound and graphics. 


## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

Hi! I am Christian Kevin Sidharta, a computer science undergraduate studying at the Unviersity of Toronto with a full-ride scholarship. I have experience working on a variety of software projects. 

### Related experience

Full Stack Developer Intern (Investree) July 2022 - August 2022
- Developed centaur fintech startup website providing peer-to-peer lending financing platform
- Created API to provide front-end and back-end communication for data retrieval and delivery
- Implemented Blade Template to make front-end appearance while taking data from the back end
- Optimized SQL queries and ran tests to see the performance of a query

### Open source projects

University of Toronto GDSC Open Source Project. 
- Developed University of Toronto's polling system

### Skills
Python, C++, Java, HTML, CSS, Matplotlib, Pandas, NumPy, VB. NET, R, Laravel, PHP, SQL

I am excited about the opportunity to contribute to the development of open-source software and to learn new skills in the process.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->
SKILLS: 
#### Python 
University of Toronto CSC course. Projects done in python included Huffman Tree implementation algorithm program. Huffman tree algorithm is a widely used algorithm in data compression to create an optimal prefix-free binary code for a set of characters. 
#### C++
During High School, I participated in Competitive Programming Club and joined Competitive Programming courses. Several programming problems that I did can be seen in my github.
https://github.com/christiankevins
#### Free and Open Source Software (FOSS)
Participation in UofT GDSC open source project. I helped develop University of Toronto MCS polling system
#### Linux
Proficient in Linux operating system (Ubuntu) and knowledge of open-source software and tools commonly used in Linux environments, such as Apache, MySQL, and Git as a Full Stack Developer Intern at Investree

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

medium

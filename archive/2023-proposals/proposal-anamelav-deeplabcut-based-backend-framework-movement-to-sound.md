---
title: Implementing a DeepLabCut-based Framework for Translating Movement into Sound
slug: proposal-anamelav-deeplabcut-based-backend-framework-movement-to-sound
author: Mana Vale
date: 2022-04-04 12:39:00 UTC-06:00
tags: proposals, hard, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Mana Vale
* website: [LinkedIn](http://linkedin.com/in/manavale) [Github](https://github.com/AnamElav)
* gitlab username: AnamElav
* timezone: UTC-06:00

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Implementing a DeepLabCut-based Backend Framework for Translating Movement into Sound

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The intersection between human music and dance is a fascinating area with great potential for artistic expression, scientific research, and improved mental health. The goal of this project will be to utilize Python and DeepLabCut, a deep learning toolbox for markerless pose estimation, to track and process real-time movements, translating them to a format in which they can be used to generate low-latency sound. Part of this project will involve comparing DeepLabCut with other pose tracking softwares to determine an optimal backend framework for the overall project of creating software for music generation through movement. The major work of this project will involve tracking and classifying various bodily movements (e.g. lifting/lowering arms/legs, stretching, side-to-side arm movements).

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

This project has three main points of impact. Firstly, enabling music generation via a medium as fundamental and accessible as movement will empower creativity in the performing arts, allowing people of all ages and artistic backgrounds to experiment with this novel form of art. Secondly, there is ongoing research about the benefits of music and dance, and creating technology that blends the two performing arts will open the doors to additional research about the interplay of the two creative forces and their impact on the body and mind. This is a segue into the last point of impact, which will allow individuals with Alzheimer’s, autism, and other disorders known to be aided by music and dance to engage in a therapeutic and self-empowering activity.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

* April 4th - May 4th:
    - Re-familiarize myself with DeepLabCut
    - Understand the capabilities and limitations of DLC

* May 4th - May 28th (Community Bonding Period):
    - Establish regular check-in points and meeting times with mentors
    - Understand mentor expectations & re-evaluate project objectives
    - Understand what existing work has been done for the overarching project & gain basic familiarity with any unknown concepts
    - Read relevant papers to understand related projects and identify what worked/didn’t work/could be improved

* May 29th (Coding Begins):
    - Start training with 3D DeepLabCut

* June 14th:
    - Narrow down optimal training parameters for DNN

* July 17th - 19th: 
    - Finish writing Python to classify movements 
    - Produce meaningful graphs/visuals
    - Run model on additional video data

* August 1st:
    - Hard deadline for any of the July tasks not completed
    - Compare DLC results to Livepose/other results
    - Re-evaluate objectives with mentors 

* August 18 - 22:
    - Wrap up project
    - Finalize project writeup


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

DeepLabCut, based on the human pose estimation model [DeepCut](https://arxiv.org/abs/1605.03170), has been shown to be a highly effective pose estimation tool for both humans and various species like rodents, insects, and fish [citation](https://www.nature.com/articles/s41596-019-0176-0). Unlike other 3D pose estimation software like [Jarvis](https://jarvis-mocap.github.io/jarvis-docs/) and [dannce](https://github.com/spoonsso/dannce), DeepLabCut is capable of real-time processing in addition to [3D pose estimation](https://deeplabcut.github.io/DeepLabCut/docs/Overviewof3D.html#:~:text=In%20this%20repo%20we%20directly,(and%20a%203D%20dataset).). Additionally, it prioritizes low latency (with real-time delays as low as [10 ms](https://elifesciences.org/articles/61909)) and has been a popular toolbox due to its ability to utilize transfer learning and DNNs to accurately estimate poses with minimal training data [citation](https://static1.squarespace.com/static/57f6d51c9f74566f55ecf271/t/605f8469b0d20a364dee3721/1616872562090/s41593-018-0209-y.pdf). DeepLabCut is therefore a powerful potential backend to explore for this project. 

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a freshman at Rice University in Houston, TX, where I am studying Computer Science and Cognitive Sciences. I have experience with Python, Java, Discrete Math, DS&A, OOP, Deep Learning, Neural Nets, App Development, and Design, through a combination of rigorous coursework and independent projects. I am personally motivated by this GSoC project as a musician and an active dancer of 10+ years. The intersection of dance and technology, two of my favorite fields, is an area I have not ventured into previously and I am excited at the prospect of contributing to software that is especially meaningful to me. 

I have previously utilized the DeepLabCut toolbox to investigate the role of the MPOA-Gal circuit in mice parental behaviors (Dulac Lab, 2021). I used transfer learning and deep convolutional neural networks to estimate mouse poses with DeepLabCut, processing several hours of videos of mice in a laboratory environment, and tracking keypoints in each frame. Then I wrote Python code to interpret the keypoints across frames as distinct parental behaviors, producing ethogram graphs that visually summarize the various behaviors exhibited in each video. In the process, I also formulated methods to correlate adjacent frames to mitigate errors introduced by the AI processing step.


## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* Python
    - Pandas
    - Numpy
    - Sci-kit
    - Tensorflow
* DeepLabCut
* Strong communicator & excellent written skills

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

hard

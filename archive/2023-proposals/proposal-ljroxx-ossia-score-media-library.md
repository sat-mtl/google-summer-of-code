---
title: Interfaces for Media Library Exploration in OSSIA Score (IMLEOS)
slug: proposal-ljroxx-ossia-score-media-library
author: Lakshay Jain
date: 2023-04-01 16:35:05 IST-05:30
tags: proposals, medium, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Lakshay Jain
* website: https://drive.google.com/file/d/1QzRrFZy2LDbDP4ABIkfpA-vp9AvVpctt/view?usp=sharing
* gitlab username: ljroxx
* timezone: (GMT+5:30) Asia/Kolkata – IST

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Interfaces for Media Library Exploration in OSSIA Score (IMLEOS)

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The aim of this project is to develop intuitive and user-friendly interfaces for media library exploration in OSSIA Score. The interfaces will enable users to browse, search, preview, and select media resources from different sources, including local files and online repositories, and integrate them into their scores seamlessly. The project will involve the following tasks:

Designing and implementing a modular architecture for media library management and integration in OSSIA Score, based on the existing plugin system.
Developing a graphical user interface (GUI) for media library exploration, using Qt and OpenGL technologies.
Implementing various media preview and playback functionalities, including waveform displays and video thumbnails, using suitable libraries and frameworks.
Integrating popular media repositories, such as Freesound and Wikimedia Commons, into the media library interface, using their respective APIs and protocols.
Providing documentation and examples to guide users in the use of the new interfaces and features.

The proposed project will benefit OSSIA Score users by making it easier and faster to find and use media resources in their projects, and by providing more creative possibilities for multimedia composition and performance. It will also contribute to the development of open-source multimedia tools and standards, and enhance the interoperability and collaboration between artists and researchers worldwide.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

Google and SAT would be proud to sponsor this project because it would provide a significant enhancement to the OSSIA Score platform, making it more accessible and user-friendly for media library exploration. The project would showcase the potential of open-source software development and community-driven innovation, as it would involve the collaboration of developers, designers, and users from diverse backgrounds and skill sets.
The project would benefit society as a whole by democratizing access to media resources and fostering creativity and innovation in the arts and multimedia fields. By providing a powerful and easy-to-use platform for media library exploration and integration, the project would enable musicians, composers, filmmakers, and multimedia artists to express their ideas and visions more effectively and efficiently.
Some of the cool things that would be demonstrated through this project include:
Seamless integration of media resources into musical scores and multimedia projects, enabling users to explore and experiment with different soundscapes and visual elements.
Real-time media preview and playback capabilities, allowing users to visualize and audition media resources before integrating them into their projects.
Interactive and intuitive user interfaces for media library exploration and selection, featuring rich visual and interactive elements.
Integration of online media repositories, providing users with a vast and diverse collection of media resources to explore and integrate into their projects

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

A modular media library plugin for OSSIA Score, with core functionalities such as media indexing, metadata management, and search capabilities.
A user-friendly and customizable GUI for the media library plugin, based on Qt and OpenGL, with media preview and playback functionalities.
Create code that works across major operating systems (Linux Ubuntu 22.04 used at the SAT at Metalab, macOS, Windows).


Timeline
Week 1-2 (May 17 - May 30):

Set up the development environment and familiarize yourself with the OSSIA Score architecture and plugin system.
Research existing media library interfaces and tools, and identify suitable technologies and libraries for the project.

Week 3-4 (May 31 - June 13):

Design and implement the core functionalities of the media library plugin, including media indexing, metadata management, and search capabilities.
Develop a simple GUI for the media library plugin, based on Qt and OpenGL.

Week 5-6 (June 14 - June 27):

Implement media preview and playback functionalities, using suitable libraries and frameworks.
Integrate local file browsing and management capabilities into the media library plugin.

Week 7-8 (June 28 - July 11):

Implement media repository integration, using suitable APIs and protocols such as the Freesound API and the Wikimedia Commons API.
Test and debug the media library plugin and GUI, and address any issues or bugs.

Week 9-10 (July 12 - July 25):

Enhance the GUI design and usability, based on user feedback and testing.
Provide documentation and examples for the media library plugin and GUI, and contribute to the OSSIA Score user manual.

Week 11-12 (July 26 - August 8):

Finalize the project and prepare for the final evaluation, including testing and code clean-up.
Present the project and its results to the OSSIA Score community and the SAT organization.

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The proposed project aims to review existing software platforms such as Audio Stellar, FluCoMa, and Media Cycle, and develop similar functionalities in C++ & Qt within ossia score. The project will involve studying the code of these platforms and implementing communication mechanisms between them and ossia score. The project will primarily use C++ and Qt for development, with a focus on GUI and UI/UX tools. Additionally, the project will incorporate external APIs such as the Freesound API and the Wikimedia Commons API. By leveraging these technologies, the project seeks to provide a comprehensive audio signal processing and analysis solution within ossia score.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am writing to express my interest in the project at your organization. As an undergraduate Computer Science student with a strong interest in learning and acquiring knowledge, I am confident that my skills and experiences align with the requirements for the project.
Currently, I am pursuing a B. Tech in Computer Science from Manipal University, Jaipur, where I have been developing my skills in web development, software development, and data analysis. I have worked on several projects, including a Management System created using C++, a Covid Screening System, and a Movie Recommendation System Using Machine Learning. Additionally, I have also created a Web Music Player, a Chat App, and a Spotify Clone using React JS and Firebase.
In terms of skills, I possess strong problem-solving skills and attention to detail, knowledge of databases and data modelling, verbal and written communication skills, and familiarity with version control systems such as Git. I also have a good understanding of data structures and algorithms, and I can write clean and efficient code. I have experience with several programming languages, including C/C++, Java, Python, SQL, HTML/CSS, and JavaScript.
I am confident that my skills and experiences make me an ideal contributor for this project. I am a quick learner and can adapt to new technologies and tools in order to respond to the industry's ongoing evolution. I can manage, prioritize, and complete tasks in a timely manner, and I am committed to producing high-quality work.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* required: C++
* preferred: experience with Qt, AR/MR/VR/XR toolkits and potentially AI and (unsupervised) machine learning skills
C++ : I possess a remarkable level of proficiency in the C++ programming language, having acquired a comprehensive understanding of its intricacies during my academic years and honing my expertise through continued self-study and practical application, particularly in the realm of data structures. Moreover, I have put my knowledge into practice by developing a sophisticated project utilizing Data File Handling techniques in C++, wherein I crafted a purchase and billing system for an Online Optical Shop, demonstrating my aptitude for implementing practical solutions in real-world scenarios.

Potentially machine learning skills : Having recently availed myself of free online resources on Machine Learning, I have acquired several valuable competencies in this burgeoning field. Drawing on this knowledge, I have already constructed an impressive ML model that facilitates movie recommendations. Yet, mindful of the need for continued growth and development, I am currently engaged in refining my expertise in this domain, undertaking a project entitled "Enhancement in Market Basket Analysis". I hope to further expand my knowledge and proficiency in the field of Machine Learning, while simultaneously crafting a practical solution that promises to benefit businesses seeking to optimize their sales and revenue generation strategies.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

Medium

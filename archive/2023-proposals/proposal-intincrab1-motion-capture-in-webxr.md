---
title: Motion Capture in WebXR
slug: proposal-intincrab1-motion-capture-in-webxr
author: Ajay Chauhan
date: 2023-03-24 11:24:11 UTC-05:00
tags: proposals, medium, 350 hours
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Ajay Chauhan
* gitlab username: [intincrab1](https://gitlab.com/intincrab1)
* timezone: UTC +5:30

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Represent Pose detection using Mediapipe in a WebXR environment.

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The proposed project aims to build a prototype platform that leverages the power of [MediaPipe Pose](https://google.github.io/mediapipe/solutions/pose.html), a free and open-source software for pose detection in live streaming contexts, to enable Extended Reality (XR) users in telepresence settings to relive live performances through motion capture data replay. The project seeks to create an XR session for presenting real-time performances and gaining experience with technology integration. The platform will enable users to experience the excitement and energy of live performances from the comfort of their own homes, thereby enhancing accessibility and inclusivity in the music industry.

The project will be developed using [WebXR](https://immersiveweb.dev/), a JavaScript API for creating immersive and interactive experiences that run in web browsers. The Mediapipe Pose model will be integrated into the WebXR application to detect the poses of performers and capture their movements in real time. The captured motion data will be stored in a database and replayed in a synchronized manner to enable XR users to experience the performance as if they were there in person.

A similar GSoC contribution was achieved last year with the SAT, [Face tracking for improving accessibility in hybrid telepresence interactions](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-ideas/idea-face-tracking-livepose-satellite/). Based on Matt Wiese's [efforts](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-contributions/matthewwiese-satellite-face-tracking/work-product-matthewwiese/), the project seeks to decouple motion detection from the XR user, to simulate a live performance context. 


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The benefits of this project to the community are multifold. It can significantly enhance the experience of XR users who may not have the opportunity to attend live performances due to geographical or accessibility reasons. By providing a platform for telepresence users to relive live performances through motion capture data replay, this project can increase access to cultural events and performances, and promote inclusivity and diversity in the arts. By creating a prototype platform for reliving live performances, we are addressing a [growing need in the entertainment industry](https://www.transparencymarketresearch.com/extended-reality-xr-market.html) for innovative ways to engage audiences in virtual and augmented reality settings.

Google and the SAT would be proud to sponsor this project as it aligns with their shared goals of promoting innovation and advancing the field of XR technology. 
The open-source nature of this project means that it will be available to the wider community, enabling others to build on our work and potentially create new applications for motion capture data replay, using open-source software and sharing our code and documentation, we can contribute to the development of a more accessible and diverse XR ecosystem. It would also help establish open-source tools like Google MediaPipe Pose as valuable tools for developers working in the XR space, ultimately advancing the field as a whole.

The project has the potential to create a significant impact in the XR and entertainment industries, promote inclusivity and cultural exchange, and inspire innovation in the open-source community.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

The proposed project aims to develop a WebXR platform prototype that enables XR users to interact with motion capture data obtained from live performances. The project will leverage the capabilities of Google's Mediapipe Pose, an open-source software for pose detection in live streaming contexts, to enable real-time pose detection and tracking. The project will be implemented in WebXR and will simulate motion detection and map pose data onto XR users. The deliverables for this project will include:

 1. Investigation and planning
 2. Integration of Mediapipe Pose with WebXR
 3. Implementation of the XR platform prototype
 4. Testing and optimization
 5. Documentation

### **Investigation and Planning (Weeks 1-3)**
*Community Bonding Period ( May 4, 2023 - May 28, 2023 )*

* Get better acquainted with the mentors and other Contributors.
* Investigate and research the Mediapipe Pose API and WebXR technology to gain a thorough understanding of their capabilities and limitations.
* Planning of the project, including the development of a project plan, a technical design document, and a user interface design document

* **Deliverables:**
    * Project plan outlining the design of the XR session and the integration of Mediapipe Pose into the WebXR environment
    * Technical design document
    * User interface design document

###  **Integration of Mediapipe Pose with WebXR (Weeks 4-6) June 6 - June 26** 

  * Set up of development environment for the project, including all necessary tools and libraries
  * Integration of the Mediapipe Pose API with the WebXR platform
  * Development of a real-time pose detection and tracking system for XR user 
  
* **Deliverables:**
    * Mediapipe Pose API integrated with WebXR
    * Real-time pose detection and tracking system for XR user

### **Implementation of the XR Platform Prototype (Weeks 7-11) June 27 - July 24**
   * Midterm evaluation (July 10 - July 14)
   * Implementation of the XR platform prototype
   * Integration of the real-time pose detection and tracking system with the XR platform
   * Development of a user interface for the XR platform and ensuring that the WebXR environment is functional and can run on common devices and browsers

* **Deliverables:**
    * XR platform prototype
    * Real-time pose detection and motion capture integrated with the XR platform
    * User interface for the XR platform

###  **Testing and Optimization (Weeks 11-12) July 25 - August 14**

   * Testing of the XR platform prototype to ensure that the motion capture is accurate and reliable
   * Optimization of the XR platform performance

* **Deliverables:**
    * Optimized XR platform prototype
    * Refined user interface for the XR platform

### **Documentation (Weeks 13-14) August 15 - August 28**

   * Development of documentation for the project, including a technical report and user manual
   * Include information on how to use the XR session, how to interact with the interface, and any technical details that may be relevant
   * Submission of final project report

* **Deliverables:**
    * Technical documentation that outlines the technical details of the project, including the integration of Mediapipe Pose into the WebXR environment, the development of the XR session
    * User manual
    * GitLab repository with all code and documentation

### **Optional Deliverables**
   * Implement a user interface for controlling the replay of the motion capture data. For example, allowing users to pause, rewind, or fast-forward the performance or switch between different camera angles
   * Integration of network functionality that allows multiple users to participate in the WebXR experience simultaneously to enable group interactions


At the end of the project, the following deliverables will be provided:

  * A working XR platform prototype that enables XR users to interact with live performance data.
  * Documentation, including a technical report, user manual, and GitLab repository with all code and documentation.
  * Optional deliverables (if applicable).

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->
The goal of this project is to develop a platform prototype where XR users in telepresence settings can relive a live performance through motion capture data replay.
The [Mediapipe Pose](https://google.github.io/mediapipe/solutions/pose.html) solution developed by Google is a popular open-source project that uses computer vision to detect human body poses in real-time. It has been widely used in various applications, including fitness and dance instruction, sports training, and gaming.

There are a few existing projects that are similar to the proposed WebXR prototype for the replay of motion capture from live performances. These projects include virtual reality concert experiences, such as the [VR experience for a Coldplay concert](https://venturebeat.com/mobile/coldplay-and-nextvr-release-virtual-reality-concert-for-samsung-gear-vr/) created by the company NextVR, and motion capture-based VR music videos, such as "Chorus" by the artist Justice.

The proposed project differs from these existing projects in several ways. First, the proposed project focuses specifically on motion capture data from live performances, rather than creating a fully virtual concert experience. This allows users to relive actual performances, rather than experiencing a simulated concert. Using motion capture data from live performances also adds a layer of realism to the experience, as it captures the movements and interactions of the performers in real-time. This creates a unique and engaging experience for users, as they can relive the performance from multiple perspectives and see the nuances of the performers' movements up close.

There also have been some related works in the domain of XR and music performances, such as [MixCast VR](https://mixcast.me/) and [WaveXR](https://wavexr.com/). However, these projects are focused on providing immersive experiences for music performances, rather than developing a platform prototype for XR users to interact with motion capture data obtained from a live performance.

The proposed project will be developed using WebXR technology, which allows users to access the experience from any web browser without requiring the installation of additional software or hardware. This makes the experience more accessible and eliminates barriers to entry for users who may not have access to VR headsets or other specialized equipment.
In summary, while some existing projects share similarities with the proposed WebXR prototype, the project's focus is on motion capture data from live performances. Additionally, being open source, enable others to build on our work and potentially create new applications for motion capture data replay.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

My name is Ajay Chauhan, and I'm a second-year undergraduate student of Computer Science and Engineering. 
I'm a person of colour in STEM with a fascination towards arts and music. All of this made me gravitate toward the SAT as my organisation of choice for GSoC, and I'm determined to use my experiences to make a positive impact.

I have always believed that "coders are the sorcerers of the 21st century". It conveys that they have the power to create tools for the upliftment of mankind, and what better way to do so than through open-source contributions. My experience with open source is relatively small, but it has been so exciting that I've been contributing to multiple projects once I got acquainted with the concept. Open-source contributions have made me realize the importance of well-documented and easy-to-understand code. I also have experience with competitive programming.

I am particularly interested in several areas of programming, including Algorithms, Operating Systems, and Web Development. Recently, I have also been exploring the fields of machine learning and Android development, broadening my knowledge and skills.
I started my programming journey with Web Development, which I studied in the [FullstackOpen](https://fullstackopen.com/en/) course offered by the University of Helsinki. Programming languages that I am well-versed in include C, C++, Javascript and Python, which are all part of my university curriculum. I have been actively exploring the Rust programming language for the past few months. ([source code](https://github.com/intincrab/rust-lang-book))  
Making music is one of my hobbies due to my fascination with art.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* **required**: some experience with coding
    * I have been using C/C++ and Python for various university projects for the past 1.5 year
    * I have an adequate understanding of various development concepts including Git, Github, MongoDB, React, NextJS, Express, Node and Javascript
    * I have completed a course on Git and Github ([credentials](https://www.udemy.com/certificate/UC-31e987ac-8064-4701-83d8-2c6423736da0/))
* **preferred**: some experience with Free and Open Source Software (FOSS)
    * I've been helping people transition to the Linux ecosystem by providing support and resolving queries about GNU/Linux on various forums and Discord servers, and encouraging the adoption of open-source culture
    * I started my open-source contribution in Hacktoberfest of 2022, where I contributed to many small projects. ([credentials](https://www.holopin.io/userbadge/cl8jtt8gq192509l90w7th1ug))
* **preferred**: some experience with Python
    * I've been using Python since high school for creating Discord bots ([source code](https://github.com/intincrab/banana-bot)), and I also took a course on machine learning with Python at my university, furthering my understanding of the language. ([coursework](https://github.com/intincrab/ml-coursework))
* **preferred**: some interest in computer graphics


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

medium

---
title: Blender add-on to create maps between UI signals and external controllers with libmapper
slug: proposal-yijielu-blender-addon-libmapper
author: Yijie Lu
date: 2023-04-03 10:21:00 UTC+08:00
tags: proposals, hard, 350 hours
type: rejected
---


## Name and Contact Information

* name: Yijie Lu
* website: https://github.com/111huan
* gitlab username: TH1901
* timezone: GMT+8


## Title

Blender add-on to create maps between UI signals and external controllers with libmapper


## Synopsis

This project aims to develop a Blender add-on for mapping UI signals to external controllers using the libmapper inter-operability protocol. The add-on will enable Blender users to create custom mappings between the Blender UI and various external controllers that support OSC control. Additionally, the add-on will explore the use of AI composer tools like MuseNet to find the magic relationship between sound and 3D visuals. The project will be developed over a period of 24 weeks, divided into different stages for the implementation and testing of various features.


## Benefits to Community

The Blender add-on developed through this project would provide a powerful tool for artists, designers, and developers who work with Blender to create immersive experiences, projection mapping, and digital musical instruments. By supporting the integration of external sensors and controllers, the add-on would make it easier for users to control Blender and create complex mappings between 3D visuals and sound.

The integration of AI composers like MuseNet would also provide users with a new level of creative control and the ability to explore new relationships between sound and 3D visuals.


## Deliverables

### Week 1-2:

* Set up project environment
* Research and select the appropriate version of libmapper python bindings
* Develop a plan for the Blender add-on architecture
* Start coding the basic structure of the blender add-on

### Week 3-4:

* Create a user interface for the Blender add-on
* Implement basic mapping functions between Blender UI signals and libmapper

### Week 5-8:

* Implement external controllers with inter-operability protocols such as MIDI or OSC
* Test and debug mapping functionality

### Week 9-10:

* (Optional) Implement additional physical sensors
* (Optional) Integrate support for Arduino and Raspberry Pi boards as external controllers

### Week 11-12:

* Test and debug support for external controllers
* (Optional) Implement support for mapping between features extracted from audio and video content and haptic feedback
* (Optional) Begin development of AI composer integration using tools such as Musenet

### Week 13-16:

* (Optional) Complete development of AI composer integration
* (Optional) Implement additional mapping features for AI-generated sound

### Week 17-20:

* Test and debug additional mapping features for AI-generated sound
* Implement support for 3D mapping, allowing external controllers to manipulate 3D models within Blender

### Week 21-24:

* Test and debug 3D mapping functionality
* Develop documentation and tutorials for the Blender add-on
* Final testing and debugging
* Finalize documentation and tutorials
* Release the Blender add-on as open source software


## Related Work

Blender.NodeOSC: A Blender addon that allows OSC communication with external devices. This addon is similar to the proposed work as it also enables the control of Blender with external devices through OSC communication. It has limitations to OSC communication only and does not provide support for other protocols like MIDI or libmapper.

Blender-MIDI: Another Blender addon that provides MIDI support for controlling Blender. This addon is similar to the proposed work in terms of supporting MIDI as an external control protocol, but it lacks support for other protocols like OSC or libmapper.

libmapper: An open-source library for creating mappings between devices and applications. This library is similar to the proposed work as it also provides support for creating mappings between devices and applications.

[Splash](https://gitlab.com/sat-mtl/tools/splash/splash): A projection mapping software that allows for the mapping of video content onto physical surfaces. While not directly related to the proposed work, the Blender addon can create a draft scene (textured objects + cameras) and export them as a Splash configuration file, and then send in real-time, through shmdata, meshes and textures from Blender to Splash. This could give this project some inspirations on how to build the bridge between blender and external control.

OSCulator: OSCulator is a software that translates input from various external controllers (such as MIDI, OSC, Wiimote, etc.) to output OSC messages that can be used to control other software. OSCulator does not integrate with Blender directly, so it requires an additional bridge software to communicate with Blender.


## Biographical Information

My name is Yijie Lu, and I am a highly motivated software engineering student at Beijing University of Technology. I have a passion for developing applications in Python, and have acquired advanced proficiency in the language through various personal and academic projects. I have also honed my skills in 3D game development, having created games using Blender, Maya, and Unity Engine. Additionally, I have experience in using machine learning tools, single-board computers and sensors. I am excited to bring my technical skills and passion for open source development to this project, and I am confident that I can contribute significantly to its success.. 


## Skills


* required: Python
1. [Poisson-Distribution](https://github.com/111huan/Poisson-Distribution.git): Given the input current and circuit resistance of a large-scale integrated circuit on a chip, this project uses the Poisson equation to quickly calculate the output current of the circuit's lower level.
2. [Facial-Recognition](https://github.com/111huan/Facial-Recognition.git): This project is based on convolutional neural networks for facial contour detection, feature extraction, and recognition. It includes image acquisition, image preprocessing, face detection, training data, facial recognition function, and the training set is sourced from LFW2013. The key technologies involved include digital image processing, neural networks, and Python GUI programming. The performance of four models, CNN, ResNet, Vgg, and Cnn, is compared.
3. [Web-Spider-of-Douban-Film](https://github.com/111huan/Web-Spider-of-Douban-Film.git): This project uses Python spider technology to crawl the top 250 movies on the movie ranking list of Douban (a Chinese recommendation website for movies, books, games, etc.) and uses Python data analysis to obtain information such as the most popular directors, actors, and movie genres.
4. [Repurchase-Prediction](https://github.com/111huan/Repurchase-Predicton.git): This project uses commonly used Python libraries such as sklearn, numpy, and pandas, as well as machine learning techniques, to analyze the personal and shopping information of users on Tmall (a Chinese shopping website), and thus predict the likelihood of a user buying a certain product. The data set is sourced from the public competition data set provided by the website company.

* preferred: Blender
[3D Visual](https://github.com/111huan/Blender-Project): I used Blender to create some models for my game, rendered them, and set up lighting to achieve a visually appealing result.

* Noteworthy:
1. [Unity 2D Game](https://github.com/111huan/dans1.git): This is a 2D action game for Windows that I developed using Unity Engine. 
2. [GGJ2023](https://github.com/111huan/GGJ2023.git): This is a game that I participated in creating during the Beijing offline Global Game Jam in February 2023. I contributed to both the art and programming aspects of the game in my team. 
    

## Expected size of project 

350 hours

## Rating of difficulty 

Hard

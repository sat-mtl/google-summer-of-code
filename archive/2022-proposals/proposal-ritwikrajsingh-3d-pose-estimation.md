---
title: 3D pose estimation from multiple uncalibrated cameras
slug: proposal-ritwikrajsingh-3d-pose-estimation
author: Ritwik Raj Singh
date: 2022-04-16 12:06:11 UTC+05:30
tags: proposals, hard, 350 hours, LivePose, metaverse
type: rejected
---

## Name and Contact Information

* name: Ritwik Raj Singh
* website: [COdeDEmon](https://ritwikrajsingh.github.io)
* gitlab username: @Ritwikrajsingh
* timezone: UTC+5:30

## Title

3D pose estimation from multiple uncalibrated cameras

## Synopsis

Measuring 3D human pose is important for analyzing the mechanics of the human body in various research fields such as biomechanics, sports science and so on. In general, some additional devices like, optical markers and inertial sensors are introduced for measuring 3D human pose.
<p>
<img src="https://raw.githubusercontent.com/Ritwikrajsingh/ritwikrajsingh.github.io/master/assets/images/3d_pose_fig1.png"> 
</p>
Despite the various advantages of this approach in terms of high estimation quality, i.e. precision and robustness, it is sometimes difficult to utilize them in some practical scenarios, such as monitoring people in daily life or evaluating the performance of each player in a sports game, due to inconveniences of installing the devices. 
To estimate 3D human pose in such cases, vision-based motion capture techniques have been studied in the field of computer vision. Basically, they utilize multi-view cameras or depth sensors and assume that they are synchronized and calibrated beforehand. Such synchronization and calibration are troublesome to establish and maintain; typically, the cameras are connected by wires and capture the same reference object. 
<p>
<img src="https://raw.githubusercontent.com/Ritwikrajsingh/ritwikrajsingh.github.io/master/assets/images/3d_pose_fig2.png"> 
</p>

Some 2D local features based synchronization and calibration methods have also been proposed for easy-to-use multi-view imaging systems in cases where the preparations cannot be done. However, they sometimes suffer from difficulties of feature correspondences in case for which the multiple cameras are scattered with wide baselines, which the erroneous correspondences affect the stability and precision of estimation severely.

<p>
<img src="https://raw.githubusercontent.com/Ritwikrajsingh/ritwikrajsingh.github.io/master/assets/images/3d_pose_fig0.png">
</p>
This project aims at facilitating the calibration process by using the human body as a calibration tool, hence removing the need to manually place calibration points or any such cumbersome method by using multiple cameras and calibrating the outputs together to extract 3D coordinates from the 2D pose estimations associated with each camera. 

## Benefits to Community

[**Society for Arts and Technology (SAT)**](https://sat.qc.ca/en) offers an educational model at the crossroads between science, technology, society, and art and allows the transfer of knowledge coming out of the SAT's artistic activities and lines of research. By offering night classes and educational activities for adults, as well as a pedagogical program adapted for youths ages 9 to 17 in its spaces and in the school, municipal, community, and cultural networks, Campus SAT reaches over 8000 participants every year and provides a series of activities on sound composition, 2D animation, video creation, Vjing, programming and 3D modeling. In addition to introducing participants to various digital tools, SAT's facilitators approach creation in a free and multidisciplinary way that allow the use of several tools and software in a creative, abstract, non-narrative and experimental approach and it the developement laboratory of SAT is know as [**Metalab**](https://sat-metalab.gitlab.io/en/). 

The research fields of Metalab that is useful in social technologies are; *telepresence* ([**Scenic**](https://gitlab.com/sat-mtl/telepresence/scenic/), [**Switcher**](https://gitlab.com/sat-metalab/switcher/)), *interaction* ([**LivePose**](https://sat-metalab.gitlab.io/documentations/livepose/)), *immersion* ([**EditionInSitu**](https://gitlab.com/sat-metalab/EditionInSitu/), [**Mirador**](https://gitlab.com/sat-metalab/hardware/mirador/)), *video mapping* ([**Splash**](https://sat-metalab.gitlab.io/splash/), [**Calimiro**](https://gitlab.com/sat-metalab/calimiro/)) and *spatialized sound* ([**SATIE**](https://sat-metalab.gitlab.io/documentations/satie/), [**Audiodice**](https://gitlab.com/sat-metalab/hardware/audiodice), [**vaRays**](https://gitlab.com/sat-metalab/vaRays/)), etc.. Metalab's mission is twofold: to stimulate the emergence of innovative immersive experiences and to make their design accessible to artists and creators of immersion through an ecosystem of free software.

## Deliverables

### Timeline

#### Before June 13
* Community bonding period
  * Contact with mentors and know more about the organization and project
  * Decide schedule and mode of communication and emergency meetings
  * Understand codebase of the SAT
  * Set-Up a blog for GSoC

#### June 13 - June 28
* Research resources over the internet
* Discover resources(papers, repos etc) relevent to the project
* Make a list of all the relevant resources

#### July 7 - July 24
* Prototyping
* Try to implement various implementation approaches
* Evluate results
* Decide a final relevent approach

#### July 25 - July 29
* Official GSoC Evaluation Phase 1

#### July 30 - August 16
* Understand LivePose
* Start working with LivePose
* Try to implement selected models using LivePose
* Create the relevent pipline from selected models
* Document everything

#### August 17 -  September 4
* Formulate the integration of selected computer vision pipline to LivePose
* Start working on the LivePose and computer vision pipline integration
* Unit and integration testing

#### September 5 - September 19
* Start Writing documentation
* Unit and integration testing
* Work on improving the codebase

#### September 20 - October 19
* Debugging
* Furter improvements
* Continue writing documentation

#### October 20 - November 20
* Refactoring code
* Complete documentation
* Complete project report
* Squash bugs
* Finalize the code

#### November 21
* Final date for all contributors to submit their work product

#### November 28
* Final date for mentors to submit evaluations for GSoC contributor projects with extended deadlines


### Milestones
* Talk with LivePose team to get precisions regarding the uses and needs for 3D pose estimation
* Research various resources and state of the art papers and codebases relevant to the project
* Choose a method which fits the needs and is doable in GSoC timeframe
* Implement a working prototype inside LivePose (implementing models based on previous findings)
* Improve codebase and integration with the rest of LivePose
* Partial representation of results by training/testing on a subset of a publicly available dataset

## Related Work

* [Human Pose as Calibration Pattern; 3D Human Pose Estimation with Multiple Unsynchronized and Uncalibrated Cameras Kosuke Takahashi Dan Mikami Mariko Isogawa Hideaki Kimata NTT Media Intelligence Laboratories NIPPON TELEGRAPH AND TELEPHONE COORPORATION, Japan](https://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w34/Takahashi_Human_Pose_As_CVPR_2018_paper.pdf)
  * This paper proposes a novel 3D human joint position estimation algorithm for unsynchronized and uncalibrated cameras with wide baselines.
  * This method focuses on the major skeleton joints and the constancy of joint separation. The 2D human pose is detected from the multi-view images
and joint position estimates are used in a structure-from-motion manner. 
  * The proposed method provides an objective function consisting of a relaxed reprojection error term and human joint error term in order to achieve robust estimation even if the input data is noisy; the objective term is optimized. Evaluations using synthesized data and real data shows that the proposed method works robustly with noise-corrupted data.  
* [MetaPose: Fast 3D Pose from Multiple Views without 3D Supervision Ben Usman, Andrea Tagliasacchi, Kate Saenko, Avneesh Sud](https://arxiv.org/pdf/2108.04869)
  * This paper proposes a new modular approach to 3D pose estimation that requires only 2D supervision for training and significantly improves upon the state-of-the-art
by fusing per-view outputs of singe-view modules with a simple view-equivariant neural network. 
  * The modular approach not only enables practitioners to analyze and improve the performance of each component in isolation, and channel future improvements in respective sub-tasks into improved 3D pose estimation “for free”, but also provides a common “bridge” that enables easy inter-operation of different schools of thought in 3D pose estimation – enriching both the “end-to-end neural world” with better model-based priors and improved interpretability, and the “iterative refinement world” with better-conditioned optimization problems, transfer-learning, and faster inference times. 
  * This paper provide a detailed ablation study dissecting different sources of the remaining error, suggesting that future progress in this task might come from the adoption of a full camera model, further improvements in 2D pose localization, better pose priors, and incorporating temporal signals from video data.
* [Estimating 3D Body Pose using Uncalibrated Cameras R ́omer Rosales, Matheen Siddiqui, Jonathan Alon, and Stan Sclaroff Computer Science Department, Boston University Boston, MA 02215](https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.969.8884&rep=rep1&type=pdf)
This paper has presented a novel approach for estimating 3D body pose from image sequences. It introduces the formulation of a probabilistic, multiple hypothesis Structure from Motion framework along with an approximate, algorithm for solving it and there are several advantages to the proposed approach:
  * It does not require any camera calibration nor manual initialization as required by previous approaches that use multiple-camera setups.
  * This approach exploits the invariances of Hu moments and the concept of “virtual cameras” to obtain good 3D structure estimates and a more tractable algorithm for solving this problem. 
  * The approach also allows use of large baselines for better 3D reconstruction, without the nettlesome issue of feature correspondence. 
  * Finally, the method runs at around 2 frames/sec. when computing with one frame at a time.
* [Deductive Learning for Weakly-Supervised 3D Human Pose Estimation via Uncalibrated Cameras Xipeng Chen Pengxu Wei Liang Lin](https://ojs.aaai.org/index.php/AAAI/article/view/16194/16001)
  * This paper proposes a deductive weakly-supervised learning method for 3D human pose machines with multi-view images and only 2D pose annotations. Instead of view synthesis which involves given camera parameters and complex view alignment, it employs deductive reasoning for human pose inference and develops a mechanism of self-demonstration to guide the model learning. 
  * It also proposes learning by deduction with view-transform demonstration and structural rules, aiming to make an inference reasonably for the human pose from one view to another and improve the reliability of the model training with weak supervision.
  * Quantitative and qualitative experimental results on challenging 3D human pose datasets show a superior performance of our proposed method, demonstrating the effectiveness of our learning by a deduction for weakly-supervised 3D human pose estimation, even in unconstrained scenes.

## Biographical Information

I am an undergraduate, pursuing a degree in Computer Science and Engineering from [University Institute of Technology](https://hpuniv.ac.in/university-detail/home.php?uiit), Himachal Pradesh, India.

I am skillful at Python Programming and have experience with many python frameworks like; PyQt, tkinter, Kivy, Pygame. I have done few projects on ML and have experience with tensorflow. All of my projects can be found [here](https://github.com/Ritwikrajsingh).

## Skills

* **required: experience with Python**
  * I am skillful in Python and completed various projects like this one [here](https://github.com/Ritwikrajsingh/Slider-based-Control-GAN.git) 
* **required: experience with computer vision**
  * I have previously worked on quiet a numebr of computer vision applications, one of whih is a drone landing marker tracking application that can be found [here](https://github.com/Ritwikrajsingh/vitarana_drone.git)
* **required: strong foundation in math**
  * I am a final year engineering student, I am good with maths and it is my favourite. This project [Slider-based-Control-GAN](https://github.com/Ritwikrajsingh/Slider-based-Control-GAN.git) I did, required me to do complex mathematics like; PCA, Reverse PCA, Calculus, Matrix Algebra, etc.. 
* **if going the deep learning road: experience with machine learning frameworks**
  * I have also done a great deal of ML projects and have experienced tensorflow, one of them are [Slider-based-Control-GAN](https://github.com/Ritwikrajsingh/Slider-based-Control-GAN.git)
## Expected size of project 

350 hours

## Rating of difficulty 

hard

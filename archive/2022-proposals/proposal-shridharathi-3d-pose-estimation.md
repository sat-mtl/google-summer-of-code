---
title: Estimation of 3D Pose from Multiple Uncalibrated Camera Images
slug: proposal-shridharathi-3d-pose-estimation
author: Shridhar Athinarayanan
date: 2022-04-09 15:10:42 PST
tags: proposals, hard, 350 hours, LivePose
type: rejected
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, identical as filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Shridhar Athinarayanan
* website: https://github.com/shridharathi
* gitlab username: shridharathi
* timezone: PST

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Estimation of 3D Pose from Multiple Uncalibrated Camera Images

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

3D pose estimation has become a relevant topic of discussion with various applications in robotics, sports analysis, and human
immersive experiences. Typically, images from multiple calibrated cameras can easily extract a 3D pose, but the process becomes
more challenging when the cameras are uncalibrated. This GSoC project aims to conduct 3D pose estimation from 2D images using the human body as a calibration reference. This project will leverage state-of-the-art deep learning techniques to statistically infer 3D poses, unlike other methods already in use in LivePose which give 2D poses in the camera frame. From Usman et. al., for instance, a stacked hourglass Convolutional Neural Network can be deployed encoding features such as joint positions, bone length, and camera parameters to learn an estimation of 3D positionality (https://arxiv.org/pdf/2108.04869.pdf). This project will be incredibly useful for creating audiovisual teleimmersive artistic experiences with sufficiently precise and intimate human interaction. 

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

For SAT's particular use, this project would be used in teleimmersive experiences to increase the possibilities offered to participants to interact with the virtual content. This would allow for a new generation of art, one in which participants can actively interact with artwork in a more intimate manner. It will also allow for interactive pieces which do not depend on physical controllers for the participants to use and, in particular, not on smartphones. In the end this should make interactive art pieces more accessible and inclusive. From here, this method of pose estimation extends further into multiple use cases. One of the papers from research outlines how images of sports players from variously placed uncalibrated cameras can be used to construct a 3D model of a player to ascertain out-of-bounds errors or analyze a player's movement strategies. I see how this idea could extend to other fields, as well. For instance, precise poses of surgeons could be constructed through images from multiple camera angles to aid in robotic surgery. Additionally, poses of sign language could be documented to train a robotic glove which could help teach deaf children sign language movements or even create live closed captioning for televised ASL events. 


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Milestones

* Investigate and research about 3D Pose Estimation and how neural networks can be used for the task.
* Implement the mathematical initialization estimation portion of Usman et. al.'s paper.
* Implement the refinement CNN portion of Usman et. al.'s paper.
* Integrate pipeline into LivePose and investigate/conduct applications of the newly developed tech.

### Timeline

* Pre GSOC Period 
	* Remain in contact with Christian, Emmanuel, and Fay to ask more questions/get more information about the project.
    * Close read Usman et. al.'s paper and investigate other papers.

* Community Bonding Period
    * Get closer with mentors and other members of SAT.
    * Learn more about SAT's other projects.

* Week 1 - Week 4
    * Conduct thorough literature review/further investigation and fully understand Usman et. al. paper.
    * Read through LivePose codebase to figure out how to integrate 3D Pose pipeline.
    * Implement rigid alignment portion of Usman et. al. for initialization of joint and camera coordinates.
    * Extensively test initialization before moving onto CNN.

* Week 5 - Week 9
    * Implement CNN for refinement of positions
    * Extensively test CNN with validation set to evaluate if refinement is working.

* Week 10
   	* Fully integrate pipeline into LivePose codebase.

* Week 11-12
    * Discuss applications for 3D Pose Estimation Pipeline for teleimmersive experiences.
    * Work closely with the team to determine and conduct an application project which can be used to test the finished implementation.

* Post GSOC Period 
    * Continue making necessary modifications/adding new features to the pipeline.
    * Remain in close contact with the team to contribute to new projects.

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The main paper the implementation will be based on is from Usman et. al. on their method MetaPose for calculating 3D pose from 2D 
images of uncalibrated cameras without 3D supervision (https://arxiv.org/pdf/2108.04869.pdf). The implementation of MetaPose relies
on a 2 step process. The first step is initialization, or establishing a guess for the 3D coordinates of the body's joints and the 
placement of the cameras. Knowing the initial parameters of one camera, the positions and angles of the others can be determined 
through a rigid alignment optimization problem solved using singular value decomposition. From this, the joint positions can be 
obtained using the results from the rigid alignment. From here, a stacked hourglass Convolutional Neural Network with inputs of the
current pose estimate and the new average heatmap likelihood for joint positions refines the initial estimate, all the while 
minimizing the L2 distance between the predicted and ground truth 2D coordinates from each camera frame. This paper's method works 
around occlusions and obscured positions, ultimately outperforming other methods of 3D pose estimatation on the Human3.6M and 
Ski-Pose PTZ datasets.

In contrast to Usman et. al.'s neural method, Takahashi et. al. presents a more purely mathematical method for computing these 3D 
joint positions (https://openaccess.thecvf.com/content_cvpr_2018_workshops/papers/w34/Takahashi_Human_Pose_As_CVPR_2018_paper.pdf).
They formulate the task into a large optimization problem viewing each 2D image as a projection from the original 3D body pose in 
the plane of the camera frame with numerous constraints on joints, bone length, and smooth motion of each joint. This is more 
computationally efficient than the neural method, most likely not requiring any help from external machines. However, this method 
requires entails more algorithmic complexity and most likely yields less accurate results.

Other works discuss different circumstances for the task. For instance, Xie et. al. developed MetaFuse which is another highly 
effective 3D pose detection model which, like Usman et. al.'s work, deals with the issue of occlusion in 2D images (https://openaccess.thecvf.com/content_CVPR_2020/html/Xie_MetaFuse_A_Pre-trained_Fusion_Model_for_Human_Pose_Estimation_CVPR_2020_paper.html). 

However, in this paper, a fully-supervised 3D Pose Estimation method is used which means parameters of the camera are known and are
incorporated in training. Another method was proposed by Bridgeman et al. which uses other people in the field of vision as 
calibration points (https://cvssp.org/projects/4d/multi_person_3d_pose_sports/2019_cvprw_bridgeman.pdf). However, this is specific
to the sports example where there is a team with other people to use as reference points, and we are focused on only one person in 
the space for the camera to examine. Though these methods are different from our original problem, they might prove to be useful in
 informing the implementation of my own method through their various perspectives of the task at hand.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a junior studying Mathematical and Computational Science and Interdisciplinary Art at Stanford. I am also a Coterminal Masters
student in Computer Science on an Artificial Intelligence track. Much of my past experience has been in data science and
engineering. I have worked with the Stanford Food-Water-Energy for Urban Sustainable Environments Team to graphically construct the
water distribution system of Pune, Maharashtra, India from 550+ pipe vector files. The following summer, I interned with NVIDIA's 
RAPIDS Data Engineering team where I revamped parts of their dataframe database and constructed a read_csv pipeline for csv files 
in AWS S3 buckets through integration of Python, Cython, and C++ layers. This year, I declared my Interdisciplinary Arts minor and 
am now becoming more interested in graphics, computer vision, and human-computer immersive experiences. My past artworks have 
focused on integrating STEM in portraiture, and I plan to extend this by diving into computer vision. I believe my skills in 
computer systems, AI, and object oriented programming in languages such as Python, C++, and Java in addition to my extensive arts
and design background serve as strong credentials for the 3D Pose Estimation project with SAT.

More Info on Pune Water Distribution Project: http://earthresearchsymposium.su.domains/freshwater-resources/shridhar-athinarayanan-pinpointing-inequitable-water-distribution-in-pune-india/
Art Portfolio: https://shridharathinaraya.wixsite.com/portfolio
GitHub: https://github.com/shridharathi
LinkedIn: https://www.linkedin.com/in/shridhar-athinarayanan-638493199/

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* required: experience with Python 
Stanford's CS41 (The Python Programming Language), CME 193 (Intro to Scientific Python), CS109 (Probability for Computer Scientists), CS279 (Computational Biology) all taught in Python. 
Protein folder project for CS279 in Python (sole creator): https://github.com/shridharathi/protein_folder
Implemented functions in Python for RAPIDS cudf library for DataFrames on the GPU as a Data Engineering Intern with NVIDIA.

* required: experience with computer vision
Currently enrolled in CS231N (Deep Learning for Computer Vision) - CNNs, RNNs, GANs for Image Classification and Generation using Python and PyTorch
Previously took CS224N (Deep Learning for NLP) and became versed with PyTorch for RNNs and Transformer Models. Final project: http://web.stanford.edu/class/cs224n/reports/custom_116978375.pdf

* required: strong foundation in math
Stanford's Math 50 Series, including Linear Algebra and Multivariable Matrix Calculus, Integration of Several Variables, and Ordinary Differential Equations. Misc Courses include Introduction to Optimization (constraints, SVD, support vector machines, duality, etc. for linear and convex problems), Applied Matrix Theory, Design and Analysis of Algorithms, Stochastic Processes, and Statistical Inference.

* required: if going the deep learning road: experience with machine learning frameworks
Experience with scikit-learn, pandas, and numpy from Introduction to Scientific Python
Machine Learning Frameworks discussed in STATS 202: Data Mining and Analysis (SVM, PCA, Bayes, Regression, Random Forest, etc.), CS224N, CS231N. This quarter in CS231N, I will be doing a sign language pose estimation project which should equip me with the tools to succeed with this project over the summer.
Protein Classifier implemented with scikit-learn Naive Bayes and self-implemented Naive Bayes algorithm: https://github.com/shridharathi/protein_classifier


## Expected size of project 


<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

hard

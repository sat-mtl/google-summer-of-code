# Editing SAT's GSoC website 

(Google Summer of Code at [SAT](https://sat.qc.ca/))

## Installation

To start contributing with GSoC at SAT, please clone this repository locally and follow the instructions below.

### Recommended configuration process (python3 venv)

We recommend using uv for managing Python versions and virtual environment.

Follow the [uv](https://docs.astral.sh/uv/getting-started/installation/) installation guide for your operating system.

Then we can activate the environment and install the dependencies:

```bash
$ uv venv --python 3.8
$ source .venv/bin/activate
$ uv pip install setuptools wheel
$ uv pip install "Nikola[extras]==8.2.4"
```

Done !

## Using venv

Before running the nikola command, activate the python3 venv:

```bash
$ source .venv/bin/activate
```

### Browse the website locally before pushing to GitLab

From the website folder, run `nikola auto --browser` to build and run the server automatically, detect site changes and rebuild, and refresh the browser:
    
```bash
$ cd website
$ nikola auto --browser
```

This should open the website in your default browser. If not, use this URL: `http://127.0.0.1:8000/`

## More information about nikola

[Getting started](https://getnikola.com/getting-started.html) (10 min. read).
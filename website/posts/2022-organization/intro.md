<!--
.. title: More about the SAT
.. slug: intro
.. author: Christian Frisson
.. date: 2022-03-27 15:41:11 UTC-04:00
.. tags: organization
.. type: text
-->

Please consult the following pages to get more details about our application as organization for GSoC 2022:

* [Organization Profile](/posts/2022-organization/organization-profile/)
* [Organization Questionnaire](/posts/2022-organization/organization-questionnaire/)
* [Program Application](/posts/2022-organization/program-application/)

Please visit [sat.qc.ca](https://sat.qc.ca) for more information about the SAT.
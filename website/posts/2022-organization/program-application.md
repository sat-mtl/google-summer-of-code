<!--
.. title: Program Application
.. slug: program-application
.. author: Christian Frisson
.. date: 2022-02-18 15:50:11 UTC-04:00
.. tags: organization
.. type: text
-->

By submitting this form, Society for Arts and Technology (SAT) will be applying to be considered for inclusion in Google Summer of Code 2022. The content here, plus the contents of the [Organization Profile](/posts/2022-organization/organization-profile/) and [Organization Questionnaire](/posts/2022-organization/organization-questionnaire/), will be reviewed by Google Program Administrators.

> 2022

Feb 7, 2022 - Dec 2, 2022

## Ideas List

Provide the URL for your organization's curated Ideas List for 2022. This is very important: Prospective GSoC Contributors will view this link to understand the kinds of projects available in your organization. A clean and simple presentation is best. Avoid links to unfiltered bugtrackers or other specialized tools.

Ideas List URL *

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)

## Mentors

How many Mentors does your Organization have available to participate in this program?

Number of Mentors *

> 6 (application) then 7

Alphabetically:

* [Christian Frisson](https://gitlab.com/ChristianFrisson)
* [Edu Meneses](https://gitlab.com/edumeneses)
* [Emmanuel Durand](https://gitlab.com/paperManu)
* [Farzaneh Askari](https://gitlab.com/faskari)
* [Nicolas Bouillot](https://gitlab.com/nicobou)
* [Thomas Piquet](https://gitlab.com/tpiquet)
* [Valentin Laurent](https://gitlab.com/vlaurent)
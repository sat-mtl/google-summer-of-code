<!--
.. title: Organization Profile
.. slug: organization-profile
.. author: Christian Frisson
.. date: 2022-02-17 15:41:11 UTC-04:00
.. tags: organization
.. type: text
-->

If your organization is accepted into Google Summer of Code all of the Profile information on this page will be publicly visible on the program site.

## Website URL for Organization *
> [https://sat.qc.ca](https://sat.qc.ca)

## Organization Logo *
> ![](https://gitlab.com/sat-mtl/metalab/metalabcomm/-/raw/master/logos/SAT/Logo%20SAT.png?inline=false)

Your organization logo must be a PNG, with a minimum height and width of 256 pixels.

## Tagline *

> Create teleimmersive multisensorial experiences.

Describe the organization's mission. This is often the only thing a participant will read about your org, be sure to make it clear and concise. 50 characters maximum.

## Primary Open Source License *

- [x] GNU General Public License version 3 (GPL-3.0)  

## What year was your project started? *

- [x] 1996

## Link to your source code location *

> [https://gitlab.com/sat-mtl](https://gitlab.com/sat-mtl)

## Organization Categories

Select which categories fit your organization best. You may select up to 2 categories. This helps GSoC contributors filter the large list of organizations by their interests.

Select at least 1:
(warning) Select no more than 2. 

* [x] Data (databases, analytics, visualization, AI/ML, etc)
* [ ] Development tools (version control systems, CICD tools, text editors, issue managers, q/a tools, etc) 
* [/] End user applications
* [/] Infrastructure and cloud (hardware, software defined infrastructure, cloud native tooling, orchestration and automation, etc)
* [x] Media (graphics, video, audio, VR, streaming, gaming, content management, etc)
* [ ] Operating systems
* [ ] Programming languages (libraries, package managers, testing tools, etc)
* [ ] Science and medicine (healthcare, biotech, life sciences, academic research, etc)
* [ ] Security (tools and frameworks)
* [ ] Social and communications (Blog, chat, forums, wikis, etc)
* [/] Web (tools and frameworks)
* [ ] Other

## Ideas List URL

Prospective GSoC Contributors will view this link to understand the kinds of projects available in your organization. A clean and simple presentation is best. Avoid links to unfiltered bugtrackers or other specialized tools. 

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)

## Organization Technologies

Enter up to 5 keywords for the primary specific technologies your organization uses. Examples: Python, Javascript, MySQL, Hadoop, OpenGL, Arduino 

## Organization Technologies *

> Python, Javascript, C++, OpenGL, SuperCollider

Enter at least 2.

## Organization Topics

Enter keywords for general topics that describe your organization. Examples: Robotics, Cloud, Graphics, Web, etc.
Select up to 5. 

Organization Topic *

> Multimedia, Cloud, Graphics, Web, Accessibility

Enter at least 2.

## Organization Description

Describe what it is your organization does. This information will also be included in the archive once the program has ended.

## Description *

> The Society for Arts and Technology [SAT] is a non-profit organization recognized internationally for the development and creative use of immersive technologies, virtual reality and high-speed networks. Founded in 1996, its triple mission as a center for the arts, training and research, is to enable and host teleimmersive multisensorial experiences.

> SAT creates open-source tools for: 
> Immersive rendering (Mirador),
> Interaction (LivePose), 
> Projection mapping (Splash), 
> Telepresence (Satellite hub, Scenic, Switcher), 
> Audio spatialization (Audiodice, SATIE, vaRays), 
> Data sharing (shmdata).

## Contributor Guidance

Provide your potential contributors with a page containing tips on how to write a successful proposal for your organization. Let them know what you want included, how you want it structured, and how to best get in touch. Examples.

Link for contributor guidance (Wiki, blog, et cetera) *

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-proposals/proposal-welcome/)

## Communication Methods

How do you want potential contributors to interact with your organization? Select methods that your community uses daily, as you will receive many inquiries if your org is selected. 

* [x] Chat > URL: [https://matrix.to/#/#sat-gsoc-2022:matrix.org](https://matrix.to/#/#sat-gsoc-2022:matrix.org)
* [ ] Email > URL: 
* [ ] Mailing List / Forum > URL: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues)

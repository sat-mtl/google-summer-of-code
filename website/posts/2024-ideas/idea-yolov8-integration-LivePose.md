<!--
.. title: Integration of a YOLOv8 backend in LivePose
.. slug: idea-yolov8-integration-LivePose
.. author: Manuel Bolduc
.. date: 2024-02-12 10:15:00 UTC+01:00
.. tags: ideas, medium, 350 hours, LivePose
.. type: closed
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Integration of YOLOv8 backend in LivePose

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

[LivePose](https://gitlab.com/sat-mtl/tools/livepose/livepose) is a command line tool which tracks people skeletons from a RGB or grayscale video feed (live or not), applies various filters on them (for detection, selection, improving the data, etc) and sends the results through the network (OSC and Websocket are currently supported). 

Some preliminary work has been done regarding the integration of object detection backends such as [YOLOv8](https://github.com/ultralytics/ultralytics) in LivePose (for instance, see the [proposed YOLOv5 backend integration](https://gitlab.com/sat-mtl/tools/livepose/livepose/-/commit/4fef98469876b54145ef1be1ae5700affb469deb)). 

With this project, we hope to evaluate the latency and the accuracy of multiple object detection in a live video processing framework. 

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- A LivePose fork with the proposed YOLOv8 backend integration
- Some demonstration of the integration of the different YOLOv8 computer vision tasks in LivePose (detect, classify, track, ...)
- Documentation of the integration process + experimentations 

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 
 
- Some familiarity with Python (or a will to learn!)
- Some familiarity with AI frameworks in computer vision (or a will to learn!)
- Documentation skills 

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Manuel Bolduc](https://gitlab.com/BolducManuel)
- [Rochana Fardon](https://gitlab.com/rfardon)
- [Sarah Al Mamoun](https://gitlab.com/salmamoun)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium

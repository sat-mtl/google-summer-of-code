<!--
.. title: Adding custom Satellite features to the hubs blender add-on
.. slug: idea-satellite-blender-add-on
.. author: Rochana 
.. date: 2024-02-20 
.. tags: ideas, medium, 350 hours, Satellite, Blender
.. type: closed
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->
Adding custom Satellite features to the hubs blender add-on

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

SAT's metaverse, also known as Satellite, is a WebXR platform powered by Mozilla Hubs designed for multiple purposes including but not limited to facilitating access to virtual experiences and promoting immersive community interactions.

Satellite operates mostly by [injection](https://gitlab.com/sat-mtl/satellite/serveur-injection/-/tree/documentation?ref_type=heads) to add custom features to the hubs platform. Since its creation in 2020, numerous features have been introduced to enrich the user's experience.
Although some of those features are available on a GUI, they require support from the SAT team to access. 

To aid artists in the creative process, Mozilla Hubs developed a [blender add-on](https://github.com/MozillaReality/hubs-blender-exporter) that allows 3D creators to make use of their features before exporting the models.

The purpose of this project is to add some of Satellite's main custom features to the add-on, more specifically the zone-based features.
The zone-based features rely on a bounding box collision in order to emit a specific event such as a teleportation or a room change.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- A Hubs add-on fork with at least 2 added features.
- A Satellite room using the newly added blender features.
- Documentation of the process and the features

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: 
    * Python
* preferred:
    * Javascript
    * Familiarity with Blender and WebXR platforms

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Rochana Fardon](https://gitlab.com/rfardon)
- [Manuel Bolduc](https://gitlab.com/BolducManuel)
- [Jean-Michaël Celerier](https://gitlab.com/jmcelerier)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium

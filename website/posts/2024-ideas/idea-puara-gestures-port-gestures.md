<!--
.. title: Adding gestures to puara-gestures, a library for creating high-level gestural descriptors for IoT devices and New Interfaces for Musical Expression
.. slug: idea-puara-gestures-port-gestures
.. author: Edu Meneses
.. date: 2024-02-12 10:00:00 UTC-05:00
.. tags: ideas, puara, 350 hours, IoT, easy
.. type: closed
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Adding gestures to puara-gestures, a library for creating high-level gestural descriptors for IoT devices and New Interfaces for Musical Expression.

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

Self-contained and IoT systems oftem use sensors to get real-world data and provide methods for users to control and interact with systems.
This sensor data is often hard to understand and don't properly represent the way we interact with everyday objects, e.g., it is hard to check if a smartphone is being shaked by looking at the plotted [IMU](https://en.wikipedia.org/wiki/Inertial_measurement_unit) data. In this example, it is be easier when the user or IoT designer gets access to a pre-processed a "shake" parameter. 

[Puara-gestures](https://github.com/Puara/puara-gestures) is currently a small library within the [Puara Framework](https://github.com/Puara) for creating these kind of high-level descripotrs for sensor data. 
The library currently has only a handful of gestures, but there are other initiatives that created descriptor algorithms used in academic research to create digital musical instruments in the last 15 years.
More specifically, we have access to implementations for instruments developed in the context of [The McGill Digital Orchestra](https://www-archive.idmil.org/projects/collaborations/mcgill_digital_orchestra) and [the T-Stick](https://www.idmil.org/project/the-t-stick/).

This project is about porting sensor fusion and gestural descriptors algorithms from [Max](https://cycling74.com/products/max) patches and plugins, including the [Digital Orchestra Toolbox (DOT)](https://github.com/malloch/digital-orchestra-toolbox) into the puara-gestures, ensuring they are compatible with [SoCs](https://en.wikipedia.org/wiki/System_on_a_chip) commonly used in IoT such as the [ESP32](https://www.espressif.com/en/products/socs).

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Multiple C/C++ functions (or classes), compatible with commonly used IoT SoCs, that expect specific sensor data and output gestural descriptors.
* Updating the standalone version of the library to include the created functions/classes.
* (BONUS) Refactor current functions into classes. The library currently uses an old Arduino-like organization where a single class makes all functions available while sensor data is updated via call in a loop. The ideal refactoring include create classes for each gesture and ensure both sensor imput and gestural output are thead safe.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: 
    * C/C++
    * Max
* preferred:
    * PureData
    * PlatformIO

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Edu Meneses](https://gitlab.com/EduMeneses)
- [Jean-Michaël Celerier](https://gitlab.com/jmcelerier)
- [Joseph Malloch](https://gitlab.com/malloch)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

easy

<!--
.. title: Towards a full-featured embedded media sequencer
.. slug: idea-avnedish-nodes-porting
.. author: Jean-Michaël Celerier
.. date: 2024-02-09 18:15:00 UTC+01:00
.. tags: ideas, medium, 350 hours, ossia, puara, avendish
.. type: closed
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Towards a full-featured embedded media sequencer

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

Embedded microcontrollers can be extremely powerful tools in the right hands, but usually require
advanced programming capabilities. 
The [ossia](https://ossia.io) and Puara [Puara/MPU](https://github.com/Puara/MPU) projects allow media artists to create installations, performances, devices based on embedded systems, ranging from Linux-compatible such as Raspberry Pi, to OS-less microcontrollers such as ESP32 or Raspberry Pi Pico.
In particular, _ossia score_, a media sequencer, allows exporting scores made in it into C++ programs which will run directly on embedded microcontrollers. These scores are composed of processing nodes scheduled in time: automations, sound file players, mappings, audio effects, small scripts, etc. 
This project is about porting some of these nodes from ossia so that they can run directly on microcontrollers: for instance, to enable playing soundfiles from an SD Card in an ESP32 and not be limited to basic GPIO & LED control or network messages.
In practice, this involves rewriting the dataflow nodes implementation to remove dependencies to the existing graph system, by porting them to [Avendish](https://github.com/celtera/avendish) as this will make them automatically useable in an embedded context.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- Multiple objects of ossia ported so that they can run on OS-less systems, targetting for instance the Arduino platform.
- ossia should be able to export code for these objects through the pre-existing infrastructure.
- In the end, this would allow to write a score that for instance plays a sound file when a GPIO changes state on a desktop computer, and then compile that score to ESP32 without the artist having to write a single line of code.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 
 
- C++20
- Qt
- Embedded experience: Arduino / ESP-IDF
- (Bonus) Knowledge of a media art tool: ossia, Max/MSP, PureData, TouchDesigner, ...

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Jean-Michaël Celerier](https://gitlab.com/jmcelerier)
- [Edu Meneses](https://gitlab.com/EduMeneses)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium

<!--
.. title: More about the SAT
.. slug: intro
.. author: Edu Meneses
.. date: 2024-01-29 12:59:25 UTC-05:00
.. tags: organization
.. type: text
-->

Please consult the following pages to get more details about our application as an organization for GSoC:

* [Organization Profile](/posts/2024-organization/organization-profile/)
* [Organization Questionnaire](/posts/2024-organization/organization-questionnaire/)
* [Program Application](/posts/2024-organization/program-application/)

Please visit [sat.qc.ca](https://sat.qc.ca) for more information about the SAT.
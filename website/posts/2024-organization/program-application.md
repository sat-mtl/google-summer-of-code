<!--
.. title: Program Application
.. slug: program-application
.. author: Christian Frisson
.. date: 2023-03-27 15:50:11 UTC-04:00
.. tags: organization
.. type: text
-->

<!-- edited by Edu Meneses - 2024-01-29 -->

By submitting this form, the Society for Arts and Technology (SAT) will be applying to be considered for inclusion in Google Summer of Code 2024. The content here, plus the contents of the [Organization Profile](/posts/2024-organization/organization-profile/)
and [Organization Questionnaire](/posts/2024-organization/organization-questionnaire/), will be reviewed by Google Program Administrators.

> 2024

Jan 23, 2023 - Nov 17, 2023

## Ideas List

Provide the URL for your organization's curated Ideas List for 2023. This is very important: Prospective GSoC Contributors will view this link to understand the kinds of projects available in your organization. A clean and simple presentation is best. Avoid links to unfiltered bugtrackers or other specialized tools.

Ideas List URL *

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)

## Mentors

How many Mentors does your Organization have available to participate in this program?

Number of Mentors *

> 8

Alphabetically:

* [Edu Meneses](https://gitlab.com/edumeneses)
* [Guillaume Riou](https://gitlab.com/guillaumeriousat)
* [Jean-Michaël Celerier](https://gitlab.com/jcelerier)
* [Manuel Bolduc](https://gitlab.com/bolducmanuel)
* [Olivier Gauthier](https://gitlab.com/ogauthier_sat)
* [Rochana](https://gitlab.com/rfardon)
* [Valentin Laurent]()

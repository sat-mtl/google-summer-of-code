<!--
.. title: SATIE Multispatializer Mapper
.. slug: idea-SATIE-multispatializer-mapper
.. author: Edu Meneses
.. date: 2023-02-07 09:10:00 UTC-05:00
.. tags: ideas, easy, 175 hours, SuperCollider, SATIE
.. type: filled
.. intern: Haokun Song
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!--
Please update post metadata:
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT)
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

SATIE Multispatializer Mappers

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment -->

[SATIE](https://gitlab.com/sat-metalab/SATIE) is SAT/Metalab's audio spatialization engine programmed in SuperCollider.
One interesting (and often unexplored) user case for spatializing sound in venues is coupling the venue's sound system with added speakers.
This practice is employed to increase spatial resolution.
However, the common method to implement this hybrid configuration (venue and added speakers) does not account for scenarios where it makes semantic sense to have separate speaker configurations while having sound objects shared between spatializers.
It is possible to implement [SATIE mappers](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/plugins.html) to overcome this challenge, but those mappers currently need to be programmed for each use case.

The GSoC contributor will work on creating generalizable SATIE mappers that will allow users to set arbitrary relative spatial positions for each [spatializer](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/spatializers.html).

## Expected outcomes

<!-- Please add 2-5 items below this comment -->

* One (or a series of) SATIE mappers (plugins) that allow adding geometric information (relative position) to each loaded spatializer, regardledd of the spatilizer characteristics (number, location, and distance of speakers). The expected functionalities of these general-case mappers are:
  * Provides a consistent workflow for setting relative spatializer positions, and sphere size for Vector Base Amplitude Panning (VBAP)
  * The mapper should be scalable to any amount of spatilizers, speakers, and ideally any panning algorithm implemented in SATIE 
* (Extra) Create SuperCollider/SATIE methods and OSC endpoints to facilitate the usage of the mappers

## Skills required/preferred

<!-- Please add 2-5 items below this comment -->

* required: knowledge of SuperCollider
* required: knowledge of panning algorithms
* preferred: previous experience in programming SuperCollider Quarks and custom methods  
* preferred: experience with Open Sound Control (OSC) in the context of music programming languages 
* preferred: understanding of the fundamentals of digital art technologies

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Edu Meneses](https://gitlab.com/edumeneses), [Michał Seta](https://gitlab.com/djiamnot)

## Expected size of project

<!-- Please write below this comment either: 175 hours or 350 hours -->

175 hours

## Rating of difficulty

<!-- Please write below this comment either: easy, medium or hard -->

easy

<!--
.. title: Using markerless motion capture for music generation, music creativity and music-dance interactions
.. slug: livepose-audiovisual-generation
.. author: Suresh Krishna, Christian Frisson
.. date: 2023-02-07 08:37:11 UTC-05:00
.. tags: ideas, hard, 350 hours, Sound/music generation, Image processing, Pose estimation, Python, MediaPipe, Wekinator, LivePose
.. type: closed
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Using markerless motion capture for music generation, music creativity and music-dance interactions

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

This is a new exploratory project that aims to use the recent advances in real-time markerless motion/pose capture software (SAT's [Livepose](https://gitlab.com/sat-mtl/tools/livepose) features pose detection backends implemented with [Google MediaPipe](https://github.com/google/mediapipe) and [OpenMMLab MMPose](https://github.com/open-mmlab/mmpose) and [NVIDIA trt_pose with TensorRT](https://gitlab.com/sat-mtl/tools/forks/trt_pose), and potentially [Jarvis](https://jarvis-mocap.github.io/jarvis-docs/) / [DeepLabCut](https://deeplabcut.github.io/DeepLabCut/)) to create a working framework whereby movements can be translated into sound with short-latency, allowing for example, gesture-driven new musical instruments and dancers who control their own music while dancing. The project will require familiarity with Python, and ability to interface with external packages like MediaPipe and Jarvis. Familiarity with low-latency sound generation, image processing, and audiovisual displays is an advantage, though not necessary. The development of such tools will facilitate both artistic creation, as well as scientific exploration of multiple areas, including for example - how people engage interactively with vision, sound, and movement and combine their respective latent creative spaces. Such a tool will also have therapeutic/rehabilitative applications in populations of people with limited ability to generate music and in whom agency and creativity in producing music have been shown to produce beneficial effects.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- Evaluate which pose backend implemented or to be implemented in [Livepose](https://gitlab.com/sat-mtl/tools/livepose) (among [Google MediaPipe](https://github.com/google/mediapipe) and [OpenMMLab MMPose](https://github.com/open-mmlab/mmpose) and [NVIDIA trt_pose with TensorRT](https://gitlab.com/sat-mtl/tools/forks/trt_pose), and towards potentially [Jarvis](https://jarvis-mocap.github.io/jarvis-docs/) / [DeepLabCut](https://deeplabcut.github.io/DeepLabCut/)) is best suited for low-latency audiovisual generation
- Implement or optimize the chosen pose backend (Python)
- Implement an audiovisual generation engine (Python or C++)
- Implement a [LivePose output filter](https://gitlab.com/sat-mtl/tools/livepose/-/tree/develop/livepose/outputs): either embedding directly audiovisual generation (Python) or communicating with an external engine via inter-operatibility protocols (libmapper, OSC, websocket)

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: comfortable with Python
* required: experience with image/video processing and using deep-learning based image-processing models
* preferred: familiarity with C/C++ programming, low-latency sound generation, image processing, and audiovisual displays, as well as MediaPipe or other markerless pose/motion capture tools is an advantage, though not necessary

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Suresh Krishna ](https://gitlab.com/bsureshkrishna) (McGill [m2b3 Lab](http://m2b3.lab.mcgill.ca)), [Christian Frisson](https://gitlab.com/ChristianFrisson) (LivePose), [Michał Seta](https://gitlab.com/djiamnot) (low-latency sound synthesis)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard

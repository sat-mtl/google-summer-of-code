<!--
.. title: Haptic and audio interaction design with Feelix supporting TorqueTuner and/or DeformableHapticSurfaces
.. slug: idea-feelix-torquetuner-forcehost-deformablehapticsurfaces
.. author: Christian Frisson
.. date: 2023-03-28 08:00:11 UTC-04:00
.. tags: ideas, hard, 350 hours, Feelix, TorqueTuner, DeformableHapticSurfaces, HapticFloor, haptics, audio, interaction design
.. type: filled
.. intern: Maxwell Gentili-Morin
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Haptic and audio interaction design with Feelix supporting TorqueTuner and/or DeformableHapticSurfaces

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

The SAT is developing a unique [Haptic floor](https://sat.qc.ca/en/haptic-floor) with force-feedback actuators that can individualize haptic feedback to participants in group settings to equip its dome hosting immersive arts. 
Haptic technologies, while omnipresent for people owning smartphones producing vibrotactile feedback (a subset of haptics), support an interaction design space that is yet to be explored by artists authoring immersive arts. 
We propose to onboard artists onto immersive haptic and audio interaction design, step by step, starting with low-cost affordable devices with 1 degree of freedom and authoring tools designed by designers for designers, towards a near future where haptic floors are deployed in immersive art spaces. 

[Feelix](https://feelix.xyz/) is a "haptic authoring tool developed to support the design and integration of force feedback and shape change in user interfaces" (the source code is available on [Feelix 2.0 GitHub repository](https://github.com/ankevanoosterhout/Feelix2.0)).
[TorqueTuner](https://github.com/idmil/torquetuner) is a force-feedback haptic device with 1 rotary degree of freedom and [ForceHost](https://gitlab.com/ForceHost) is a toolchain to generate firmware with [FAUST](https://faust.grame.fr/) to create audio-haptic applications embedded in TorqueTuner, both projects developed in collaboration with the SAT. 
[DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces) is a work-in-progress affordable open-source toolkit to discover interactive deformable surfaces with multiple linear degrees of freedom created at the SAT. 

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

The goals of this project idea are to: 

* add support of [TorqueTuner](https://github.com/idmil/torquetuner) and/or [DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces) in [Feelix](https://feelix.xyz/) (potentially through [ForceHost](https://gitlab.com/ForceHost))
* add support of audio in [Feelix](https://feelix.xyz/) as output modality (for producing sound and/or vibrotactile feedback) and input modality (for instance for controlling the haptic floor) (potentially through [ForceHost](https://gitlab.com/ForceHost))
* create a video of a demo showcasing how the newly-consolidated software and hardware toolkit facilitates exploring the design space of immersive haptic and audio interactions
* (bonus) co-author a related publication with your co-mentors

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: experience with C++ and ESP32 ([Feelix C++ Library](https://docs.feelix.xyz/downloads/c-library) and [TorqueTuner](https://github.com/idmil/torquetuner) and [DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces))
* required: experience with TypeScript/JavaScript ([Feelix](https://github.com/ankevanoosterhout/Feelix2.0))
* preferred: experience with Electron and Angular (electron is based on html/css/javascript and is very easy to get started with) ([Feelix](https://github.com/ankevanoosterhout/Feelix2.0))
* preferred: experience with haptics and audio interaction design

## Possible mentors

<!-- Please list yourself/yourselves. 2 possible mentors are more failsafe. -->

[Anke van Oosterhout](https://github.com/ankevanoosterhout/), 
[Christian Frisson](https://gitlab.com/christianfrisson), 
[Edu Meneses](https://gitlab.com/edumeneses), 
[Michał Seta](https://gitlab.com/djiamnot)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard

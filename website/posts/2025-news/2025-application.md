<!--
.. title: GSoC mentoring organization application
.. slug: 2025-application
.. author: Edu Meneses
.. date: 2025-01-27 10:33:00 UTC-05:00
.. tags: news
.. type: text
-->

[SAT](https://sat.qc.ca/) is proud to announce we applied for the [Google Summer of Code (GSoC) 2025](https://summerofcode.withgoogle.com/) as a mentor organization.

We will update this website with information as soon as Google publishes the list of accepted mentoring organizations. 

## About Google Summer of Code

From the official website:

[Google Summer of Code (GSoC)](https://g.co/gsoc)is a global, online program focused on bringing new contributors into open source software development. 
GSoC Contributors work with an open source organization on a 12+ week programming project under the guidance of mentors.

GSoC is a highly competitive program, so don't wait to the last minute to prepare! 
GSoC contributors should reach out to the organizations that interest them once organizations are announced on February 27, 2025.
Potential GSoC contributors can apply at [g.co/gsoc](https://g.co/gsoc).

Got questions about GSoC? E-mail [gsoc-support@google.com](mailto:gsoc-support@google.com).
If you have any questions on SAT and SAT's participation on the program, please e-mail us directly through our [GitLab GSoC project page](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code).  

## About The Society for Arts and Technology (SAT)

Founded in 1996, the [Society for Arts and Technology (SAT)](https://sat.qc.ca) is a non-profit organization dedicated to digital culture. 
With its triple mission as a center for the arts, training and research, the SAT is a gathering space for diverse talent, curiosity, and knowledge. 
It is recognized internationally for its active, leading role in developing technologies for immersive creation, mixed realities and telepresence. 
The Society for Arts and Technology is a place of collective learning that holds the promise of exploring technology to infuse it with more meaning, magic and humanity. 

SAT's public research center aims to be a place for the production of technological and methodological tools for the arts that can be easily transferable to other industries and environments.
The research and creation laboratory of the Society for Arts and Technology is dedicated to stimulating the emergence of innovative immersive experiences and making their design accessible to artists.

---------------------

<!--
.. title: Organization Questionnaire
.. slug: organization-questionnaire
.. author: Edu Meneses
.. date: 2025-01-16 14:51:11 UTC-05:00
.. tags: organization
.. type: text
-->

<!-- edited by Edu Meneses - 2025-01-16 -->

Please be thorough in your answers. All fields are required, unless noted otherwise.

## Why does your organization want to participate in Google Summer of Code? *

> We have gained collective experience with mentoring students (more than 20 internships so far) from local and remote universities. Our singular location in Quebec and Canada attracts students fluent in either English and/or French, from many countries and continents, to the extent of our current networks. With our second participation as Google Summer of Code organization, we want to continue expanding our horizons and benefiting from more exposure to reach more students around the world. After all, one core objective at SAT is to enable hybrid telepresence immersions, and we want our collaborations to happen in similarly distributed workflows: wherever and whenever.
>
> Our mission is to democratize technologies to enable people to experience and author multisensory immersions. By widening our community worldwide and initiated by international students, we want to ensure that a greater diversity of people will contribute by having their voice heard and by instilling their culture into the open-source tools that we create, including: enabling augmented collective interactions through deep learning for pose estimation through computer vision with no visual bias on people, improving social experiences with hybrid telepresence options with no barriers in accessibility.

## What would your organization consider to be a successful GSoC program? *

> Our first goal is educational: that students will learn software development, while ultimately contributing to open-source projects. Our second expectation is social: that GSoC contributors will merge their networks with ours and communicate about our joint work through at least one final presentation. Our third ambition is cultural: that GSoC contributors will influence and feed into artistic creations of immersive experiences.

## How will you keep mentors engaged with their GSoC contributors? *

> We designed our project ideas to feature two mentors, and expect at least one to be part of the core team of our organization with long-term experience in developing the open-source tools related to our project ideas and to contributors' proposals. We believe that this balance can bring several benefits: contributors get a greater diversity of channels to suit their interpersonal communication preferences, mentors can combine their complementary expertise (for example, one can focus on content, the other on context), mentors can relay each other to mitigate unforeseen circumstances along the GSoC 2023 timeline. Our GSoC organization administrator will maintain an open channel with contributors and mentors so that they all can get assistance in solving potential issues, including finding replacement mentors as last resort.

## How will you keep your GSoC contributors on schedule to complete their projects? *

> We ask contributors as part of their proposal to submit a list of tasks and milestones that they plan to validate through their project. Rather than a tentative weekly schedule over the course of the summer project, for which software developers in the making (and more confirmed developers as well) can rarely predict strategies and blockers beyond two weeks, we will ensure that objectives are met, if not re-evaluated, with frequent feedback loops. We understand that even after careful considerations at contributors selection, people reveal fascinating abilities and their skills evolve.To support their mutating schedule, we will help them structure their project management by offering templated repositories shared across all contributors and foster open review using gitlab issues and milestones.

## How will you get your GSoC contributors involved in your community during GSoC? *

> In the short term, we will ensure that contributors will strengthen their experience in software development and successfully contribute to projects by a series of merge/pull requests, of gradual complexity towards positive reinforcement, of their work into the main branches; and by welcoming them and integrating them in our team, involved in our daily and weekly gatherings. We hope that GSOC contributors of 2023 will become long-term contributors to our tools and help us consolidate usages of hybrid local/remote immersions, and own our tools in ways we did not foresee. In 2023 we have invited as co-mentors some of our external collaborators who have interests in using tools created or maintained at the SAT with their students to increase our chances to keep new contributors involved in the longer term.

## Is your organization part of any government?

* [ ] Yes
* [x] No

## GSoC Reference Contact (optional)

<!--
.. title: More about the SAT
.. slug: intro
.. author: Edu Meneses
.. date: 2025-01-16 14:49:25 UTC-05:00
.. tags: organization
.. type: text
-->

Please consult the following pages to get more details about our application as an organization for GSoC:

* [Organization Profile](/posts/2025-organization/organization-profile/)
* [Organization Questionnaire](/posts/2025-organization/organization-questionnaire/)
* [Program Application](/posts/2025-organization/program-application/)

Please visit [sat.qc.ca](https://sat.qc.ca) for more information about the SAT.
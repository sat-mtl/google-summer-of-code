<!--
.. title: Program Application
.. slug: program-application
.. author: Edu Meneses
.. date: 2025-01-16 15:50:11 UTC-04:00
.. tags: organization
.. type: text
-->

<!-- edited by Edu Meneses - 2025-01-16 -->

By submitting this form, the Society for Arts and Technology (SAT) will be applying to be considered for inclusion in Google Summer of Code 2025. The content here, plus the contents of the [Organization Profile](/posts/2025-organization/organization-profile/)
and [Organization Questionnaire](/posts/2025-organization/organization-questionnaire/), will be reviewed by Google Program Administrators.

> 2025

Jan 16, 2025 - Nov 17, 2025

## Ideas List

Provide the URL for your organization's curated Ideas List for 2025. This is very important: Prospective GSoC Contributors will view this link to understand the kinds of projects available in your organization. A clean and simple presentation is best. Avoid links to unfiltered bugtrackers or other specialized tools.

Ideas List URL *

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)

## Mentors

How many Mentors does your Organization have available to participate in this program?

Number of Mentors *

> 8

## Program Retention Survey

 The last program your organization participated in was:

2023

We're looking for more details on how many of your students/GSoC contributors from the above program are still active in your community today.

Number of students / GSoC contributors that your org accepted into GSoC 2023: 4

Number of those participants from GSoC 2023 that are still active in your community today?: 1

## Org Admins

* [Edu Meneses](https://gitlab.com/edumeneses)
* [Jean-Michaël Celerier](https://gitlab.com/jcelerier)


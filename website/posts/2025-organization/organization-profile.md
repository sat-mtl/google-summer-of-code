<!--
.. title: Organization Profile
.. slug: organization-profile
.. author: Edu Meneses
.. date: 2025-01-27 10:52:00 UTC-04:00
.. tags: organization
.. type: text
-->

## Website URL for Organization *
> [https://sat.qc.ca](https://sat.qc.ca)

## Organization Logo *
> ![](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/LOGO_SAT_noir.png?inline=false)

## Tagline *

> Technological tools for creativity and industry

## Primary Open Source License *

- [x] GNU General Public License version 3 (GPL-3.0)  

## What year was your project started? *

- [x] 1996

## Link to your source code location *

> [https://gitlab.com/sat-mtl](https://gitlab.com/sat-mtl)

## Organization Categories

Select which categories fit your organization best. You may select up to 2 categories. This helps GSoC contributors filter the large list of organizations by their interests.

Select at least 1:
(warning) Select no more than 2. 

 * [x] Artificial Intelligence
 * [ ] Data (databases, analytics, visualization, etc)
 * [ ] Development tools (version control systems, CICD tools, text editors, issue managers, q/a tools, etc)
 * [ ] End user applications
 * [ ] Infrastructure and cloud (hardware, software defined infrastructure, cloud native tooling, orchestration and automation, etc)
 * [X] Media (graphics, video, audio, VR, streaming, gaming, content management, etc)
 * [ ] Operating systems
 * [ ] Programming languages (libraries, package managers, testing tools, etc)
 * [ ] Science and medicine (healthcare, biotech, life sciences, academic research, etc)
 * [ ] Security (tools and frameworks)
 * [ ] Social and communications (Blog, chat, forums, wikis, etc)
 * [ ] Web (tools and frameworks)
 * [ ] Other 

## Ideas List URL

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/categories/ideas/)

## Organization Technologies

Enter up to 5 keywords for the primary specific technologies your organization uses. Examples: Python, Javascript, MySQL, Hadoop, OpenGL, Arduino 

## Organization Technologies *

> Python, Javascript, C++, Qt, ESP32

## Organization Topics

> graphics, multimedia, computer vision, telepresence, digital arts

## Organization Description

Describe what it is your organization does. This information will also be included in the archive once the program has ended.

## Description *

> The Society for Arts and Technology [SAT] is a non-profit organization recognized internationally for developing and creatively using immersive technologies, computer vision, embedded systems, and connectivity tools. Founded in 1996, its triple mission as a center for the arts, training and research, is to enable and host teleimmersive multisensorial experiences.
> 
> SAT creates open-source tools for pose detection, orchestration, gestural control, interaction, tele-copresence, audio spatialization, haptic feedback, and immersive multisensory multimedia authoring.

## Contributor Guidance

Provide your potential contributors with a page containing tips on how to write a successful proposal for your organization. Let them know what you want included, how you want it structured, and how to best get in touch. Examples.

Link for contributor guidance (Wiki, blog, et cetera) *

> [https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-proposals/proposal-welcome/](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-proposals/proposal-welcome/)

## Communication Methods

How do you want potential contributors to interact with your organization? Select methods that your community uses daily, as you will receive many inquiries if your org is selected. 

* [x] Chat > URL: [https://matrix.to/#/#sat-gsoc-2022:matrix.org](https://matrix.to/#/#sat-gsoc-2022:matrix.org)
* [ ] Mailing List / Forum > URL: [https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues)

##  Social Communication Methods 

* Social Media - X > URL: [https://twitter.com/SATmontreal](https://twitter.com/SATmontreal)
* Social Media - Facebook > URL: [https://www.facebook.com/SATmontreal](https://www.facebook.com/SATmontreal)
<!--
.. title: Exploration of Advanced Rendering Pipelines in ossia score with Raytracing for Fulldome Content
.. slug: idea-non-standard-volumetric-render-pipelines
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 15:01:00 UTC-05:00
.. tags: ideas, hard, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

Exploration of Advanced Rendering Pipelines in ossia score with Raytracing for Fulldome Content

## More detailed description of the project

This project aims to explore and integrate cutting-edge rendering pipelines into ossia score, with a particular focus on using raytracing APIs to improve the efficiency and visual quality of fulldome content rendering. Fulldome rendering traditionally requires multiple render passes to project onto a dome, which can be computationally expensive. Raytracing offers the potential to render content for the entire dome in a single pass, significantly reducing overhead. The project will investigate various advanced rendering techniques including HDR rendering, meshlet-based rendering, volumetric/voxel-based rendering, and SDF-based rendering. Furthermore, a graphical composer for SDF-based 3D content might be developed. The student will benchmark and compare different rendering pipelines, identifying the most suitable options for various types of content and performance targets within ossia score.

## Expected outcomes

*   Implementation of at least one advanced rendering pipeline (HDR, Meshlet, Volumetric/Voxel, SDF) within ossia score.
*   Investigation and integration of a raytracing API (e.g., Vulkan Ray Tracing, DirectX Raytracing) for fulldome rendering.
*   Development of a prototype raytracing-based fulldome rendering solution within a Qt RHI example, demonstrating single-pass rendering.
*   Performance analysis and comparison of different rendering pipelines, including benchmarks for fulldome content.
*   Documentation of the implemented rendering pipelines and their usage within ossia score.

## Skills required/preferred

*   Required: Strong C++ programming skills.
*   Required: Solid understanding of 3D graphics principles and rendering techniques.
*   Required: Experience with at least one modern graphics API (OpenGL, Vulkan, DirectX).
*   Preferred: Experience with raytracing APIs and algorithms.
*   Preferred: Familiarity with HDR rendering, meshlet rendering, volumetric rendering, or SDF-based rendering techniques.
*   Preferred: Knowledge of fulldome rendering techniques and projections.
*   Preferred: Experience with performance profiling and optimization of graphics code.

## Possible mentors

*   [Jean-Michaël Celerier](https://github.com/jcelerier)
*   [Manuel Bolduc](https://gitlab.com/bolducmanuel) 

## Expected size of project 

350 hours

## Rating of difficulty 

hard
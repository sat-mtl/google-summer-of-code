<!--
.. title: Porting Puara-gestures to Avendish and creating ossia score objects for each gestural descriptor 
.. slug: idea-porting-puara-ossia
.. author: Edu Meneses
.. date: 2025-01-29 11:51:00 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Porting [Puara-gestures](https://github.com/Puara/puara-gestures) to [Avendish](https://github.com/celtera/avendish) and creating [ossia score](https://github.com/ossia/score) objects (processes) for each gestural descriptor

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

[Puara-gestures](https://github.com/Puara/puara-gestures) is a library for creating and managing high-level gestural descriptors using data from various sensors such as accelerometers, gyroscopes, and magnetometers.
It helps instrument designers and ioT developers extract meaningful data from sensors and use them to control anything via the network.
Porting (wrapping) the library classes to Avendish will allow the creation of VST/LV2 plugins for audiovisual/artistic projects and the compilation of those classes as individual objects.
Further, adding them as objects in [ossia score](https://github.com/ossia/score) will open possibilities for orchestrating and automating both physical and virtual elements using data gathered from a distributed network of sensors and actuators. 

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* A wrapper for each puara-gestures class in Avendish 
* An object created from each wrapper
* At least one test case of a VST plugin created from Avendish

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: 
    * C/C++
* preferred:
    * Qt

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Edu Meneses](https://gitlab.com/EduMeneses)
- [Jean-Michaël Celerier](https://gitlab.com/jmcelerier)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

medium
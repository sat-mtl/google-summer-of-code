<!--
.. title: Object Detection on Point Cloud Streams 
.. slug: idea-object-detection-point-cloud
.. author: Manuel Bolduc
.. date: 2025-02-06 09:10:00 UTC-05:00
.. tags: ideas, hard, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Object Detection on Point Cloud Streams

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

[LivePose](https://gitlab.com/sat-mtl/tools/livepose/livepose) is a command-line tool for detecting skeletons using Human Pose Estimation (HPE) models on video streams and streaming output data on the network, so that it can be used in interactive applications. The basic architecture of LivePose could be adapted to accommodate for different types of input data, such as point cloud streams.

The first objective of this project is therefore to prototype a pipeline in Python language for applying deep learning models in real-time on streaming point cloud data. The second objective, if time allows, is to implement such a pipeline as an additional feature of the LivePose software. 
 

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* Familiarization with techniques for point cloud streaming in Python using libraries such as Open3D, OpenCV
* Conceiving a Python pipeline using the [ONNX](https://github.com/onnx/onnx) framework for object detection on point clouds
* Evaluation of the pipeline for real-time performance
* Feature request for integration of the pipeline in the LivePose software

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: openness to read and learn about new stuff
* required: willingness to ask questions when stuck
* preferred: some experience with programming (but not a prerequisite!)

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

[Manuel Bolduc](https://gitlab.com/bolducmanuel)
[Jean-Michaël Celerier](https://gitlab.com/jmcelerier)
[Bruno Colpron](https://gitlab.com/brunocolpron)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard

<!--
.. title: CLAP Audio Plug-in Integration in ossia score
.. slug: idea-clap-plug-in-support-ossia-score
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 14:30:00 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

CLAP Audio Plug-in Integration in ossia score

## More detailed description of the project

ossia score, as a DAW for interactive audiovisual performances, benefits from a wide range of audio plug-ins.  This project aims to integrate the CLAP (CLEVER Audio Plug-in) format into ossia score, providing a modern and efficient alternative to existing plug-in standards like VST2/3 or LV2.  CLAP offers several advantages, including improved stability, thread safety, and parameter modulation capabilities, leading to a better overall experience for users creating complex audio-visual compositions. The project involves implementing the CLAP host API within ossia score, allowing users to load, control, and process audio through CLAP plug-ins directly within the score environment, by using as starting point the existing audio plug-in backends for LV2, VST2, VST3, JSFX and Faust. This will expand the sonic palette available to artists and enhance the interactive audio capabilities of ossia score.

## Expected outcomes

*   Implementation of a CLAP host within ossia score.
*   Ability to load and instantiate CLAP plug-ins within ossia score.
*   Cross-platform display of CLAP plug-in GUIs.
*   Mapping of CLAP parameters to ossia score automation lanes and interactive controls.
*   Support for CLAP's thread-safe audio processing in ossia score's engine.
*   Testing and documentation of the CLAP integration.

## Skills required/preferred

*   Required: C++ programming skills.
*   Required: Experience with audio programming and signal processing.
*   Preferred: Familiarity with the CLAP API and its features or at least one audio plug-in API.
*   Preferred: Experience with GUI programming using Qt or similar frameworks (as ossia score uses Qt).
*   Preferred: Knowledge of DAW architecture and audio routing concepts.

## Possible mentors

*   [Jean-Michaël Celerier](https://github.com/jcelerier)
*   [Vincent Berthiaume](https://github.com/vberthiaume)

## Expected size of project 

350 hours

## Rating of difficulty 

medium

<!--
.. title: Compute-Agnostic Compilation of Audio and Media Processing Objects for FPGA Acceleration
.. slug: idea-adaptivecpp-objects-for-compiling-to-cpu-gpu-and-fpga
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 15:05:00 UTC-05:00
.. tags: ideas, hard, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

Compute-Agnostic Compilation of Audio and Media Processing Objects for FPGA Acceleration

## More detailed description of the project

Media creation utilizes a variety of audio and media processing objects for interactive performances. This project aims to enable the offloading of computationally intensive processing tasks to heterogeneous hardware, such as FPGAs or GPUs, by leveraging compute-agnostic programming models like SYCL or AdaptiveCpp. The project will involve creating a compilation pipeline that transforms existing audio and media processing objects into code suitable for execution on FPGAs, using AdaptiveCpp to abstract away the underlying hardware details. This allows for targeting different platforms with minimal code changes and enables significant performance gains through hardware acceleration and offloading. The project would explore optimizing data transfer between the host system and the FPGA and developing efficient implementations of common audio and media processing algorithms in AdaptiveCpp.

## Expected outcomes

*   Development of a compilation pipeline that translates audio and media processing objects through AdaptiveCpp compiler or an equivalent technology.
*   Implementation of any required portability shim to enable compilation of the existing library of objects  (e.g., filters, FFTs, convolution).
*   Performance evaluation of the FPGA-accelerated processing objects compared to CPU-based implementations.
*   Documentation of the compilation pipeline and its usage within ossia score.
*   Proof-of-concept implementation on a target FPGA platform.

## Skills required/preferred

*   Required: Strong C++ programming skills.
*   Required: Understanding of audio and media processing algorithms.
*   Required: Experience with parallel programming concepts.
*   Preferred: Experience with SYCL or AdaptiveCpp.
*   Preferred: Familiarity with FPGA development tools and hardware architectures.
*   Preferred: Knowledge of hardware acceleration techniques.
*   Preferred: Understanding of compiler design principles.

## Possible mentors

*   [Jean-Michaël Celerier](https://github.com/jcelerier)
*   [Edu Meneses](https://github.com/edumeneses) 
*   David Ledoux

## Expected size of project 

350 hours

## Rating of difficulty 

hard
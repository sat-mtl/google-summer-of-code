<!--
.. title: GPU rendering for the score GUI
.. slug: idea-gpu-rendering-score-GUI
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 14:52:00 UTC-05:00
.. tags: ideas, hard, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

GPU Rendering for ossia score's GUI using the Qt RHI

## More detailed description of the project

ossia score's current GUI relies on the older QGraphicsScene API in Qt, which can be a performance bottleneck, especially with complex scores and interactive elements in 4K or greater resolutions. This project aims to modernize the rendering pipeline by leveraging Qt's Rendering Hardware Interface (RHI) for GPU-accelerated rendering. Specifically, the project will involve integrating a library such as QNanoPainter (with the RHI backend) to replace the QGraphicsScene implementation. 
This will improve performance, enable smoother animations, and unlock possibilities for more advanced visual effects in the score interface such as direct rendering of GPU content. The challenge lies in migrating the existing QGraphicsScene-based rendering to the RHI-based solution while maintaining compatibility and ensuring visual fidelity as well as support on lower-end platforms such as Raspberry Pi.


Useful links:

- https://github.com/QUItCoding/qnanopainter 
- https://github.com/alpqr/qrhinanovg/

## Expected outcomes

*   Integration of a GPU rendering library (e.g., QNanoPainter) into ossia score.
*   Migration of key GUI elements (score timeline, objects and widgets within) to be rendered using the new RHI-based pipeline.
*   Improved performance and responsiveness of the ossia score GUI, especially with complex scores.
*   Optimization of GPU resource usage to ensure efficient rendering.
*   Thorough testing and debugging to ensure visual fidelity and stability.
*   Documentation of the RHI integration and any necessary code changes.

## Skills required/preferred

*   Required: Strong C++ programming skills.
*   Required: Understanding of GPU rendering concepts and pipelines.
*   Preferred: Experience with Qt and its graphics APIs, including QGraphicsScene and QPainter.
*   Preferred: Experience with Qt's RHI (Rendering Hardware Interface) or any other similar RHI such as Unreal Engine RHI, WebGPU or SDL3's GPU API.
*   Preferred: Knowledge of performance profiling and optimization techniques for GPU rendering.

## Possible mentors

*   [Jean-Michaël Celerier](https://gihub.com/jcelerier)
*   [Sarah Al Mamoun](http://github.com/samamou)

## Expected size of project 

350 hours

## Rating of difficulty 

hard
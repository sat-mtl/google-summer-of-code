<!--
.. title: A library-content-manager
.. slug: idea-library-content-manager
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 14:45:00 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

A library Content Manager for Media Software

## More detailed description of the project

This project aims to create a robust and versatile library content manager tailored for large-scale media software. The core challenge is efficiently managing diverse media types (audio, video, VST plugins, presets, etc.) and their associated metadata. The library manager will provide tools for automated scanning, categorization based on customizable rules, and serving media content. It should also feature background processing for media information extraction (duration, etc.) and a system for handling arbitrary metadata. Special attention should be paid to performance and concurrency to ensure responsiveness even with very large media collections. The system could potentially share the content over the network or at least over local sockets in an API that would enable it to be useable across a wide range of software.

## Expected outcomes

*   A command-line tool and/or library and/or server backend for scanning media files and plugins based on user-defined rules.
*   A system for extracting and storing media metadata (duration, codec, etc.) in a performant manner.
*   A mechanism for defining and managing arbitrary metadata associated with media items (e.g., DSP include paths, shader dependencies).
*   An API for accessing and querying the library content, including filtering by category, tag, and metadata.
*   A prototype implementation integrating the library content manager with ossia score.

## Skills required/preferred

*   Required: C++ programming skills.
*   Required: Experience with file system operations and data structures.
*   Required: Familiarity with database systems (e.g., SQLite) or alternative persistent storage solutions.
*   Preferred: Knowledge of media formats (audio, video, VST, etc.).
*   Preferred: Experience with multithreading and concurrency.

## Possible mentors

*   [Jean-Michaël Celerier](https://github.com/jcelerier)
*   [Edu Meneses](https://github.com/edumeneses) 

## Expected size of project 

350 hours

## Rating of difficulty 

medium
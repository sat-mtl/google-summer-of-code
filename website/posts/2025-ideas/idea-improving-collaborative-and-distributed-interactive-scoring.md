<!--
.. title: Improving collaborative & distributed interactive scoring
.. slug: idea-improving-collaborative-and-distributed-interactive-scoring
.. author: Sarah Al Mamoun
.. date: 2025-02-12 15:10:00 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--SAT mentors, please use this template to propose internal SAT ideas
The contents of this template can be directly copied to an idea file for the MR
Please follow the instructions on https://gitlab.com/sat-mtl/collaborations/google-summer-of-code#target-audience-1-->

## Project title/description

<!--Please add title below this comment to match the value in post title metadata-->

Improving collaborative & distributed interactive scoring

The goal is to make **remote collaboration** in [ossia score](https://github.com/ossia/score) more intuitive and efficient, making it easier for artists, musicians, and developers to create interactive media compositions in real-time, across multiple locations. 

## More detailed description of the project

<!--Please write 2-5 sentences below this comment-->

With "Teleprescene" being an important area of our research at the SAT, we are interested in improving the open source tools we develop / use / offer to artists at during residencies. The idea of this project would be to make **remote collaboration** more intuitive in [ossia score](https://github.com/ossia/score), a feature that’s already experimental in release [3.0.11](https://github.com/ossia/score/releases/tag/v3.0.11), where score introduced experimental [_collaborative features_](https://ossia.io/posts/distributed/%C2%A0).

This involves some backend (c++) and frontend (Qt/QML) improvements to the exisiting [iscore-addon-network](https://github.com/ossia/iscore-addon-network) ossia score plugin. The software is built around a modular architecture where most of its functionality comes from plug-ins. To support different kinds of plugins, score offers two APIs that allow for customization of the software. More on developing in score [here](https://ossia.io/score-docs/development).

## Expected outcomes

<!--Please add 2-5 items below this comment-->

* Backend and frontend improvements
  * Adding feedback/alerts on which _machine_ is handling what part of the score_,_ basically the UX of editing conflicts.
    * For example, “participant x is currently editing \[ \] ” //  “participant x applied X transformation”
  * Shareable links and invitations + saved sessions to quickly connect back 
    * ex: _Generate session link_ (copy) 127.0.0.1:12345
* New features implementation
  * Embedded Device support
    * Support a mode where a desktop instance controls an embedded device (with the host as the source of truth for I/O devices), in the case of an installation or residency where artists are using/developing embedded systems.
  * Integration of a chat system
    * If artists are working remotely on a piece, it would be important if they have a way of communicating in-App

## Skills required/preferred

<!--Please add 2-5 items below this comment-->

* required:
  * C/C++
  * git, source control
* preferred:
  * Qt/QML
  * embedded systems design

## Possible mentors

<!--Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe.-->

@jmcelerier @salmamoun 

## Expected size of project

<!--Please write below this comment either: 175 hours or 350 hours-->

350 hours

## Rating of difficulty

<!--Please write below this comment either: easy, medium or hard-->

medium

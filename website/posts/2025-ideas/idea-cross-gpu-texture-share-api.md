<!--
.. title: A cross-platform API for GPU texture sharing
.. slug: idea-cross-gpu-texture-share-api
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 14:50:00 UTC+01:00
.. tags: ideas, hard, 350 hours, ossia
.. type: text
-->

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

A cross-platform API for GPU texture sharing

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

Every desktop operating system provides a way to share GPU textures between multiple processes.
This is especially useful in the media arts field: a software can be specialized in particles rendering, another in video mapping, and both can then be combined at run-time with no CPU performance cost for inter-process sharing.
On Windows, [Spout](https://spout.zeal.co/) is the standard way to use these OS APIs and share a texture across applications ; on macOS, it is [Syphon](https://syphon.github.io/), and on Linux, this is achievable through [PipeWire](https://pipewire.org/) which wraps [DMA-BUF](https://blaztinn.gitlab.io/post/dmabuf-texture-sharing/).
This means that cross-platform applications wanting to provide texture sharing have to implement multiple different backends, each matching the different platform APIs which leads to a lot of code duplication.
This project is about providing a zero-cost cross-platform abstraction over these useful APIs, which would allow software authors to interoperate with Spout / Syphon / PipeWire without having to duplicate shared abstraction code, in particular by extracting the existing code from ossia score and wrapping it in a separate library.

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

- An open-source library which allows to share a GPU texture across applications.
- The library should be able to import / export the texture multiple graphics APIs, for instance OpenGL, Vulkan, Metal, D3D. 
- The open-source media arts ecosystem will become more cross-platform-friendly :-)

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 
 
- C++20
- Graphics programming experience in at least one modern GPU API.
- Cross-platform experience

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

- [Jean-Michaël Celerier](https://gitlab.com/jmcelerier)
- [Manuel Bolduc](https://gitlab.com/BolducManuel)

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard
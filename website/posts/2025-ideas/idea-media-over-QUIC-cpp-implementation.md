<!--
.. title: A Media-over-QUIC C++ implementation 
.. slug: idea-media-over-QUIC-cpp-implementation
.. author: Jean-Michaël Celerier
.. date: 2025-02-06 09:35:00 UTC-05:00
.. tags: ideas, hard, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

A Media-over-QUIC C++ implementation

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

QUIC has been introduced by major industry members (Google, Microsoft, etc.) as a new protocol
to replace and improve efficiency of web communications.

The Media-over-QUIC initiative aims to provide an alternative to WebRTC, based on QUIC.

While many QUIC implementations in C++ exist, none so far cover the Media-over-QUIC protocol specification.
We propose the following work for this internship: 

- Choose an existing QUIC implementation in C++
- Implement Media-over-QUIC on it to stream e.g. audio-visual media in the lowest possible latency.
- If the Media-over-QUIC specification proves too unwieldy, it is acceptable to implement a custom protocol for unicast media streaming over QUIC.


## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* A C++ library allowing to stream audiovisual media over the QUIC protocol.
* A sample web page able to receive QUIC audio & video feeds.

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: C++ skills
* required: experience with networking & media
* preferred: knowledge of asynchronous I/O mechanism, for instance with boost.asio or C++ senders / receivers

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

@jmcelerier 
@ogauthier_sat 

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

350 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

hard

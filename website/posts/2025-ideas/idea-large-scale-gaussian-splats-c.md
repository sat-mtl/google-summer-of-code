<!--
.. title: GPU-Independent Pipeline for Large-Scale Point Cloud and Gaussian Splat Rendering
.. slug: idea-large-scale-gaussian-splats-c
.. author: Jean-Michaël Celerier
.. date: 2025-02-20 15:00:00 UTC-05:00
.. tags: ideas, medium, 350 hours
.. type: text
-->

<!-- Most content copied from idea-welcome.md made by Christian Frisson -->


<!-- Please follow and keep (do not delete) all of these comments! -->

<!--
This template has been created using the following resources:
https://google.github.io/gsocguides/mentor/defining-a-project-ideas-list
https://google.github.io/gsocguides/mentor/making-your-ideas-page
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with idea-, identical as filename)
* author (comma-separated list of mentors)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: ideas, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

## Project title/description

GPU-Independent Pipeline for Large-Scale Point Cloud and Gaussian Splat Rendering

## More detailed description of the project

This project focuses on developing a robust and GPU-independent C++ rendering pipeline for large-scale point clouds and potentially Gaussian splats: some approaches exist but are restricted to CUDA-based systems. The goal is to enable media arts software to visualize and interact with datasets containing millions or even billions of points efficiently, while maintaining visual quality. The pipeline will be drawing inspiration from approaches such as used by Magnopus and in the Skye project. It will prioritize interactive frame rates, effective occlusion handling, and high-quality visual representation, avoiding performance bottlenecks associated with naive point rendering methods. The project will involve exploring techniques such as compute-based rendering, progressive rendering, and efficient data management to handle massive point cloud datasets.

## Expected outcomes

*   Implementation of a GPU-independent rendering pipeline for point clouds, based on a GPU RHI.
*   Potential extension to Gaussian Splats.
*   Support for loading, sorting, processing and visualizing large-scale point cloud datasets (millions to billions of points).
*   Implementation of efficient occlusion culling and level-of-detail (LOD) techniques to optimize rendering performance.
*   Exploration and implementation of progressive rendering techniques for improved visual quality and responsiveness (similar to Skye).
*   Performance profiling and optimization to achieve interactive frame rates.
*   Documentation of the rendering pipeline and its usage.

## Skills required/preferred

*   Required: Strong C++ programming skills.
*   Required: Deep understanding of GPU rendering principles and techniques (OpenGL, Vulkan, DirectX).
*   Required: Experience with compute shaders and GPU data management.
*   Preferred: Experience with point cloud processing and rendering algorithms.
*   Preferred: Familiarity with Gaussian splatting techniques.
*   Preferred: Knowledge of performance profiling and optimization tools.
*   Preferred: Familiarity with linear algebra and 3D graphics concepts.

## Possible mentors

*   [Jean-Michaël Celerier](https://github.com/jcelerier)
*   Bruno Colpron

## Expected size of project 

350 hours

## Rating of difficulty 

medium
---
title: Welcome to our 2023 GSoC Contributors! 
slug: welcome-contributors
author: Christian Frisson
date: 2023-05-11 14:29:11 UTC-04:00
tags: contributions
type: text
---

We are pleased to announce that 5 contributors were selected for SAT's second year participating in GSoC 2023. 

We are excited to welcome in our team starting May 4:

* [Fanny Cacilie](/authors/fanny-cacilie/)
* [Haokun Song](/authors/haokun-song/)
* [Maxwell Gentili-Morin](/authors/maxwell-gentili-morin/)
* [Pedro Andrade Ferreira Sobrinho](/authors/pedro-andrade-ferreira-sobrinho/)
* [Tarek Yasser](/authors/tarek-yasser/)
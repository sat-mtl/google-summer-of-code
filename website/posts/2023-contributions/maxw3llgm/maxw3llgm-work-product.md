---
title: Work Product for Audio to Haptic interaction design with Feelix supporting TorqueTuner
slug: work-product-maxw3llgm-audio-to-haptic-interaction-design-with-feelix-supporting-torquetuner
author: Maxwell Gentili-Morin
date: 2023-08-27 12:02:11 GMT-4:00
tags: contributions, products, TorqueTuner, Feelix, JavaScript, TypeScript, electron, microcontroller
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Maxwell Gentili-Morin
* website: [LinkedIn](https://www.linkedin.com/in/maxwell-gentili-morin-a14b3b1a2/)
* gitlab username: Maxw3llGM
* timezone: GMT-4

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->
Audio to Haptic interaction design with Feelix supporting TorqueTuner
...

## Short description of work done
The first and most extensive part of my Google Summer of code 2023 project was dedicated to adding support for the Haptic Knob called TorqueTuner in Feelix. Feelix export effects with a custom config file to the TorqueTuner. The TorqueTuner's hardware was also extended to now receive such files and playback the effect.

A development environment for Feelix on Apple Silicon Macs was also created.

Feelix also has the capability of creating and transmitting Midi Effects for the TorqueTuner to then playback into any usb enabled musical interface. This opens the avenue of developing haptic effects that can be used in conjunction of common musical instrument over midi and to continue the dev process of Feelix as a tool for haptic audio.


<!-- 
Please write a short description of work done. 
(150-200 words)
-->



## What code got merged
[Added TorqueTuner and Midi to Feelix](https://github.com/ankevanoosterhout/Feelix2Dev/commit/c650b88442bb65eed4bfeaf39a86d01850671f04)

* Description
  * Extended Feelix to accept the TorqueTuner as a Motor type
  * Made transmission of data possible to the TorqueTuner from Feelix
  * Added Midi Effect type and capability of transmitting it out.
  * Fixed issue with exe-icon-extractor not being an optional node-module for Mac-Os
  * Fixed a MacOS specific problem where the menu bar did not change when switching between windows. 
* Major Commits
  * [Midi transmission to TorqueTuner](https://github.com/ankevanoosterhout/Feelix2.0/commit/a272027db964a004c14ba81b25a0b8bae243bf27#diff-2d8674e670ce3fab211f4ea7503bdb84e94493edb07534696a558da33937d073) 
  * [Addition of TorqueTuner support in Feelix](https://github.com/ankevanoosterhout/Feelix2.0/commit/bfc5aceda55ced295ce216be3109569432325fd6)

[Merging of Feelix Firmware with TorqueTuner](https://github.com/ankevanoosterhout/Feelix-cpp/pull/2)

* Description 
  * Graphed on Feelix to the TorqueTuner's Firmware.
  * Added the reception and playback of simple Midi CC information, and torque based effects whilst preserving previous effects found on the TorqueTuner.
  * Midi information can be transmitted over Serial for Midi interfaces to and play.


## What’s left to do
To continue the extension of Feelix with TorqueTuner, there is still some features that need to be added in the hardware side of the software. Better control of the uploaded effects on the TT are required to cover some edge cases for torque. The extension of effect types to the TT like velocity and potentially position. Also, a set of potential functions to modulate the effect array in creative ways. In true music producer fashion, there should be a way to apply modification to the uploaded effects in code.

Midi was a new addition to the Feelix project and to the TorqueTuner this summer. We began to add what we believed to be the most important parts of crafting midi effects in Feelix along with a very basic method of having them played on the TorqueTuner. First, a fix is required to have the midi effects sub values width be synced accordingly. More research is required to understand what would be welcome a addition on the hardware side for controlling and transmitting these midi effects.

Finally, on the line of communication protocols, the addition of say OSC over WIFI could be of interest.



<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->
---
title: Work Product for Building Immersive Learning Experiences
slug: work-product-pedroansa1-building-immersive-learning-experiences
author: Pedro Andrade 
date: 2023-08-21 15:02:11 GMT-4:00
tags: Satellite, 3D, Explorable Explanation, Mozilla Hubs
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Pedro Andrade Ferreira Sobrinho
* website: [https://github.com/pedroansa](https://github.com/pedroansa)
* gitlab username: [@pedroansa1](https://gitlab.com/pedroansa1)
* timezone: GMT-4:00

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

Building Immersive Learning Experiences

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

In my 3-month Google Summer of Code project for 2023, I focused on learning, developing, and integrating a new immersive room into SAT's 3D environment, Satellite. This room leverages Mozilla Hubs to offer an immersive learning experience, aiding the comprehension of light-related concepts within a virtual setting. The heart of this experience lies in a JavaScript component crafted using the Three.js framework. It simulates a ray source in the virtual space and includes a user interface to manipulate diverse light attributes.

Project highlights include:

* Simulating a ray source within the virtual environment.
* Designing a user interface to manipulate light properties—angle, position, decay, axis, and rotation speed.
* Augmenting learning by visualizing light rays as lines extending from the source to surfaces in the virtual world.
* Ensuring changes in light attributes alter the color and behavior of rays, enhancing comprehension of raycasting and light simulation.
* Enabling synchronization of the light component across users in the Mozilla Hubs room for collaborative learning.

Further, I developed a user tutorial, providing clear guidance on manipulating the light source and interacting with the environment. A practical test box lets users observe light interaction with diverse surfaces.

Another addition is the visualization of ray lines refracting towards a camera. This feature deepens understanding of light refraction principles and offers interactive exploration.

The project provides a dynamic learning environment, yet there's room for improvement, including extending light refraction to all surfaces and conducting user testing for valuable insights.

## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

The following merge requests were successfully merged into Serveur-injection repository which is the injection server used to inject components and rooms into the Satellite environment:

* [Deploy new room for GSOC project -DEPLOY](https://gitlab.com/sat-mtl/satellite/serveur-injection/-/merge_requests/39)

Description: After implementing and testing in local, this MR firstly deployed the room and all its components so we could test the compability and start user-testing. This single MR acumulates the contribution of the entire project since it was uploaded in a local environment first.

Here we are able to see the new component rayCastLight and the room configuration.

* [Update sounds for raycasting room -DEPLOY](https://gitlab.com/sat-mtl/satellite/serveur-injection/-/merge_requests/43)

Description: Some changes need to be done to include all the audios for the popups, some URL were not working and some files were in the wrong format

* [ Fix timeout from popup -DEPLOY ](https://gitlab.com/sat-mtl/satellite/serveur-injection/-/merge_requests/47)

Description: Some fixes needed to be done so we could include the narrator for the popups

## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* ...

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

While the core objectives of the project have been achieved, there are areas that require further attention and improvement. Specifically, enhancing the light refraction feature to interact with all surfaces within the virtual environment is a priority for future work. This improvement would provide users with a more accurate and comprehensive understanding of light refraction principles and its interaction with various materials.

In addition, the project would greatly benefit from user testing to collect valuable feedback for further improvements. Although a tutorial and a Google Form for user testing were prepared, the limited time available prevented the implementation of user testing to gather insights and enhance the project based on user feedback.

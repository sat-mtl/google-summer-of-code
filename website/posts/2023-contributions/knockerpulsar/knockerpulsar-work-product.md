---
title: Work Product for Replace OpenGL with a multi-API Rendering Library
slug: replace-opengl-by-a-multi-api-rendering-library
author: Tarek Yasser
date: 2023-05-26 19:00:00 UTC+03:00
tags: contributions, products
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Tarek Yasser
* website: [github](https://github.com/KnockerPulsar), [linkedin](https://www.linkedin.com/in/tarek-ismail-263a031b4/)
* gitlab username: knockerpulsar
* timezone: UTC+3

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->
Replace OpenGL by a multi-API rendering library

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

- Converted Splash to run on the Raspberry Pi 4 by using OpenGL ES, This consisted of a few phases: 
    1. Converting OpenGL 4.5 calls to their corresponding OpenGL ES 3.2 counterparts, 
    2. Converting shaders to work with OpenGL ES 3.2 (version and floating point precision directives, various minor fixes), 
    3. Removing uniform initializations from shaders as it isn't supported by OpenGL ES 3.2,
    4. Merging the existing OpenGL 4.5 and OpenGL ES 3.2 code paths,
    5. Refactoring out graphics code to separate classes to allow further extension,
    6. Lots of code cleanups and bug fixes

- Extended Splash's unit testing code to allow for image comparisons: I thought this might be a good idea to start with to familiarize myself with Splash's testing code. It's always a good idea to start a refactor with some tests to ensure your refactor maintains the same output. This is not yet used significantly, but can be used as a basis for more graphics testing.
- Researched various rendering libraries ([magnum](https://github.com/mosra/magnum), [Granite](https://github.com/Themaister/Granite), [bgfx](https://github.com/bkaradzic/bgfx), among others): One of our first ideas was to outright replace existing rendering code with something that might handle all the complexity for us. And while all the mentioned libraries are more than enough for Splash, they also introduced more complexity. For example, some would introduce complexity with shaders. We then decided to explore other avenues such as Vulkan and OpenGL ES.
- Researched the viability of using Vulkan (for a rewrite) and OpenGL ES (as a drop-in replacement) with existing rendering code: The Raspberry Pi (Rpi) supports both. Vulkan is better for low-end devices like the Rpi as it has lower CPU overhead, while OpenGL ES was mainly considered as it was close to existing code and compatible with the Rpi. It also wouldn't be as complex to implement and maintain as Vulkan.
- Tested most graphics related functionality to catch any bugs not immediately apparent.
- Researched ways to profile the Pi's GPU performance, and did some rudamentary profiling runs.


## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* [MR #595](https://gitlab.com/splashmapper/splash/-/merge_requests/595): My first contribution to the project, a simple fix in the docs where a link didn't point to the proper location.
* [MR #600](https://gitlab.com/splashmapper/splash/-/merge_requests/600): Extracted out duplicated error fetching code into separate functions. It also used newer C++ features instead of the older C style code.
* [MR #602](https://gitlab.com/splashmapper/splash/-/merge_requests/602): Second attempt at making Splash work on the Pi through OpenGL ES, cleaner with better commit separation and more bugs fixed vs [MR #598](https://gitlab.com/splashmapper/splash/-/merge_requests/598) (the first version).
* [MR #618](https://gitlab.com/splashmapper/splash/-/merge_requests/618)
    * Merges the new OpenGL ES code path with the existing OpenGL code path
    * Tries to automatically create a renderer if not given via the CLI.
    * Provides an API to create and use OpenGL objects depending on the selected renderer.
    * Fixes many newly discovered bugs.
* [MR #621](https://gitlab.com/splashmapper/splash/-/merge_requests/621): Fixes a bug where Image objects didn't use the passed specification, resulting in an incorrectly sized image and out of bounds accesses.
* [MR #623](https://gitlab.com/splashmapper/splash/-/merge_requests/623): Fixes a bug where OpenGL debug callbacks would crash due to casting and passing in an object of the incorrect type.
* [MR #628](https://gitlab.com/splashmapper/splash/-/merge_requests/628): Pulls graphics code out of existing classes to make it easier to implement other APIs if needed, as well as refactoring some previously untouched graphics code.


## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* [MR #596](https://gitlab.com/splashmapper/splash/-/merge_requests/596): Turned out that testing Splash manually wasn't that time consuming, so this fell to the wayside while I was busy porting Splash for onto the Rpi. However, the MR still has some prototype code for graphics testing, so it might be merged or used in the future.
* [MR #598](https://gitlab.com/splashmapper/splash/-/merge_requests/598): First attempt at using OpenGL ES instead of OpenGL to make Splash run on the Rpi. Overall a success, but the branch was a bit messy, and was superceeded by [MR #602](https://gitlab.com/splashmapper/splash/-/merge_requests/602).
* [MR #601](https://gitlab.com/splashmapper/splash/-/merge_requests/601): Just a basic test to get comfortable with Splash's unit testing framework, for similar reasons to [MR #596](https://gitlab.com/splashmapper/splash/-/merge_requests/596), this wasn't given much attention.
* [MR #622](https://gitlab.com/splashmapper/splash/-/merge_requests/622): Already included in [MR #628](https://gitlab.com/splashmapper/splash/-/merge_requests/628). replaces duplicated and lower level code to read a texture off the GPU with a higher level RAII implementation.

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

Some objectives of this project weren't fully realized due to (mostly) technical limitations. An example includes:

*   Profiling GPU performance on the Rpi: We settled on using OpenGL ES, but the spec doesn't require any profiling functionality. Thus, the Rpi doesn't have any native/portable profiling options. Another option we could've gone with was using the extension [`GL_AMD_PERFORMANCE_MONITOR`](https://registry.khronos.org/OpenGL/extensions/AMD/AMD_performance_monitor.txt), but it required some work to set up and even more work to use properly. It also wasn't guaranteed to be portable across different GPUs. Ultimately, we decided it would be too much work for one platform and decided to skip profiling GPU performance altogether. 
* Graphics optimization: Due to not being able to pinpoint where exactly our bottlenecks were, the only option to optimize graphics code was to blindly optimize parts that we _thought_ would improve performance on the pi. This of course would've been too time consuming, so it was also skipped for the time being.

## Further reading
[Blog post](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2023-contributions/knockerpulsar/2023-10-06-porting-splash-to-the-raspberry-pi-4/) detailing more parts of the journey.

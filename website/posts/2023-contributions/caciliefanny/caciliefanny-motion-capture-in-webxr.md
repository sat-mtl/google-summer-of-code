---
title: Motion Capture in WebXR
slug: motion-capture-in-webxr
author: Fanny Cacilie
date: 2023-08-25 16:00:00 UTC-03:00
tags: contributions, products
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Fanny Cacilie
* website: [GitLab](https://gitlab.com/caciliefanny), [GitHub](https://github.com/fanny-cacilie), [Linkedin](https://www.linkedin.com/in/fanny-cacilie/)
* gitlab username: @caciliefanny
* timezone: UTC-03:00

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

Motion Capture in WebXR: a search for the uniqueness in replaying live performances

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

During my participation in the Google Summer of Code program, I executed a proposed project focused on crafting a prototype platform for generating XR experiences to allow users to relive live performances through motion capture technology.

My work revolved around two primary modules. Firstly, I tackled motion capture using MediaPipe Pose, an open-source tool for real-time pose detection. This module was integrated into a live streaming context. The second module was responsible for developing Augmented Reality (AR) sessions on the web using WebXR. To enable seamless integration between systems, I employed Web Sockets, comprising a server-side Web Socket, a client-side Web Socket, and a web application.

The Web Socket server established a socket server, capturing video frames from a camera through MediaPipe's pose estimation. These frames were then transmitted to clients in a pickled format, with size information appended. On the client side, the Web Socket module received data packets, reconstructed video frames, and presented them using OpenCV's Python package.

The web application was built using Three.js, forming a 3D scene. This application initialized scene elements, camera, renderer, lighting, and a 3D object. The scene was WebXR-compatible, offering Augmented Reality experiences. The animation loop ensured continuous updates and rendering of the 3D object, creating an interactive experience. Furthermore, the application established a WebSocket connection, logging received messages to the console.

These integrated components facilitated communication between servers and clients, allowing transmission and display of video frames. The aim was to create an augmented reality experience featuring a 3D object on a web page. However, direct manipulation of video frames within a WebXR module for AR sessions remained a work-in-progress, detailed in the "What's left to do" section.


## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

The project has been crafted within its own dedicated repository: [Motion Capture in WebXR](https://gitlab.com/sat-mtl/collaborations/2023-GSoC-MotionCaptureWebXR), containing the code that drives the application. The following topics represent the main contributions:

* [Create WebXR server](https://gitlab.com/sat-mtl/collaborations/2023-GSoC-MotionCaptureWebXR/-/commit/3a61b45e0a0ab2aa8033eeb939d4ed2b6f422d3f)

Description: Implement the WebXR server to create Augmented Reality session for a diverse array of devices.

* [MediaPipe Pose proof of concept](https://gitlab.com/sat-mtl/collaborations/2023-GSoC-MotionCaptureWebXR/-/commit/61c0412577e2ed66066a614806dbcb2733b15d1a)

Description: Design the proof of concept developed to detect and output landmarks of human bodies.


* [Web Socket communication of real-time video frames](https://gitlab.com/sat-mtl/collaborations/2023-GSoC-MotionCaptureWebXR/-/commit/2beb89013ba9bf4ee409f007db3640afd07359ee)

Description: Establish the Web Socket communication protocol to transmit real-time video frames of motion capture.

## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

* ...

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

Currently, a significant task remains on the agenda regarding the system's functionality and integration. At this juncture, the real-time communication of video frames, specifically those detecting human poses through MediaPipe Pose, is solely feasible between Python-based clients and servers using the Web Socket approach in a testing module. However, a pivotal objective is to bridge this gap, enabling seamless communication and integration between the motion capture module which is a Python-based system and the JavaScript-based WebXR environment responsible for crafting AR experiences.

To accomplish this, the integration of both programming languages becomes imperative. This amalgamation would pave the way for more comprehensive and versatile interactions within the system. This challenge holds the key to advancing the system's capabilities. It will serve as the foundation for a crucial future enhancement: the direct manipulation of video frames within the WebXR module during AR sessions.

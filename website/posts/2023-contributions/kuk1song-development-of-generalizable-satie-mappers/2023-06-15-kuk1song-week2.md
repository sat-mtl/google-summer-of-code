---
title: Haokun Song's Blog 2
slug: 2023-06-15-blog
author: Haokun Song
date: 2023-06-15 00:00:00 UTC+02:00
tags: contributions, blog
type: text
---

# Week 2

### One of the objectives assigned to me this week was to reinstall _SATIE_ by utilizing a locally cloned repository via git on the localhost [```Quarks.install("/path/to/checked/out/SATIE");```], as opposed to directly downloading it from the remote source in the SuperCollider compiler [``` Quarks.install("SATIE")```]. This approach offers the benefit of making the downloaded _SATIE_ better for development purposes.

### Subsequently, I utilized the freshly downloaded _SATIE_ to generate a _SATIE configuration_ and encountered the error depicted in the image provided:
![image](website/images/ERROR.png){width=900 height=600}

### The issue was successfully fixed by re-downloading _sc3-plugins_.

## Pending issues:

### 1. The attempt to run the examples provided in the [_Ambisonics chapter_](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/ambisonics.html) of the [_SATIE documentation_](https://sat-mtl.gitlab.io/documentation/satie/en/contents.html) using the re-downloaded _SATIE_ was unsuccessful.

### 2. The perception of spatialization through _SATIE_ is not distinct for me. When running 
```
(
Routine {
~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 30);
3.wait;
~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 60);
3.wait;
~addr.sendMsg('/satie/source/set', 'mySource', 'aziDeg', 90);
}.play;
)
``` 
### I am unable to perceive the movement of the sound source clearly. Nevertheless, the perception of spatialization effects can be subjective in nature.


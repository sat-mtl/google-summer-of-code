---
title: Haokun Song's Blog 7
slug: 2023-07-28-blog
author: Haokun Song
date: 2023-07-28 00:00:00 UTC+02:00
tags: contributions, blog
type: text
---

# Week 7

## The flow chart shows the construction of SATIE

![image](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/SATIE.png){width=550 height=360} 
|:--:| 
| *Flow chart of SATIE* |

From the depicted flowchart, it is evident that the mapper is utilized prior to the spatializer, introducing new adjustable arguments. Depending on the specific objectives, distinct mapper controls default output parameters based on corresponding algorithms and formulas through the newly introduced arguments. This enhances the flexibility and expandability of the entire process and further enriches the functionality of **_SATIE_**.

## cartesianControl Mapper
With the utilization of this mapper, you have the capability to set the position of spatializer by defining 3D points (x, y, z) within the **Cartesian coordinate** system.

For the attenuation of gainDB according to the distance, we utilize two models, the _inverse distance model_ and the _exponential distance model_, to simulate and determine the reduction of volume as an spatializer moves away from the listener.

<table>
  <tr>
    <td>Inverse Distance Model</td>
     <td>Exponential Distance Model</td>
  </tr>
  <tr>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/exponential.png" width=470 height=158></td>
  </tr>
 </table>

 ## Pending issues
 When setting the position of spatializer using one mapper, the 3D points in the cartesian coordinate system and azimuth and elevation in the spherical coordinate system can be set and successfully implement at the same time.

 ## Some new ideas for Mapper
 1. Real-time Spatializer Control: The position of the spatializer is controlled in real time by the position of the mouse, which enhances the interactivity and enjoyment of SATIE.

 2. TouchOSC Integration: Connecting Supercollider with TouchOSC via OSC, users can control mapper parameters through their phone. For instance, the movement data accelerometer and gyroscope of the phone can be associated with the mapper for a more versatile experience.
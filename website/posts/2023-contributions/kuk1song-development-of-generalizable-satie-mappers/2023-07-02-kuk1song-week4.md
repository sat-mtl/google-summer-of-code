---
title: Haokun Song's Blog 4
slug: 2023-07-02-blog
author: Haokun Song
date: 2023-07-02 00:00:00 UTC+02:00
tags: contributions, blog
type: text
---

# Week 4

## Mapper

**_Mapper_**, a plug-in in <strong><em>SATIE</em></strong>, is the focus of my research and development in this project. Firstly, really appreciate <strong><em>Michal</em></strong> @djiamnot for creating the diagram and using Blender for modelling, as well as <strong><em>Edu</em></strong> @edumeneses for providing detailed explanations. Their contributions have greatly enhanced my understanding of mapper.

> *📝* The primary function of mapper is to improve the flexibility to select and switch spatializers used in real-time. By introducing additional parameters, the sound scene configuration was further fine-tuned and get refinement, and the spatial arrangement was further enhanced, with a particular account of the <mark><em>semantic relationships</em></mark> between different spatializers. This enables better and more convenient adaptation of various spatializers to different scenarios when using <strong><em>SATIE</em></strong>.

### Spatializer

Each spatializer represents a distinct audio spatialization structural layout, determining the configuration of simulation outputs, such as the quantity and positioning of sound sources. Under the directory ```./satie/plugins/spatializers``` in the <strong><em>SATIE</em></strong> repository,  the current various types of saptilizers can be viewed.

The spatializers I have utilized are <em>\stereoPanner</em>, <em>\headphoneListener</em>, and <em>\octoVBAP</em> so far. The first two spatializers provide sound sources on the left (-90) and right (90) sides, respectively. And for the <em>\octoVBAP</em> (<em>\domeVBAP</em>, <em>\exaVBAP</em>, etc. also the same) spatializer is the practical application based on the principle of [<strong><em>VAPA (Vector Base Amplitude Panning)</em></strong>](http://legacy.spa.aalto.fi/research/cat/vbap/) on the Supercollider, which allows for the creation of multiple surround-type virtual audio sources in order to enhance the richness and effectiveness of audio spatialization.

### Mapper <em>\nearFarField1</em>

This mapper simulates a virtual near and far field by scaling the gain of different spatializers to create distinct perceptions of the distance between the user and the associated sound sources from each spatializer.

By analyzing the source code of the mapper(<em>nearFarField1.scd</em>), it can be seen that the key parameter in this mapper is <strong><em>nfIndex</em></strong>, which is the index of the near field and used here as the gain scaling factor.
```
nfGain = gainDB.dbamp * nfIndex;
ffGain = gainDB.dbamp * (1 - nfIndex);
```
From the above code, <em>nfIndex + ffIndex = 1</em>. Assuming nfIndex = <em>0.3</em>, the gain ratio between the near field and the far field is <em>3 : 7</em>(not dB scaling). When the <strong><em>nearFarField1 mapper</em></strong> is used in <strong><em>two stereoPanner spatializers</em></strong>, the resulting effect is as follows:

<table>
  <tr>
    <td><em>.set(\nfIndex, 0.3)</em></td>
    <td><em>.set(\nfIndex, 0.5)</em></td>
    <td><em>.set(\nfIndex, 0.8)</em></td>
  </tr>
  <tr>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/0.3.png" width=200 height=230></td>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/0.5.png" width=200 height=230></td>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/0.8.png" width=200 height=230></td>
  </tr>
 </table>

In the demo above, outputs channels <em>0</em> and <em>1</em> correspond to the output of one <em>stereoPanner</em>, while channels <em>2</em> and <em>3</em> correspond to another <em>stereoPanner</em>. By adjusting the parameter <strong><em>nfIndex</em></strong>, distinct near and far field effects can be observed.

> **_NOTE:_** In this demo, the parameters are identical except for the <strong><em>nfIndex</em></strong> parameter, while <strong><em>aziDeg</em></strong> is set to <em>90</em> all(<em>right hand</em>).

### Test various sounds using Mapper <em>\nearFarField1</em> 

| Sound Name | \aziDeg | \nfIndex |
|----------|:-------------:|------:|
| dustyRez | ✅ | ✅ |
| Tubie | ✅ | ✅ |
| Gravity | ❌ | ✅ |
| Marimba | ✅ | ✅ |
| SpaceDolph | ✅ | ✅ |
| ... | ... | ... |

<br/>


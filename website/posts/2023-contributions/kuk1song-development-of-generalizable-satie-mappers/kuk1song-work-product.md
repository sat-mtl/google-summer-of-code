---
title: Work Product of Haokun Song - Development of Generalizable SATIE Mappers that Optimized Audio Spatialization Flexibility
slug: work-product-kuk1song
author: Haokun Song
date: 2023-08-26 18:06:00 UTC+02:00
tags: contributions, products, SuperCollider, SATIE, mappers, multi-spatilizer
type: text
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with work-product-, then your your gitlab username and contribution title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: contributions, products; tools from SAT) 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Haokun Song
* gitlab username: [@kuk1song](https://gitlab.com/kuk1song)
* timezone: UTC+02:00

<!-- Please do not share private contact information yet (email addresses, telephone number). -->

## Title

<!-- 
Please use the same title as for your proposal
-->

Development of Generalizable SATIE Mappers that Optimized Audio Spatialization Flexibility

## Short description of work done

<!-- 
Please write a short description of work done. 
(150-200 words)
-->

During my participation in the Google Summer of Code (GSoC) 2023 program, my work primarily focused on researching the [mappers](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/plugins.html) of [SATIE](https://gitlab.com/sat-metalab/satie), a plugin designed to link the virtual sound source and the [spatializer](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/spatializers.html), and develop the innovative generalizable mappers using [SuperCollider](https://supercollider.github.io/) to enhance the flexibility and scalability of SATIE, allowing for more immersive audio spatilization experiences.

After gaining a solid understanding of the core principles and code structure, I successfully implemented a new mapper _cartesianControl1_. This mapper can manipulate the position of the virtual source through the specification of 3D points in the Cartesian coordinate system, and introduced the geometric mathematical model to realize the physical phenomenon where the gain attenuation as the distance between the sound source and the user increases.

Building upon the foundation of _cartesianControl1_, I developed two more mappers: _cartesianControl2_ and _xDirAdjacent_. These mappers were designed to enable multi-spatializer control through calculations between diverse geometric relationships so that users can dynamically and flexibly customize their desired audio spatialization effects.

## What code got merged

<!-- 
Please list all merge requests that got merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

The new mapper:

* [cartesianControl1](https://gitlab.com/sat-mtl/tools/satie/satie/-/merge_requests/531): This mapper can manipulate the position of the spatializer through the specification of 3D points in the Cartesian coordinate system.

The posted blog:

* [Blog 1, 2, 3, and 4](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests/134)
* [Blog 5 and 6](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests/135)
* [Blog 7](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests/136)

The found bug(fixed):

* [Conflict problem in sound source](https://gitlab.com/sat-mtl/tools/satie/satie/-/merge_requests/528).

## What code didn’t get merged

<!-- 
Please list all merge requests that did not get merged from all repositories related to your GSoC contribution (except: https://gitlab.com/sat-mtl/collaborations/google-summer-of-code)
-->

The new mapper(wait for review):

* [cartesianControl2](https://gitlab.com/sat-mtl/tools/satie/satie/-/merge_requests/532): when there are two spatializers, this mapper can be employed to place them independently in two irrelevant cartesian coordinate systems, enabling separate control over the distinct spatializers without any mutual interference.
* [xDirAdjacent](https://gitlab.com/sat-mtl/tools/satie/satie/-/merge_requests/534): Using this mapper, two spatializers can be defined as adjacent mode, and the direction is consistent with the x-axis.

The found bug(solution has been identified and is pending review):

* There are distortions in the negative regions of x and z in the `cartesianControl1` mapper, solved in the [merge request !533](https://gitlab.com/sat-mtl/tools/satie/satie/-/merge_requests/533) of the [SATIE project](https://gitlab.com/sat-mtl/tools/satie/satie).

## What’s left to do

<!-- 
Please write a short description of future work left to do. 
(150-200 words)
-->

* Develop _yDirAdjacent_ and _zDirAdjacent_ counterparts based on _xDirAdjacent_ to introduce the new layer for multi-spatializer control.
* Enhance multi-spatializer control by either expanding the existing mapper or creating new ones. This involves introducing new parameters and discovering additional geometric relationships to enrich the functionality.
* The current multi-spatializer control is limited to two spatializers. Once this mode is stabilized, it can be further expanded to accommodate a higher number of spatializers, enabling comprehensive multi-spatializer control.
* By employing UGens like _Changed_ and _LastValue_ to realize automation functions, the objective is to enable concurrent control of the position of spatializer in both cartesian and spherical coordinates within a single mapper, ensuring a seamless integration without any conflicts.
* Establish a connection between parameters on external devices and mappers through OSC messages. For instance, linking the gyroscope and accelerometer parameters from a mobile phone to the position control variable of the mapper, allowing spatializer position control through the mobile phone. This enhances the interactivity and enjoyment of SATIE, adding to its overall appeal.

Overall, my GSoC journey has been a rewarding experience of hands-on coding, problem-solving, and collaboration with my mentors [Edu](https://gitlab.com/edumeneses) and [Michał](https://gitlab.com/djiamnot), I am very honored to be guided by both. I hope this final implementation will pave the way for more possibilities within mappers even SATIE in the future.

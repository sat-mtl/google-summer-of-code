---
title: Haokun Song's Blog 3
slug: 2023-06-21-blog
author: Haokun Song
date: 2023-06-21 00:00:00 UTC+02:00
tags: contributions, blog
type: text
---

# Week 3

## The Challenges and Work Done This Week

 1. During the server boot process in <em><strong>SuperCollider</strong></em>, it is essential 
    to ensure that the sample rates of both the input and output devices are consistent. Typically, the built-in input and output devices on a computer are standardized to a sampling rate of _44100Hz_. Consequently, if the internal microphone and speaker of the computer are utilized directly, the occurrence of <mark>sampling rate mismatch errors</mark> is generally unlikely.
 
    For an enhanced spatial audio experience, it is highly recommended to utilize headphones, as they provide a heightened sense of space and immersion. Additionally, the level of immersion can be directly influenced by the number of loudspeakers employed. The greater the number of speakers, the more functional and rich the SATIE system becomes, as it is able to deliver a more immersive auditory experience. Imagine an auditory landscape where sounds approach you from various directions.

    
    
    ![image](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/Immersive_setup.png){width=500 height=330} 
    |:--:| 
    | *Figure: Ambisonics system in Sound and Music Computing Lab, Como Campus of Politecnico di Milano* |

    The headphones equipment I utilized was the Airpods Pro, which is not recommended for work on <em><strong>SuperCollider</strong></em> due to their embedded microphone only providing <strong>mono</strong> input with a default sample rate of _16000Hz_, which cannot be modified. The Airpods Pro has an output sampling rate that can be set to either _16000Hz_ or _48000Hz_, and they support <strong>two-channel</strong> output. This frequently leads to mismatch issues in the sampling rates and channels between the input and output, resulting in errors.

    To address this issue, I employ the <em><strong>Aggregation Device Tool</strong></em> within the Audio MIDI settings on my PC. By establishing a new Aggregation Device, it becomes possible to flexibly combine arbitrary existing input and output devices, enabling the selection of suitable devices with matching sampling rates and channels. This approach effectively resolves the issue of sample rate mismatch.

    > **_NOTE:_** On <em>OSX</em>, before server booting, you need to enter and run the code ``` Server.default.options.device = "The_Name_of_Your_Aggregation_Device";``` for selecting the Aggregation Device.

    <br/>
    
 2. The SATIE environment was successfully configured last week. This week, our focus is on running 
    the basic SATIE model. To begin, we need to establish a SATIE configuration utilizing the Spatializer. This involves instantiating a SatieConfiguration: 
    ``` 
    ~satieConfiguration = SatieConfiguration.new(s, listeningFormat:[\octoVBAP]); 
    ```
    The choice of the ```listeningFormat``` parameter aligns with the specific Spatializer being used. Each Spatializer represents a distinct audio spatialization structural layout, determining the configuration of simulation outputs, such as the quantity and positioning of sound sources. Subsequently, utilize the configuration to generate a Satie object, that is instantiating the SATIE renderer and boot it:
    ``` 
    ~satie = Satie.new(~satieConfiguration);
    ~satie.boot(); 
    ```
    Lastly, generate an audio source, and different <strong>azimuth degrees</strong> are set to manipulate the sound array, aiming to achieve the desired audio spatialization effect:
     ``` 
    ~synth = ~satie.makeSourceInstance(\dusty, \dustyRez, \default, synthArgs: [\density, 7, \attack, 15, \gainDB: -10]);
    
    <!--  Move the sound array -->
    ~synth.set(\aziDeg, 0) <!-- front of the user -->
    ~synth.set(\aziDeg, -90) <!-- left of the user -->
    ```
   <!-- ($${\color{green}listeningFormat}$$) -->
   <!-- $```\textcolor{green}{\text{listeningFormat}}```$ -->

## GUI for SATIE scenarios

The SatieGUI tool offers a nice means of comprehending the audio spatialization capabilities supported by SATIE. (Thanks to <strong><em>Edu</em></strong> @edumeneses for providing) I will demonstrate its usage. Here is the code:
```
<!-- Instantiate the GUI class -->
~visualizer = SatieGUI.new(~satie);

<!-- request the current spatializer's drawings -->
~visualizer.drawViews;
```
After successfully booting the SATIE server, you can open the GUI using the above code. The GUI is illustrated in the figure below:
<!-- <p float="left">
  <img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/top.png" width="500" height="330"/>
  <img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/front.png" width="500" height="330"/> 
</p> -->
  
<table>
  <tr>
    <td>Top View</td>
     <td>Front View</td>
  </tr>
  <tr>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/top.png" width=399 height=330></td>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/front.png" width=399 height=330></td>
  </tr>
 </table>

   By selecting different Spatializers, you can observe varying arrangements and quantities of point sources on the GUI. In this example, ```octoVBAP``` is utilized. It is worth mentioning that you can also employ multiple Spatializers simultaneously, but it is necessary to declare the output channels for each one beforehand by using ```outBusIndex``` when setting the SATIE configuration. Based on the figure, the point source with a serial number represents the sound source within the audio spatializing generated by the Spatializer. By setting different azimuth directions, you can activate the corresponding sound source positioned in that direction. For instance, the point source labeled as 0 is positioned in front of the user, while point source 2 is on the left side (set variable ```aziDeg``` to 90 ), point source 6 is to the right side (set variable ```aziDeg``` to -90), and point source 4 is positioned behind the user (set variable ```aziDeg``` to 180  / -180). To ensure an accurate perception of audio spatialization, especially in cases where the user's output device has limitations, the **_Server Meter Tool_** in SuperCollider can be used as a helpful aid to monitor the data with precision:

<table>
  <tr>
     <td>Meter Server</td>
  </tr>
  <tr>
    <td><img src="https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/raw/main/website/images/Meter.png" width=269 height=239></td>
  </tr>
 </table>

 As shown in the figure, the 7 output channels correspond to the numbered point sources in the GUI introduced before one by one. When the azimuth angle is adjusted to 0 degrees(```~synth.set(\aziDeg, 0)```), it is evident from the **_Meter_** that only channel 0 displays signal fluctuation. This confirms that only the sound source directly in front of the user is activated.

 ## Mapper

**_Mapper_**, a plug-in in SATIE, is the focus of my research and development in this project. Firstly, really appreciate <strong><em>Michal</em></strong> @djiamnot for creating the diagram and using Blender for modeling, as well as <strong><em>Edu</em></strong> @edumeneses for providing detailed explanations. Their contributions have greatly enhanced my understanding of Mapper.





---
title: Building Immersive Learning Experiences
slug: proposal-pedroansa1-hybrid-telepresence-campus
author: Pedro Andrade Ferreira Sobrinho
date: 2023-03-17 19:10:15 UTC+01:00
tags: proposals, hard, 350 hours, Satellite
type: accepted
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

Please your public contact information below this comment and add your name as value in post author metadata:

* name: Pedro Andrade Ferreira Sobrinho
* website: [Github](https://github.com/pedroansa) [Linkedin](https://linkedin.com/in/pedro-andrade-sobrinho/)
* gitlab username: pedroansa1
* timezone: UTC+01 

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title
Building Immersive Learning Experiences
<!-- 
Your title should be short, clear and interesting. 
The job of the title is to Please write below this comment eitherconvince the reviewer to read your synopsis. 
-->

## Synopsis


<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The proposed project aims to support teaching in hybrid telepresence settings by developing and embedding examplar explorable explanations into the Satellite hub, an immersive 3D social web environment developed at the Society for Arts and Technology (SAT) using Mozilla Hubs. Explorable explanations are interactive documents that use visualizations, animations, and simulations to help learners understand complex concepts. The project will integrate explorable explanations in Mozilla Hubs used for Satellite by adapting Spoke to defining dynamic assets with interactivity. This integration will allow for the creation of immersive and interactive educational environments that cater to diverse abilities in learning and allow learners to play with parameters to deepen their understanding of the concepts.

The project seeks to create synergy by interconnecting various cultural and artistic contents in the Satellite hub and embedding explorable explanations to enhance the learning experience. The explorable explanations will be designed to cater to a broad audience and facilitate access to learning.

The project will involve prototyping examplar explorable explanations and embedding them in Satellite / Mozilla Hubs, leveraging the capabilities of the platform to provide an immersive learning experience. The aim is to enhance the teaching of digital arts concepts in hybrid telepresence campus settings and promote interactivity and innovation in teaching, leveraging emerging technologies to enhance the learning experience. Ultimately, the project will contribute to promoting interactivity and innovation in teaching, leveraging emerging technologies to enhance the learning experience.


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The benefit that this project will generate for the Satellite Hub is that we will populate the hub with more and more examples of playful and educational explanations. By expanding our examples even further, we can reach more people and popularize the hub. However, the benefit is even greater if we think about the possibility of helping to expand and support pedagogical techniques in areas where traditional education cannot reach or still has many difficulties in developing like digital arts, that could profit a lot from immersive and graphical explanations due to its own graphical nature. From the hub, we could empower and facilitate the learning process of many artists (even from SAT) giving a totally new perspective of many subjects within digital arts. It would also be possible (and necessary) to think about auxiliary educational tools for people with disabilities and/or learning difficulties.

This project alone may not be able to cover all these possibilities, but it represents a spark in the direction of popularizing the space of the Satellite Hub as a tool capable of expanding and democratizing access to knowledge in an immersive way. The fact that it is an OpenSource project and the collaborative way in which the community behaves can facilitate the sharing of knowledge, skills, and resources from the community itself, encouraging the community to contribute more with their ideas and creativity in order to make the space more extensive and rich in ideas.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->


1. Investigation (May 29 - July 2)

    * Conduct a comprehensive review of existing explorable explanations and immersive learning environments. (May 29 - June 4)
    * Identify the best practices and design patterns for our project. (June 5 - June 11)
    * Identify a possible audience or subject and their learning needs. (June 12 - June 18)
    * Brainstorm several possible educational sketchs that relate to the audience chossed (June19 - June 25)
    * Choose and refine the sketchs  (June 26 - July 2)


2. Prototype Development (June 26 - August 15)

    * Mid-Term Evaluation (deadline until july 14)
    * Develop a prototype of a educational project in the Satellite/Mozilla Hubs environment. (July 3 - July 23)
    * Integrate explorable explanations in the educational project by adapting Spoke to defining dynamic assets with interactivity. (July 24 - August 7)
    * Develop and integrate interactivity and parameterization functionalities, while we think about and integrate accessibility features and tools for people with disabilities or learning difficulties. (July 24 - August 7)
    * Develop and integrate functionalities to support collaborative learning and knowledge sharing. (August 7 - August 15)

3. Testing and Optimization (August 15 - August 27)

    * Conduct extensive testing and user feedback to ensure the prototype's usability, functionality, and effectiveness. (August 7 - August 13)
    * Optimize the prototype based on user feedback and testing results. (August 14 - August 20)
    * Ensure the prototype meets the requirements and specifications outlined in the technical paper. (August 21 - August 27)

4. Documentation (August 28 - September 5)

    * Finish document the project's entire process, from the investigation to the prototype development and testing. (August 28 - September 3)
    * Finish document the project's software architecture, design, and implementation, including the source code and user manuals. (September 4 - September 5 )

End of GSOC

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

Several previous works have explored the potential of explorable explanations and telepresence in education. The work of Bret Victor on ["Explorable Explanations"](http://worrydream.com/ExplorableExplanations/) is the obvious example of using interactive and engaging explanations to teach complex concepts, I'm inspired a lot by the reflection of Nick Sousanis' in his thesis ["Unflattening"](https://www.hup.harvard.edu/catalog.php?isbn=9780674744431) about the possibilities of explaining or telling a story from new perspectives, and I think that more imersive technologies could be the forefront tools in this process. Additionally, the use of telepresence has been explored in the context of remote learning, such as in the work of S. Weerasinghe and A. Gnanayutham on ["Using a Telepresence robot in an educational context"](https://hal-univ-pau.archives-ouvertes.fr/hal-02410364/file/2019_FECS_telepresence.pdf). And we can use that all to explore the combination of explorable explanations and telepresence education in teaching digital arts concepts.

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I'm a Computer Science student at the State University of Campinas in Brazil, and I am currently involved in a double degree Engineering program at Telecom Paris. During my undergraduate studies in Brazil, I specialized in Computer Vision and Machine Learning, specifically in image processing. I have also published an article on the development of an Open Source natural image segmentation tool called Iboju for [Embrapa](https://www.embrapa.br/busca-de-publicacoes/-/publicacao/1135134/iboju-uma-ferramenta-de-anotacao-de-imagens-para-treinamento-de-detectores). 

At Telecom Paris, I am specializing in image processing, 3D, and iterative systems, with a focus on the latter two. I plan to apply for a Master's program in this field in Paris. I have taken several classes in Human-Computer-Interaction at both of these schools, and I typically focus on developing solutions for minority groups. In one such class, I was able to develop a graphical prototype of a tool that would assist illiterate individuals in managing their medication schedules and health information ([First Prototype](https://www.figma.com/proto/NgHqIcDuNsDuqdfkX22Bgl/E-Receitas?node-id=75-258&scaling=scale-down&page-id=0%3A1&starting-point-node-id=75%3A258&show-proto-sidebar=1)). I have experience with OpenGL, C++, Python, javascript, and I'm currently having courses of html and css. I also have good knowledge of object-oriented programming, prototyping and a little bit of design (have took some courses). I believe that my participation in this project would be ideal and perfectly aligned with my skills and interests. Although I haven't yet explored the world of Open Source in this area, I believe that it is the initial gateway to a world of learning. I try to keep my Github updated with some experiments I do in college, as well as some games I develop on my own using either Unity or Java.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* C++: I'm in contact with C++ since the beginning of my university and I've had Object Oriented Programmation courses on C++ and Java and studied Data Structures in C and I was also able to develop several personal projects using C++ including OpenGL projects of my university.
* Javascript: I have intermediate knowledge of Javascript, but I am currently taking classes and my goal is to deepen my understanding of this programming language.
* OpenGL: I've taken since september of 2022 computer graphics courses in the IGR master program of Telecom Paris using OpenGL to implement several algorithms in computer graphics. In my github page it can be found from the implementations of a Rigidbody to the implementation of the DFSH algorithm.
* I have experience with image processing, deep learning, and computer vision, and familiarity with Python, Scikit-learn, and PyTorch.
* Experience with game/rendering engine programming: I have also experience with game development, and have already used Unity and Godot to develop personal projects in addition to having developed a game from scratch using java API.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours. 
Please also adjust accordingly tags in post metadata.
-->


350 hours.

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard. 
Please also adjust accordingly tags in post metadata.
 -->

Hard.
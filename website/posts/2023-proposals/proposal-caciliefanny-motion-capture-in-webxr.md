---
title: Motion Capture in WebXR
slug: proposal-caciliefanny-motion-capture-in-webxr
author: Fanny Cacilie
date: 2023-03-31 15:23:00 UTC-03:00
tags: proposals, medium, 350 hours
type: accepted
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Fanny Cacilie
* website: [GitHub](https://github.com/fanny-cacilie), [Linkedin](https://www.linkedin.com/in/fanny-cacilie/)
* gitlab username: caciliefanny
* timezone: UTC-03:00

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Motion Capture in WebXR: a search for the uniqueness in replaying live performances


## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Live performances and concerts promote a sense of uniqueness. It happens for two main reasons: the relationship between the artist and the public; and the relationship between the audience itself. It is a space in time where artists respond to the public as much as the public responds to the artists. Also, every person in the audience is surrounded by other people having similar feelings and sensations. 

Searching to create related energy and experience via live performances in Extended Reality, this proposal aims to develop a platform prototype to generate a fluid XR experience for users by reliving live performances through motion capture. The key technologies for this goal are: MediaPipe Pose, open source software for pose detection in a live streaming context, and WebXR for the development and hosting of virtual reality and augmented reality experiences on the web.

Live performances have no analog due to their unique energy and feeling of shared experience. XR's experience with concerts and live performances does not necessarily wants to exist as a copy of performances in real-life, but as a proposal of different interactions. Therefore Extended Reality can create its paradigm in art and technology.


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The proposal of developing a platform prototype in Extended Reality (XR) to replay motion capture from live performances and concerts has the potential to benefit the community in various ways, from providing new forms of entertainment to preserving cultural heritage and creating economic opportunities.

As a new form of entertainment, the XR experience allows people to experience concerts and performances in innovative ways but also could make these experiences more accessible to people who are unable to attend in person due to various reasons such as geographic location, disability, or financial limitations.
 
In addition, the use of XR can enhance the experience of revisiting past performances by providing a more immersive and engaging environment than traditional video recordings. Capturing and preserving live performances through pose detection can be seen as a way to safeguard cultural heritage. 

A platform that allows people to relive concerts and live performances in XR could potentially create new perspectives on art and technology relationships and revenue streams in the XR industry, as well as provide opportunities for performers and event organizers to disclose monetize past performances in a different format or even elaborate performances that explore directly XR paradigms.


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

The present topic includes a six-stage initial structure to demonstrate deliverables including general tasks, milestones, and deadlines. 
It is following the Google Summer of Code (GSoC) [timeline](https://developers.google.com/open-source/gsoc/timeline?hl=en) and is open to suggestions and flexibility according to the development process.

### Stage 01

* **Date range:** May 4 to May 28 (3 weeks)
* **Milestones:** community bonding, documentations acknowledgment, environment setup
* **Deliverables:**
    - Get to know mentors and the organization
    - Enlighten the project processes
    - Read documentation 
    - Initial setup environment for the project
    - Write setup configuration document
    - Begin to work on the project


### Stage 02

* **Date range:** May 29 to June 18 (3 weeks)
* **Milestones:** software architecture, software design, MediaPipe Pose and WebXR initial experiences
* **Deliverables:**
    - Define approach strategy to the solution
    - Determinate and document best practices and guidelines
    - Discuss, create, and document software architecture and software design
    - Investigate, experiment, and document MediaPipe Pose
    - Investigate, experiment, and document WebXR API 
    - Create project tasks backlog (optional)

  

### Stage 03

* **Date range:** June 19 to July 09 (3 weeks)
* **Milestones:** Proof of Concept for motion capture, midterm evaluations
* **Deliverables:**
    - Design a Proof of Concept to test the validity of motion capture project ideas
    - Develop integration to MediaPipe Pose
    - Implement software for motion capture of live performances using MediaPipe Pose
    - Document the investigation and development processes of MediaPipe Pose usage for the project
    - Elaborate midterm evaluations of GSoC
    - Unit testing development (optional)
    - Integration testing development (optional)


### Stage 04

* **Date range:** July 10 to July 30 (3 weeks)
* **Milestones:** Proof of Concept for extended reality development
* **Deliverables:**
    - Design a Proof of Concept to test the validity of extended reality development ideas
    - Develop integration to WebXR API 
    - Implement software to create extended reality experiences for live performances using WebXR
    - Document the investigation and development processes of WebXR usage for the project
    - Unit testing development (optional)
    - Integration testing development (optional)


### Stage 05

* **Date range:** July 31 to August 20 (3 weeks)
* **Milestones:** extended reality hosting, systems integration 
* **Deliverables:**
    - Investigate, implement, and document extended reality hosting using WebXR
    - Integrate motion capture system to extended reality system and hosting 
    - Test system workflow
    - Pull implementations to the MediaPipe Pose open source code
    - Pull implementations to the WebXR open source code
    - Application deployment 
    - End-to-end testing development (optional)


### Stage 06

* **Date range:** August 21 to August 28 (1 week)
* **Milestones:** product submission, final mentor evaluation submission
* **Deliverables:** 
    - Final testing and reviews 
    - Implement the final release of the code
    - Submit the project product
    - Submit final GSoC evaluation


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

Related work was developed by [Society for Arts and Technology](https://sat.qc.ca/) and [Matt Wiese](https://gitlab.com/matthewwiese) named [Face tracking for improving accessibility in hybrid telepresence interactions](https://sat-mtl.gitlab.io/collaborations/google-summer-of-code/posts/2022-contributions/matthewwiese-satellite-face-tracking/work-product-matthewwiese/). It applied pose detection algorithms to help Extended Reality users navigate virtual worlds, in this project idea we seek to apply pose detection algorithms to the virtual world itself: to simulate a live performance context.



The work implemented by [Daniel Esteban](https://github.com/danielesteban) called [VRConcert](https://github.com/danielesteban/vrconcert) is a solution to create performances, concerts, and videos in Virtual Reality (VR) is similar to the present proposal that contemplates the implementation of live performances replays in Extended Reality. VRConcert allows inserting and packing audio and videos, the creates a blended scenery. It is using Three.js, a cross-browser JavaScript/API library used to create and display animated 3D graphics in a web browser, but intends to use WebXR. in the future after receiving support.


The project [DeepDreamers-Website](https://github.com/DeepDreamers/website) developed by [Deep Dreamers](https://github.com/DeepDreamers) as a Google Experiment Project is an open source framework made with Three.js using videos of previous concerts to render a VR environment aiming to provide to users the experience to live or play in telepresence in their favorite musician concert. DeepDreamers-Website idea conception is similar to the present proposal that intends to investigate, develop and deploy an extended reality experience related to the uniqueness of live performances However, the project seems to be outdated.


Even though the VRConcert and the DeepDreamers-Website projects focus on the implementation of concerts, such projects do not consider motion capture or pose detection as part of the replay of the live performances. Their goals are centered on converting videos into a Virtual Reality experience. 


In the sense of the association between Extended Reality and pose detection with MediaPipe Pose, software to identify key body locations and render visual effects on them, the project [Mediapipe-VR-Fullbody-Tracking](https://github.com/ju1ce/Mediapipe-VR-Fullbody-Tracking) uses the MediaPipe API for full-body tracking in VR with a single camera. It is an executable work in progress developed in Python and based on VRChat, a platform that allows users to interact with others with user-created 3D avatars and worlds. The Mediapipe-VR-Fullbody-Tracking focus on creating a virtual reality full-body presence so the users can check it with their VR headsets. Even though it implements motion capture of the human body, the project does not intend to work with live performances and is not inserted in the art context

It is possible to verify works that intend to implement live performances in virtual reality and others that work with motion capture. The present proposal intends to unite both areas through the investigation, development, and implementation of an experience in the Extended Reality of live performances and concerts through the motion capture of performers and artists, to provoke the unique sensation of experiencing live performances. 


## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

As a queer cisgender woman from Brazil, my journey in STEM is atypical. I graduated in Physical Education longing to study human movement since I am a professional contemporary dancer. Longing to bind the movement culture to technology I have accomplished a Systems Development technical course.

I am a Junior Software Developer with experience in the full-cycle development of systems working in the following stages: project planning, software architecture and design, coding and implementation, testing, deployment, and maintenance.

For the past two years, I worked as a part-time employee for the CERTI Foundation where I had the opportunity to develop image-processing web application solutions by processing rasters, making pixel validations, and generating data from pixels to apply equations based on state-of-art and constructing models. Additionally, I briefly participated in the open-source project: [qda-modelos](https://github.com/fanny-cacilie/qda_modelos). 

Searching to acquire more experience in open source coding, Google Summer of Code has presented as a challenging and learning opportunity. Also, an opportunity to contribute to the community members in a real-world proposal with full commitment to the present project development and implementation in active feedback routines and exchange of experiences.


## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* **required: experience with coding**
    * I have experience working with the following technologies: Python, Javascript (Node.js and ReactJS), Docker, SQL, Continuous Integration and Continuous Delivery, cloud computing, and agile methodologies. It is possible to check some of my experience with conding in my [GitHub page](https://github.com/fanny-cacilie).


* **preferred: some experience with Free and Open Source Software (FOSS)**
    * I have experience and preference with FOSS in working and personal projects, such as:
        - GNU/Linux
        - Mozilla Firefox
        - GitHub
        - Jupyter Notebook
        - PostgreSQL
        - QGIS (Quantum Geographic Information System) 
        - Copernicus Open Access Hub


* **preferred: some experience with Python**
    * I have experience with Python for backend development, using Django, Flask, and FastAPI frameworks, and using libraries and packages like numpy, pandas, geopandas, SQLAlchemy, GeoAlchemy2, OpenCV, matplotlib, and more. Here are some personal open projects I have developed:
        - API integration project: [Password Checker](https://github.com/fanny-cacilie/password-checker)
        
        - API development projects: [User Authentication in Flask](https://github.com/fanny-cacilie/user-authentication-in-flask) 

        - Python Package development and publishment to PyPI: [Bio-Optical Models for Water Quality Analysis](https://github.com/fanny-cacilie/qda_modelos)

        - [Other projects](https://github.com/fanny-cacilie?tab=repositories)


* **preferred: some interest in computer graphics**
	
    * So far, I have experienced computer graphics in a professional project that aimed to associate water quality analysis and aerospace image processing. Being able to generate, process, and extract data from images using computer graphics makes me feel exhilarated to learn and apply computer graphics to human movement and arts.


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

350 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

medium

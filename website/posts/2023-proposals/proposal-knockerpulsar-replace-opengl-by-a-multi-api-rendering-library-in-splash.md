---
title: Replace OpenGL by a multi-API rendering library in Splash
slug: proposal-knockerpulsar-replace-opengl-by-a-multi-api-rendering-library-in-splash
author: Tarek Yasser
date: 2023-03-08 20:15:11 UTC+02:00
tags: proposals, hard, 350 hours, Splash
type: accepted
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Tarek Yasser
* website: https://github.com/KnockerPulsar
* gitlab username: KnockerPulsar
* timezone: GMT+2

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Replace OpenGL by a multi-API rendering library in Splash.

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

The Splash projection mapping software allows for controlling multiple videoprojectors together to build a single projection. It is able to adapt to virtually any real geometry, as long as the surface is diffuse. Its rendering engine is built using the OpenGL API. It currently runs on platforms capable of runing Linux and handling an OpenGL 4.5 context, and has been tested successfully on x86_64 (with NVIDIA, AMD and Intel graphic cards) and aarch64 (with NVIDIA graphic cards)

However to be able to a) optimize it further and b) support more platforms (for example Raspberry Pi), it would be interesting to support more graphics API. To do this it is envisionned to replace direct use of OpenGL with an intermediate, multi-API rendering library. For now [bgfx](https://github.com/bkaradzic/bgfx) is considered.

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

1. Helping the community by allowing splash to run on more devices/OSes.  
	As the idea mentions, one of the outcomes of this project is to enable Splash to run on new devices, like the Raspberry Pi.
2. Helping the Splash team by making working with rendering code easier.  
	Having rendering/compute code neatly contained and separated ensures a better experience for the development team, allowing them to focus on what they're doing instead of juggling applcation and rendering code.
3. Would be a precedent on how to convert a project from having mixed rendering code to a clearer separation.  
	Although the details are project specific, the main steps and problems that will be faced are not. This would hopefully allow this experience to be a blueprint for applications in need of similar changes.

<!-- Separator so that the next unordered list renders properly. -->
<!-- https://stackoverflow.com/a/41212443 -->

Some of the things I hope to demonstrate:

- Steps of the main software lifecycle.
- Best practices for refactoring and documentation.
- Integration of an advanced graphics API into a non-trivial application.
- Steps to profile and solve performance bottlenecks.

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Part 1: Tearing it down (Weeks 1-8)
- (1-6) Separate existing OpenGL code to ease the transition to the chosen intermediate API.
	- This would give better insights into the requirements of Splash.
	- It would guide the search for libraries/intermediate APIs.
	- It would also provide a blueprint for implementing the libraries and APIs.
- (7+8) Discuss if more features are desired with the existing maintainers. 

### Part 2: Building it back up (Weeks 9-16)
- (9+10) Investigate the viability of [bgfx](https://github.com/bkaradzic/bgfx) and other libraries as intermediate APIs. 
- (11-16) Begin transitioning slowly to the new API.  
	This could be done either by swapping out small parts of the code at a time if the new API is compatible with the existing API. The main benefit of this is that we could slowly test if each part of the code works without having to rewrite large parts.  
	Or, if the chosen API doesn't fit the existing code, rewrite the rendering code.

### Part 3: Guiding others (Weeks 17+18)
- Document the architecture and how the rendering code interacts with the other parts of splash. 
- Function and module level documentation.

### Part 4: Making it go fast (Weeks 19+20)
- (Optional) Profile the new code/API to identify bottlenecks and improvements.
	- Could be aided by [Tracy](https://github.com/wolfpld/tracy).
- (Optional) Optimize the rendering code.

### Part 5: Making sure it runs everywhere (Weeks 21+22)
- (Optional) Test the renderer on multiple GPUs, OSes, etc...
- (Optional) More thorough testing for the graphics side.

### Part 6: Polishing it up (Weeks 23+24)
Left for clean-up work / as a buffer in case any other work takes too long.


## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->


### Blender's move to Vulkan
Since about 2018, the Blender developers have been considering refactoring their rendering code which had the same issue as Splash (OpenGL code intertwined with other code). They also wanted to support more platforms as OpenGL was getting deprecated and the drivers on many devices had bugs that were hard to workaround and required significant dev time.

The main differences versus Splash is that they focused on building their new GPU backend with Vulkan to allow for flexibility, performance, and to support more platforms. While this idea shares the main goals, we haven't aimed solely for Vulkan. Do note that their refactoring endeavors seem to still be ongoing.

#### Sources
- [Idea proposal & initial discussion](https://devtalk.blender.org/t/alternative-rendering-backends/830/5)
- [Blender Vulkan status report - September 2022](https://devtalk.blender.org/t/blender-vulkan-status-report/25706)
- [Eevee viewport module meeting 28-11-2022](https://devtalk.blender.org/t/2022-11-28-eevee-viewport-module-meeting/26711)
- [Eevee viewport module meeting 12-12-2022](https://devtalk.blender.org/t/2022-12-12-eevee-viewport-module-meeting/26903)
- [Eevee viewport module meeting 09-01-2023](https://devtalk.blender.org/t/2023-01-09-eevee-viewport-module-meeting/27212)
- [Eevee viewport module meeting 06-02-2023](https://devtalk.blender.org/t/2023-02-06-eevee-viewport-module-meeting/27525)
- [Eevee viewport module meeting 13-02-2023](https://devtalk.blender.org/t/2023-02-13-eevee-viewport-module-meeting/27674)
- [Eevee viewport module meeting 27-02-2023](https://devtalk.blender.org/t/2023-02-27-eevee-viewport-module-meeting/27962)

### Godot's Vulkanization
Godot originally used an OpenGL ES3 to be able to target desktop, mobile, and WebGL. But due to poor OpenGL ES3 support on mobile along with bad drivers, they had to rethink their strategy. They settled on moving to Vulkan for desktop, and reverting to OpenGL ES2 for mobile and web. This was the plan until they recently changed course again to support OpenGL ES3 along with Vulkan. Then they changed course again for a final time to support OpenGL 3 again for the sake of compatibility.

The main difference versus Splash is that Godot didn't do this to clean their code or to prepare for a new rendering backend, but to address the aforementioned issues. And similarly to Blender, they preferred to use their own implementation of the graphics backend instead of using an intermediate library. Do note that they use Google's ANGLE for compatibility though.

#### Sources
- [Moving to Vulkan (and ES 2.0) instead of OpenGL ES 3.0](https://godotengine.org/article/abandoning-gles3-vulkan-and-gles2/)
- [About Godot 4, Vulkan, GLES3 and GLES2](https://godotengine.org/article/about-godot4-vulkan-gles3-and-gles2/)
- [Status of the OpenGL 3 renderer](https://godotengine.org/article/status-of-opengl-renderer/)

## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->
I've been immensely passionate about game engine architecture, graphics (rasterization and raytracing), and games for a long, long time. As you'll see below, I've had my fair share of projects ranging from small pong clones, my own OpenGL renderer, and a raytracer with its own scene editor. As for my open source contributions, I contributed to Blender with two PRs, [#104718](https://projects.blender.org/blender/blender/pulls/104718), [#105410](https://projects.blender.org/blender/blender/pulls/105410).


## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* **required: C++**
	- Personal Projects:
		- [**Raylib Pong ECS**](https://github.com/KnockerPulsar/Raylib-Pong-ECS): I aimed to learn more modern C++ and game engine architecture in this project. The result was me implementing a small framework with the using [Raylib](https://github.com/raysan5/raylib) for basic drawing. I implemented a basic collision checking system, particles, and of course, ECS.
		- [**Raytracer**](https://github.com/KnockerPulsar/raytracer): Built on top of Peter Shirely's [Raytracing series](https://raytracing.github.io/) with C++ and ImGui. I added a few touches of my own including multi-threading, an editor for easy scene construction, and scene (de)serialzation.
		- [**CRT**](https://github.com/KnockerPulsar/CRT): The previous raytracer was just me getting my feet wet with raytracing, and I was getting interested in learning GPU programming. So I decided to re-implement the raytracer in C++ and OpenCL. This resulted in a significantly lower rendering time.
	- Relevant College Courses/Projects:
		-	[**Programming Techniques**](https://github.com/KnockerPulsar/PTProject): an introduction to OOP, C++, and problem solving. The project required us to use C++ and the CMU graphics library to create a simple snakes & ladders game.
		- [**Data Structures**](https://github.com/Hero2323/Data-structures): also implemented in C++, this course mainly taught us foundational data structures such as trees, heaps, queues, etc. This project mainly revolved around implementing the aforementioned datastructures and using them to simulate a resturant's operation.
		- **Design of Algorithms**: This course mainly introduced us to problem solving, graph algorithms, dynamic programming, shortest path algorithms, and other important foundational knowledege. This course included weekly problem solving contests in C++.

* **preferred: OpenGL or other graphics APIs**
	- Personal Projects:
		- [**ggl**](https://github.com/KnockerPulsar/ggl): I wanted to surpass what I achieved in my Computer Graphics project with OpenGL, and with a growing interest in the rust programming language, I decided to implement my own OpenGL renderer in Rust from the ground up.
	- Relevant College Courses/Projects:
		- [**Computer Graphics**](https://github.com/We2Am-BaSsem/GameX): This course introduced us to the basics of computer graphics like transformations, rasterization, and raytracing. We implemented a small game built with C++ and OpenGL that loaded scenes from files dynamically, I decided to go even further and introduce physics with the bullet Physics library and implement some editing functionality at run time using ImGUI.

* **Noteworthy**:
	- Personal Projects:
		- [**FPSTest**](https://github.com/KnockerPulsar/FPSTEST): My personal playground to learn Unity and C#. I experimented with level design, shaders, gameplay mechanics, modelling, and animations.
		- [**MathRock**](https://mousa-clearpictures.itch.io/math-rock): The result of my time as a mentor and game developer at Clear Pictures, this game started its life as a rhythm game but later evolved to an educational game meant to teach basic arithmetic. It was implemented in the Unity Engine. 
		
	- Relevant College Courses/Projects:
		- [**Software Engineering**](https://github.com/CMPLR-Technologies/CMPLR-Cross-Platform): This course taught us the software lifecycle. Agile, Extreme Programming, Waterfall, etc. Our project was to implement a Tumblr clone from scratch with a web UI, mobile application, and a remote backend with CI/CD.


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->

350 hours.

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

 Hard.


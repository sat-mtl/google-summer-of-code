---
title: Audio to Haptic interaction design with ForceHost and Feelix supporting DeformableHapticSurfaces
slug: proposal-maxw3llgm-force-host-support-deformablehapticsurfaces-feelix
author: Maxwell Gentili-Morin
date: 2023-03-29 01:10:11 UTC-04:00
tags: proposals, hard, 350 hours
type: accepted
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Maxwell Gentili-Morin
* website: [Linkedin](https://ca.linkedin.com/in/maxwell-gentili-morin-a14b3b1a2) [IDMIL](https://www.idmil.org/people/maxwell-gentili-morin/)
* gitlab username: [Maxw3llGM](https://gitlab.com/Maxw3llGM)
* timezone: GMT-4

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Audio to Haptic interaction design with ForceHost and Feelix supporting DeformableHapticSurfaces

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Haptics, as a multidisciplinary field, has applications within the Medical, Consumer and Entertainment domains, to name a few, and despite their widespread presence in our phones to provide the user with a vibrotactile feedback response (a subset of haptics), haptics have very minimally been explored by artists as a tool for authoring immersive arts. The proposed project aims to create low-cost affordable haptic devices with one or mutliple degrees of freedom and authoring tools for artists to implement into their artwork immersive haptics and interactive audio. In the near future, artists can then use the proposed toolkit to benefit from [Haptic floors](https://sat.qc.ca/en/haptic-floor) being deployed into immersive art spaces like the Dome at SAT.

The authoring tools [Feelix](https://feelix.xyz/), a "haptic authoring tool developed to support the design and integration of force feedback and shape change in user interfaces", and [ForceHost](https://gitlab.com/ForceHost), a toolchain that uses the functional sound synthesis and processing programming language [FAUST](https://faust.grame.fr/) that compiles firmware for audio-haptic applications, will be extended for use with the [DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces), a work-in-progress and open-source toolkit for interactive multi-linear DoF deformable surfaces, to create and demo the proposed immersive haptic and audio interaction toolkit. Support for audio input will also be added into Feelix potentially with ForceHost as an input modality so that ForceHost developed tools can interface their audio with the Haptic floor.


## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

For haptics be an artists' tool, they should not be distracted from the creative process by the technical rigours of current day haptic design. As such, the proposed immersive haptic and audio interaction toolkit is meant to facilitate the development and authoring of haptic devices by having the designer focus more on the creation process than the technical aspects. The toolkit would handle parts that are usually left up to the haptic designer like compiling code and dealing with hardware specific challenges. For SAT, this would mean that artists in residency can use the toolkit to quickly begin prototyping and designing for the haptic floor in the Dome. With an efficient design process, the artists would have more time to create their immersive art performance/installation and spend less time debugging, allowing for more creative results. Its modular nature will allow for adaptability to any designers request when needed. For other artists authoring immersive arts outside of SAT, they can benefit from a new interaction design space that links haptic technology to audio and, thanks to its open-source nature, can be expanded to suit other mediums like visual and to support open-source haptic devices referenced by [Haptipedia](https://haptipedia.org).

## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

### Expected Goals
To be completed before September:

*	Update ForceHost to function with newer versions of FAUST
*	Add support to export ForceHost FAUST code to Raspberry Pi frameworks
*	Add support for DeformableHapticSurface in Feelix through ForceHost
*	Add support audio input modalities to Feelix through ForceHost
*	Update Feelix’s and ForceHost Documentation
*	Create a demo video of the project

### Timeline (approx. monthly)

#### May - May 28

*	Community Bonding Period 
*	Investigate the Feelix, ForceHost and DeformableHapticSurface source code
*	Finalize decision on the projects next steps based on evaluation

#### May 29

*	Coding officially begins!

#### June - July

*	Add DeformableHapticSurface to Feelix and test its implementation
*	Update ForceHost to a recent stable version of FAUST
*	Investigate the requirements for ForceHost FAUST code to compile to Raspberry Pi
*	Extend ForceHost to compile for Raspberry Pi 
*	Add support for Audio inputs into Feelix control over the Haptic Floor with audio 


#### July - August

*	Create presets within Feelix’s Effect Library for the DeformableHapticSurface that takes advantage of it’s multi-DoF linear Haptic system.
*	Create and map audio effects to haptic effects within the library
*	Begin researching potential academic outlets (journals, conferences, etc.) which may be receptive to our topic

#### August - September

* Finalize all toolkit implementations
* Create a video of a demo showcasing how the newly consolidated software and hardware toolkit facilitates exploring the design space of immersive haptic and audio interactions
* Select venue and format (paper, presentation, etc.) to disseminate the results of our GSoC project

#### September - October

*   Continue to refine the codebase into its final version.
*	Write paper or presentation based upon work during project

#### October - November

*   Finalize codebase and documentation, merge all relevant work
*	Finalize paper/presentation and submit to a variety of outlets suitable to our topic

## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The goal of this project is to create a toolchain that can be used in the near future by artists to author haptic enabled immersive art for the haptic floor, currently in development at the SAT, and the Dome. The toolchain will use Feelix and ForceHost as the firmware pipelines for the Haptic Floor and DeformableHapticSurface. 

Feelix is a “tool that supports sketching and refinement of haptic force feedback effects” [Oosterhout et al.](https://dl.acm.org/doi/abs/10.1145/3382507.3418819?casa_token=FSVWjR7NAt4AAAAA:VO_798XhqstRHCPoDORNppIuod7ivnqGnOjeWIy6O1CaUJ9RZoMzRVc5lPEmiJRaeZa9Ay7E244H-bM). It uses a web-based graphical authoring tool that has users create their effects with the use of an editable graph representing either the devices torque, position, or velocity as an effect. The effects are then exported as C++ code to be integrated into the code of the haptic tool’s controller using the Feelix C++ Library. Since, it currently has no way to interface with audio, this project would be in part to work towards expanding Feelix with such abilities. 

Working in tandem with Feelix, [ForceHost](https://nime.pubpub.org/pub/jtdpakvp/release/1) provides a "toolchain for generating firmware that hosts authoring and rendering of force-feedback and audio signals and that communicates through I2C with guest motor and sensor boards." ([Frisson et al.](https://nime.pubpub.org/pub/jtdpakvp/release/1)) It uses a modified version of FAUST that supportd haptics as its compiler. From FAUST, it compiles to the ESP32 framework. Like Feelix, there is a graphical editor. Using [NexusUI](https://nexus-js.github.io/ui/), a JavaScript toolkit for UI development, it exposes internal application parameters to an external user interface. Feelix however uses the GUI for creating the functions and is not capable of modifying effect parameters at runtime.
 
[SATs' mission](https://sat.qc.ca/en/discover-the-sat) is the research and development of tools for immersive experiences and to make them accessible through the open-source framework for artists to adapt it into their own work. As such they have created tools like [Splash](https://sat-mtl.gitlab.io/documentation/splash/en/), a real time projection mapping engine, to allow for artist to display their work on the Dome, but also onto any non-uniform surface with multiple projectors. They also developed [LivePose](https://sat-mtl.gitlab.io/documentation/livepose/en/) to track in real-time human poses and movements with computer vision. Finally, [Mirador](https://gitlab.com/sat-mtl/tools/mirador) is also another project at SAT that uses Splash and LivePose for quick “prototyping and deployment of immersive, interactive experiences.” ([per their  description](https://gitlab.com/sat-mtl/tools/mirador)) It is composed of a [NVIDIA Jetson Xavier NX](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-xavier-nx/)  connected to a LED portable projector and an [Intel Realsense camera](https://www.intel.ca/content/www/ca/en/architecture-and-technology/realsense-overview.html) to project art pieces into physical spaces for viewers to experience and interact with them.  

Similar to Mirador, the proposed project is meant for prototyping and deployment of immersive art. Where it differentiates and becomes a great addition to SAT is its focuses on affordable haptic and audio tools, and not visuals. Instead, it would be meant to complement the visual authoring tools at SAT for artists to create [Gesamtkunstwerk](https://en.wikipedia.org/wiki/Gesamtkunstwerk) (total work of art) composed of audio, visuals and now Haptics. 



## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.

(max 200 words)
-->

I am in my first year of the Music Technology master program at McGill university, Montreal. I am a member of the [IDMIL](https://www.idmil.org/) with [Marcelo Wanderley](https://www.mcgill.ca/music/marcelo-m-wanderley) as my supervisor and of [CIRMMT](https://www.cirmmt.org/en/welcome-to-cirmmt) as a student member. In my undergraduate degree, I studied computer science and physics as a joint major and specialized towards the end in music technology by completing the music science and technology minor at McGill. 

At the IDMIL I am involved in the development on the [Digital Audio Workbench](https://www.idmil.org/project/the-digital-audio-workbench/) project, [the Rulers](https://www.idmil.org/project/the-rulers/) project and the [TorqueTuner](https://www.idmil.org/project/torquetuner/). I have experience working in HTML, C/C++ and Python for over 5 years and have recently learned JS. I also have experience using similar authoring tools like [Puara](https://github.com/Puara) and [hAPI](https://gitlab.com/Haply). I have contributed to the open-source project [libmapper](https://github.com/libmapper/libmapper) to have it work on Apple Silicon devices. 


## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

* __required: experience with C++ and ESP32 ([Feelix C++ Library](https://docs.feelix.xyz/downloads/c-library) and [TorqueTuner](https://github.com/idmil/torquetuner) and [DeformableHapticSurfaces](https://gitlab.com/sat-mtl/tools/DeformableHapticSurfaces))__

    *   I have five years of experience working in C/C++ mostly for Signal Processing and Digital Musical Instrument (DMI) prototyping using arduinos and ESP32 with either the Arduino IDE or PlatformIO for projects like [the Rulers](https://www.idmil.org/project/the-rulers/) and [TorqueTuner](https://www.idmil.org/project/sustainable-haptic-development/).
    *   I have three years of experience with FAUST for class projects, one in particular was the [Bucket Brigade Delay Pedal Simulation](https://www.idmil.org/project/the-analysis-and-simulation-of-bucket-brigade-devices-bbds-an-early-forms-of-sampling/).

* __required: experience with TypeScript/JavaScript ([Feelix](https://github.com/ankevanoosterhout/Feelix2.0))__

    * I have been working this past semester on extending the [Digital Audio Workbench](https://www.idmil.org/project/the-digital-audio-workbench/) project with a new tutorial to help student better understand the concepts behind audio sampling and quantization. 

* __preferred: experience with Electron and Angular (electron is based on html/css/javascript and is very easy to get started with) ([Feelix](https://github.com/ankevanoosterhout/Feelix2.0))__

    * I have experience using Electron to modify an existing open-source project called [ORCA](https://github.com/hundredrabbits/Orca) to run natively on Apple Silicon chips.

* __preferred: experience with haptics and audio interaction design__

    * Over the course of my masters, I had a course on the authoring of haptic effect, [CANHAP 501](https://wiki.canhaptics.ca/), worked on the [TorqueTuner](https://www.idmil.org/project/sustainable-haptic-development/) during a hackathon at IDMIL, and have worked on audio interactive devices such as the [T-Stick](https://www.idmil.org/project/the-t-stick/). 


## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->
350 hours


## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->
Hard


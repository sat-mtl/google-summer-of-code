---
title: Development of Generalizable SATIE Mappers for Hybrid Sound Configurations that Optimized Audio Spatialization Flexibility
slug: proposal-kuki-song-satie-multispatializer-mapper
author: Haokun Song
date: 2023-04-02 03:18:00 UTC-05:00
tags: proposals, easy, 175 hours
type: accepted
---

<!-- Please follow and keep (do not delete) all of these comments! -->

<!-- 
This template has been created using the following resource:
https://google.github.io/gsocguides/student/writing-a-proposal
-->

<!-- 
Please update post metadata: 
* title
* slug (unique string used in URL, dash-separated list of lowercase words, starting with proposal-, then your your gitlab username and proposal title; the whole string identical to the filename)
* author (your name as contributor)
* date (not in future otherwise not visible now when website is built)
* tags (comma-separated, starting with: proposals, including difficulty: easy or medium or hard; size: 175 hours or 350 hours; tools from SAT) 
-->

<!--
If you feel you must have graphical or interactive content associated with your application, please include this content in your merge request by prefixing the filenames with your page slug then followed by dash-separated words and embed this content with markdown syntax into your proposal. 
We prefer that you do not include of portrait picture of yourself yet, we want to avoid unintentional bias from reviewers. 
-->

<!--
We encourage participation from students from communities under-represented in the Science, Technology, Engineering, Arts, and Mathematics (STEAM) fields. We aim at increasing the participation of groups traditionally underrepresented in software development (including but not limited to: women, LGBTQ+, underrepresented minorities, and people with disabilities).
-->

## Name and Contact Information

<!-- Please your public contact information below this comment and add your name as value in post author metadata -->

* name: Haokun Song
* website: https://github.com/SONG-KUN
* gitlab username: KuKi SONG
* timezone: [UCT+2]Rome

<!-- Please do not share private contact information yet (email addresses, telephone number), we will ask you for this information later if your proposal is accepted. -->

## Title

<!-- 
Your title should be short, clear and interesting. 
The job of the title is to convince the reviewer to read your synopsis. 
Do not put your name in the title.
-->

Development of Generalizable SATIE Mappers for Hybrid Sound Configurations that Optimized Audio Spatialization Flexibility

## Synopsis

<!-- 
Start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal. 
Note that we expect the same content as for the Proposal Summary entry in your proposal submission form: website/posts/2022-proposals/proposal-forms.md
(150-200 words)
-->

Audio spatialization is a technique that locates and moves sound objects in three-dimensional space, creating realistic and immersive sound scenes. It is mainly achieved through the coordination of AudioListener and PannerNode nodes which can dynamically process audio sources according to spatial orientation. And meanwhile, audio spatialization can enhance the user’s experience and immersion by making the sound match the visual cues and the user’s movement. 

[SATIE(Spatial Audio Toolkit for Immersive Environments)](https://gitlab.com/sat-mtl/tools/satie/satie) can be used for audio spatialization. It is an audio spatialization engine programmed in [SuperCollider](https://supercollider.github.io/) that can render dense audio scenes to large multi-channel loudspeaker systems in real time. This lower-level audio rendering process maintains a dynamic DSP graph created and controlled via OSC messages from the external process.

SATIE can be used not only for sound object control (location, propagation, etc.) but it can also be used for [Haptic Floor](https://sat.qc.ca/en/haptic-floor) dedicated to large immersive spaces. In this project, however, we focus on a function of SATIE is that implementing geometry understanding at a [spatializer](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/spatializers.html) level. In this case, we usually apply the current version of [SATIE mappers plugin](https://sat-mtl.gitlab.io/documentation/satie/en/tuto/plugins.html) to perform this function. A mapper plugin does not process audio, it can be used for provide for non-generic control and geometric computation of lower-level spatialization parameters. However, it still has limitations that need to be programmed for each use case. In order to perfect it and improve its efficiency, we need to design and implement the generalizable SATIE mappers that could get all loaded spatializers and set them at a relative distance concerning each other. In other words, it is necessary to make each different spatializer in different positions recognize the relative position of each other. The proposed mappers will be designed to overcome these limitations and will be tested with different spatializer configurations. This project will provide SATIE with a powerful and flexible tool that will enable users to create rich and diverse sound spatial effects in different scenarios. 

## Benefits to Community

<!-- 
Make your case for a benefit to the organization, not just to yourself. 
Why would Google and SAT be proud to sponsor this work? 
How would open source or society as a whole benefit? 
What cool things would be demonstrated?
(150-200 words)
-->

The project makes SATIE more flexible because it will allow users to use SATIE in different scenarios without having to reprogram the mapper. At the same time, the project makes SATIE easier to use, as users can use a generalizable mapper to set relative positions without having to know the underlying implementation. In addition, people who use SATIE are able to use it more easily, which can be better integrated into their workflow. Many thanks to SAT and Google for promoting this project, which will make SATIE accessible to more people. It is no exaggeration to say that this project has the potential to help advance the development of audio spatial technology. And since SATIE is an open-source project, its improvements will help the entire community.


## Deliverables

<!-- 
Include a brief, clear work breakdown structure with milestones and deadlines. 
Make sure to label deliverables as optional or required. 
You may want your plan to start by producing some kind of technical paper, or planning the project in traditional Software Engineering style. 
It’s OK to include thinking time (“investigation”) in your work schedule. 
Deliverables should include investigation, coding and documentation.
(max 1000 words)
-->

***May***
- *Week 1* : Community Bonding Period | get to know mentors, read documentation, get up to speed to begin working on the project.

- *Week 2* : Investigation and research of the SATIE framework and relevant spatial audio techniques and algorithms by reviewing the existing literature and documentation.

- *Week 3 - 4* : Investigation and research on current SATIE mappers and their limitation then analyze investigation results. I will also study some examples of SATIE mappers and their use cases. In the meantime, I am going to familiarize myself with the code. Before the coding begins officially try to propose a conceptual model for the generalizable SATIE mappers.

***June***
- *Week 1* : Implementation and testing of the core existing SATIE mappers. Try to implement a SATIE mapper that can handle a simple scenario of two spatializers with different speaker configurations and relative positions.

- *Week 2 - 4* : To actually implement(coding) the conceptual model combined with the above experiment. I will extend the basic mappers with a modular and extensible design to handle more complex scenarios of multiple spatializers with arbitrary speaker configurations, relative positions, and panning algorithms. These extensions or improvements of the mappers that go beyond the basic functionality will enhance the usability and flexibility of mappers. 

***July***
- *Week 1 - 2* : Testing the improved mappers with different use cases of some realistic sound scenes or some synthetic sound sources to evaluate their effectiveness and usefulness and compare its performance with other existing solutions. In the meanwhile, test the mappers with various spatializers and verify its correctness and robustness.

- *Week 3 - 4* : Continue to optimize the improved mappers after the midterm evaluation. Adding some optional parameters or modes for fine-tuning the mapper's behavior. And continue to communicate with Mentors.

***August***
- *Week 1 - 3* : Continue coding and try to implement some prototypes of the extension version mappers and test them with some creative sound scenes. If the schedule permits, I will be glad to devote myself to work on the last extra feature which is the Create SuperCollider/SATIE methods and OSC endpoints.

- *Week 4* : Submission of the final project.



## Related Work

<!-- 
You should understand and communicate other people’s work that may be related to your own. 
Do your research, and make sure you understand how the project you are proposing fits into the target organization. 
Be sure to explain how the proposed work is different from similar related work.
(max 1000 words)
-->

The proposed project is related to various works that have been done in the field of audio spatialization. There are various technologies for audio spatialization, ranging from loudspeaker arrays to headphone-based systems. Some of the factors that affect the quality and performance of audio spatialization include the number and location of sound sources, the number and type of speakers or transducers, the spatial resolution and accuracy of the sound field, the computational complexity and latency of the sound processing, and the individual differences and preferences of the listeners.

One of the challenges of audio spatialization is to account for different scenarios and configurations of sound sources and speakers. For example, in some venues, it may be desirable to combine the existing sound system with additional speakers to increase the spatial resolution and coverage of the sound field. However, this may require different spatialization algorithms or parameters for each speaker configuration, as well as a way to share sound objects between different spatializers.

One such work is the development of a scalable haptic floor dedicated to large immersive spaces. The signal distribution architecture for this project was based on SATIE and [SWITCHER](https://gitlab.com/sat-mtl/tools/switcher), a low latency and multi-channel streaming engine. The haptic floor was composed of tiles with independently controllable vertices and was designed to cover arbitrary large flat surfaces. The project aimed to augment artistic venues with audience by offering immersive listening for a group of people, visual immersion, and floor space allowing for a case-by-case configuration of audience position and performance space.

SATIE allows users to create complex and dynamic sound scenes using various spatialization algorithms, such as Vector Base Amplitude Panning (VBAP), Ambisonics, Wave Field Synthesis (WFS), or Binaural Rendering. SATIE also supports hybrid configurations of multiple spatializers with different characteristics (number, location, and distance of speakers), by using mappers (plugins) that map sound objects from one spatializer to another.

However, while there have been several projects that have utilized SATIE for spatial audio rendering, the current mappers in SATIE are not generalizable or scalable to arbitrary scenarios and configurations. They need to be programmed for each specific use case, which limits the flexibility and usability of SATIE. Moreover, they do not provide a consistent workflow for setting relative spatializer positions and sphere sizes for VBAP, which are important parameters for achieving accurate and coherent audio spatialization.

Therefore, this project aims to create a generalizable mapper that can handle any number and type of spatializers, regardless of their characteristics. The mapper will allow users to set geometric information (relative position) for each loaded spatializer, which will enable them to create hybrid configurations of sound systems with different speaker layouts. The mapper will also provide a consistent workflow for setting relative sphere sizes for VBAP, which will improve the quality and consistency of audio spatialization.

***Sources***
-  *N. Bouillot, Z. Settel, and M. Seta. "SATIE: a live and scalable 3D audio scene rendering environment for large multi-channel loudspeaker configurations"*
-  *N. Bouillot, M. Seta. "A Scalable Haptic Floor Dedicated to Large Immersive Spaces"*
-  *N. Peters, G. Marentakis, and S. McAdams. "Current Technologies and Compositional Practices for Spatialization: A Qualitative and Quantitative Analysis"*




## Biographical Information

<!-- 
Keep your personal info brief. 
Be sure to communicate personal experiences and skills that might be relevant to the project. 
Summarize your education, work, and open source experience. 
List your skills and give evidence of your qualifications. 
Convince your organization that you can do the work. 
Any published work, successful open source projects and the like should definitely be mentioned.
(max 200 words)
-->

I am a Music and Acoustic Engineering student for master's degree in Politecnico di Milano. I have a background in audio programming and signal processing. In the meanwhile, I have a strong passion and understanding for audio spatialization, and I have been involved in a project with sound synthesis using SuperCollider as a programming language and tool. We design a lightsaber that can be controlled in real-time with a mobile phone and generate corresponding sounds like Buzz, etc. The movement of the lightsaber is driven by the OSC message concerning the accelerometer position. Then in Supercollider, we apply a series of methods such as a feedback oscillator with slow AM modulation, FM modulation, and Lowpass filter resonance, etc, to synthesize the sound of lightsaber waving and buzz sound. And in one of my previous works, I developed a custom distortion effect plugin with my classmate which is the specific parameters are controlled by the user using the GUI, like the knob, and worked on implementing advanced audio processing algorithms.

I have a strong foundation in computer science, mathematics, and physics, which are crucial for understanding and implementing audio spatialization algorithms. I am skilled in Python, MATLAB, SuperCollider, and JavaScript, and I have some knowledge of Processing and C++. I am familiar with various audio spatialization techniques, such as VBAP, ambisonics, wave field synthesis, and binaural synthesis. And I am also curious about exploring new opportunities and challenges in audio spatialization.

I think that I have the qualifications and enthusiasm to work on this project and to create valuable and innovative solutions for SATIE users. I am eager to collaborate with the SAT team and learn from their expertise and feedback.

## Skills

<!-- 
Copy the list of required/preferred skills from the project idea and paste it below. 
For each skill, illustrate with your past experience how you are the best fit for your proposal.
The ideal proofs are factual: a short summary of selections from your past contributions, ideally with hyperlinks when applicable (including but not restricted to: source code repositories, pages from portfolio), with a clarification of your role and an explanation how these support the skills involved in your GSoC proposal.
-->

**Required**
-  knowledge of SuperCollider: I have been using SuperCollider for over two years as my main tool for sound synthesis and composition. I have a keen interest in implementing a live coding environment that integrates SuperCollider with processing or Sonic Pi for creating algorithmic music and visuals. 

- knowledge of panning algorithms: I have studied different panning algorithms in my courses on spatial audio and sound engineering, such as VBAP, Ambisonics, Wave Field Synthesis, and Binaural Rendering. I have implemented some of these algorithms in SuperCollider using the built-in classes or external libraries. 

**Preferred**
- previous experience in programming SuperCollider Quarks and custom methods: Focusing on learning SATIE now.

- experience with Open Sound Control (OSC) in the context of music programming languages: I have used OSC extensively in my projects to communicate between different programming languages, such as SuperCollider, Max, Processing, Python, Javascript, etc. And also communicate between the applications and device, like making the GUI in the TouchOSC software on the phone to control the parameters from the software on the PC. In the aspect of music, by the OSC message concerning the accelerometer position, human-computer interaction can be realized to control specific parameters of audio or music, such as frequency, amplitude, and Pan value.

- understanding of the fundamentals of digital art technologies: I have a strong interest and background in digital art technologies, especially in the fields of sound art, interactive media, and generative art. I have taken courses on these topics at my university. I have also attended workshops and exhibitions on digital art technologies at various festivals and venues.

## Expected size of project 

<!--
Please write below this comment either: 175 hours or 350 hours
Please also adjust accordingly tags in post metadata.
-->


175 hours

## Rating of difficulty 

<!--
Please write below this comment either: easy, medium or hard
Please also adjust accordingly tags in post metadata.
 -->

Easy - medium

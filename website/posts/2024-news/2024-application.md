<!--
.. title: GSoC mentoring organization application
.. slug: 2024-application
.. author: Edu Meneses
.. date: 2024-01-29 13:00:00 UTC-05:00
.. tags: 
.. type: text
-->

[SAT](https://sat.qc.ca/) is proud to announce we applied for the [Google Summer of Code (GSoC) 2024](https://summerofcode.withgoogle.com/) as a mentor organization.

We will update this website with information as soon as Google publishes the list of accepted mentoring organizations. 

## About Google Summer of Code

[Google Summer of Code](https://g.co/gsoc) ([g.co/gsoc](https://g.co/gsoc)) is Google's mentorship program for bringing new contributors into open-source communities. It's happening again for the 19th year in 2023! Over 18,000 developers from 112 countries have participated.

Google Summer of Code is a unique program where new contributors to open source, ages 18 and over, are paired with a mentor to introduce them to the open source community and provide guidance while they work on a real-world open source project over the summer. Projects cover a wide range of fields including: Cloud, Operating Systems, Graphics, Medicine, Programming Languages, Robotics, Science, Security and many more. GSoC Contributors do earn a stipend to work on their medium (~175 hours) or large (~350 hours) projects. This is not an internship but provides an invaluable experience and allows you to be part of an amazing community!

GSoC is a highly competitive program, so don't wait to the last minute to prepare! GSoC Contributors should reach out to the organizations that interest them once organizations are announced on February 22, 2024. Potential GSoC Contributors can apply at [g.co/gsoc](https://g.co/gsoc).

Got questions? Email [gsoc-support@google.com](mailto:gsoc-support@google.com).


## About The Society for Arts and Technology [SAT]

Founded in 1996, the Society for Arts and Technology [SAT] is a non-profit organization dedicated to digital culture. With its triple mission as a center for the arts, training and research, the SAT is a gathering space for diverse talent, curiosity, and knowledge. It is recognized internationally for its active, leading role in developing technologies for immersive creation, mixed realities and telepresence. The Society for Arts and Technology is a place of collective learning that holds the promise of exploring technology to infuse it with more meaning, magic and humanity. 

---------------------

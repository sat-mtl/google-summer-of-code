<!--
.. title: GSoC announcement 2024
.. slug: 2024-announcement
.. author: Edu Meneses
.. date: 2024-02-23 09:00:00 UTC-05:00
.. tags: news
.. type: text
-->

Unfortunately [SAT](https://sat.qc.ca/) will not participate in the [Google Summer of Code (GSoC) 2024](https://summerofcode.withgoogle.com/).

We hope to participate again in the future!

---------------------

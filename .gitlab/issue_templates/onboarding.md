<!-- Update @... with contributor username and copy to issue title -->
## Onboarding @...

### Participants

<!-- 
List usernames of contributor and mentors so that all get notified. 
Check boxes when people are present in meetings (for the record). 
-->

- [ ] @...

### Onboarding

<!-- 
For each subsection, list SMART tasks (https://en.wikipedia.org/wiki/SMART_criteria) starting by mentioning the username of the person who is responsible with the task. 
For each task, when applicable, link to related issues or merge requests. 
-->

#### Tasks for Mentors

- [ ] @... Schedule weekly Google Meet meetings on a recurrent date: remember to [open issues](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues) for each meeting using template [weekly_meeting](.gitlab/issue_templates/weekly_meeting.md)
- [ ] @... Discuss goals and expectations (SAT and contributor)
- [ ] @... Define common availabilities with [timezones](https://www.timeanddate.com/worldclock/) (share known dates of holidays or remote work in different time zones)
- [ ] @... Define communication channels: Matrix for sync, gitlab comments for async
- [ ] @... Present gitlab and good practices: merge requests, suggestions, approval...
- [ ] @... Discuss timeline and milestones
- [ ] @... Discuss evaluations and final date of contribution
- [ ] @... Invite Contributor to join with the Developer role all gitlab repositories relevant to their contribution listed below:
    - [ ] https://gitlab.com/sat-mtl/...
    - [ ] https://gitlab.com/sat-metalab/...
- [ ] @... ...

#### Tasks for the Contributor

- [ ] @... Bookmark your own gitlab issues board: ...
<!--
Replace ... by one of these links:
https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/boards/4379423?assignee_username=matthewwiese
https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/boards/4379428?assignee_username=tiger-yash
https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/boards/4379429?assignee_username=vanshitaverma
--> 
- [ ] @... [Open an issue](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues/new) and related merge request to publish a blog post with these instruction in the issue description: 
    - please copy [blog-post-template.md](blog-post-template.md) into your folder under `website/posts/2022-contributions/` with `YYYY-MM-dd-username-title` for filename and `slug` (replacing `YYYY-MM-dd` by the date, `username` by your gitlab username and `title` by your blog post title, the whole filename should be a dash-separated list of lowercase words) and customize metadata and contents.
- [ ] @... [Open an issue](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/issues) to schedule publishing your [work product](https://google.github.io/gsocguides/student/evaluations#final-evaluations-and-work-product-submission) at the end of your GSoC with these instructions in the description: 
    - please copy [work-product-template.md](work-product-template.md) into your folder under `website/posts/2022-contributions/` with as filename `username-work-product.md` (replacing `username` by your gitlab username) and customize metadata and contents.
- [ ] @... Read Code of Conducts from gitlab repositories relevant to their contribution listed below:
    - [ ] https://gitlab.com/sat-mtl/...
    - [ ] https://gitlab.com/sat-metalab/...
- [ ] @... [Get Your Development Environment in Order](https://google.github.io/gsocguides/student/how-to-get-a-head-start#get-your-development-environment-in-order)
    - [ ] Operating System: ... (ideally Ubuntu/Pop!_OS LTS 20.04 or 22.04)
    - [ ] Text/Code Editor: ... (possibly VSCodium https://github.com/VSCodium/vscodium https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo)
    - [ ] Version control system: [setup ssh for gitlab](https://docs.gitlab.com/ee/user/ssh.html)
    - [ ] Real-time communication tool: Matrix (browser-based or standalone, your preference)
- [ ] @... ...

/label ~onboarding

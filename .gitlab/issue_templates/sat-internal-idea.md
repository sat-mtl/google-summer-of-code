<!--
SAT mentors, please use this template to propose internal SAT ideas
The contents of this template can be directly copied to an idea file for the MR
Please follow the instructions on https://gitlab.com/sat-mtl/collaborations/google-summer-of-code#target-audience-1 
-->

## Project title/description

<!-- Please add title below this comment to match the value in post title metadata -->

Project ideas template! 

## More detailed description of the project

<!-- Please write 2-5 sentences below this comment --> 

SAT is applying as organization for the Google Summer of Code 2025 edition.  
We welcome project ideas from mentors. 
To submit ideas, follow [these instructions](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/blob/main/README.md#project-ideas). 

## Expected outcomes

<!-- Please add 2-5 items below this comment --> 

* a project idea description following [our template](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/blob/main/idea-template.md), in folder [2025-ideas](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/tree/main/website/posts/2025-ideas)
* commitment to (co-)supervise one student who will learn software development while contributing to SAT tools during the [timeline](https://developers.google.com/open-source/gsoc/timeline)

## Skills required/preferred

<!-- Please add 2-5 items below this comment --> 

* required: knowledge of the [GSOC mentor guide](https://google.github.io/gsocguides/mentor/)
* required: willingness to create a markdown file and [open a merge request](https://gitlab.com/sat-mtl/collaborations/google-summer-of-code/-/merge_requests) to submit a project idea

## Possible mentors

<!-- Please list each mentor hyperlinked to their gitlab profile. 2 possible mentors are more failsafe. -->

Your name(s) hyperlinked to your gitlab profile(s)!

## Expected size of project 

<!-- Please write below this comment either: 175 hours or 350 hours -->

175 hours

## Rating of difficulty 

<!-- Please write below this comment either: easy, medium or hard -->

easy
